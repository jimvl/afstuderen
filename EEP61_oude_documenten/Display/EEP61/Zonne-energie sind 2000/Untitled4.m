clear all;
Data = csvread('KNMI_20181231 (1).txt',12,0);


Tijd =  Data(:,2) - (fix(Data(:,2)/10000)*10000);
B = unique(Tijd);
T = datenum(num2str(B), 'mmdd');



for c = 1:length(B)
    gemiddeld(c,:) = mean(Data(find(Tijd == B(c)),3));
end

Rendementpaneel = 0.1741;
Opppaneel = 1.650 * 0.992;

K = gemiddeld/360*3*Rendementpaneel * Opppaneel;


i  = 2.47;
A(1:length(B),1:1) = (i);

kboven = sum(K(K > i) - i);
konder = sum(i - K(K < i));


%-----
%KK = K;
%val = mean(K);

%i  = 1.51;
%A(1:length(B),1:1) = (i);
%KK(KK>i) = i;
 
%dsf = mean(KK);
%gsgsdg = min(K);


%Plot
hold on
plot(T,K);
plot(T,A);
%plot(T,KK);
hold off
datetick('x', 'mm')
title('Zonne-energie BMO-300BW Rotterdam 2000-2018 (KNMI)')
xlabel('Maanden') 
ylabel('kWh') 




