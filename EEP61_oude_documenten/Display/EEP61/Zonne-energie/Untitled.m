clear all;
data = csvread('KNMI_20180630.txt',12,0);

dagencsv = data(:,2);
dagen = datenum(num2str(dagencsv), 'yyyymmdd');

Q = data(:,3);
Rendementpaneel = (100-18.35)/100;
Opppaneel = 1.649 * 0.991;
ZPenergie = Q() / 360 * Rendementpaneel * Opppaneel;

kWhbat = 2.64;  % in kWh
kWhload = 5.6     % in kWh per dag


puntbelow = find(ZPenergie < kWhload ,1);

% Init variables
puntenbelow = 0;
val = kWhbat;
x = 0;
while(x == 0)
    val = val + (ZPenergie((puntbelow + puntenbelow),:) - kWhload)
    puntenbelow = puntenbelow + 1; 
    if(val < 0)
        x = 1;
    elseif(val > kWhbat)
        x = 2;
    end
end

%Plot
hold on
plot(dagen',ZPenergie);
hold off
%datetick('x', 'yy')
title('Zonne-energie BMO-300BW Rotterdam(KNMI)')
xlabel('Maanden') 
ylabel('kWh') 
