clear all;
data = csvread('KNMI_20180630.txt',12,0);

dagencsv = data(:,2);
dagen = datenum(num2str(dagencsv), 'yyyymmdd');

Q = data(:,3);
Rendementpaneel = 17.41/100;
Opppaneel = 1.65 * 0.992;
ZPenergie = Q() / 360 * 3 * Rendementpaneel * Opppaneel;


%Zomer is 23 maart - 23 september
%Winter is 24 september - 22 maart

kWhbat = 0.25;      % battery in kWh vrije capaciteit
kWhload = 0.19;      % in kWh gedurende dag

lengtharray = size(ZPenergie);

totaalZP = sum(ZPenergie)/10;

%Init
flag = 0;
capover = 0;
capbat = 0;
for n = 1: lengtharray(:,1)
   
    %capbat = (capbat + (ZPenergie(n,:) - kWhload));
    
    capbat = capbat + (ZPenergie(n,:) - kWhload);
    if(capbat > kWhbat)
       capbat = kWhbat;
   end
    if(capbat < 0)
        flag  = 1;
        puntbelow(n,:) = n;
        regelbelow = nonzeros(puntbelow);
        datebelow = datestr(dagen(regelbelow,:));
        capbat = 0; % Kan weggelaten worden
    end
end

i  = 0.34;
A(1:length(dagen),1:1) = (i);

sdfsd = capbat / 10; 
hold on
plot(dagen',A);
plot(dagen',ZPenergie);
hold off
datetick('x', 'yyyy')
title('Zonne-energie BMO-300BW Rotterdam(KNMI)')
xlabel('Jaren') 
ylabel('kWh') 
