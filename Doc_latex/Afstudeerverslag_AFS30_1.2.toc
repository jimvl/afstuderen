\babel@toc {dutch}{}
\contentsline {chapter}{\numberline {1.}Inleiding}{10}%
\contentsline {section}{\numberline {1.1}Bedrijfsachtergrond}{10}%
\contentsline {section}{\numberline {1.2}Probleemstelling}{13}%
\contentsline {section}{\numberline {1.3}Doelstelling}{14}%
\contentsline {chapter}{\numberline {2.}Requirements}{15}%
\contentsline {section}{\numberline {2.1}Inleiding}{15}%
\contentsline {section}{\numberline {2.2}Algemene beschrijving}{15}%
\contentsline {section}{\numberline {2.3}Systeemcontext}{15}%
\contentsline {subsection}{\numberline {2.3.1}Systeemfuncties}{15}%
\contentsline {section}{\numberline {2.4}Systeemfuncties}{17}%
\contentsline {subsection}{\numberline {2.4.1}Draadloze data overdracht}{17}%
\contentsline {subsubsection}{Stimulus en response}{17}%
\contentsline {subsubsection}{Functionele klanteisen}{17}%
\contentsline {subsection}{\numberline {2.4.2}Uitlezen en opslag data}{18}%
\contentsline {subsubsection}{Stimulus en response}{18}%
\contentsline {subsubsection}{Functionele klanteisen}{18}%
\contentsline {subsection}{\numberline {2.4.3}Updaten systeem en bestaande module (Optioneel)}{19}%
\contentsline {subsubsection}{Stimulus en response}{19}%
\contentsline {subsubsection}{Functionele klanteisen}{19}%
\contentsline {section}{\numberline {2.5}Niet-functionele klanteisen}{19}%
\contentsline {chapter}{\numberline {3.}Architectuur}{20}%
\contentsline {section}{\numberline {3.1}Ontwerpbesluiten}{20}%
\contentsline {section}{\numberline {3.2}Deelsystemen en koppelingen}{21}%
\contentsline {section}{\numberline {3.3}Beschrijving deelsystemen}{22}%
\contentsline {section}{\numberline {3.4}Beschrijving interconnects}{23}%
\contentsline {subsection}{\numberline {3.4.1}Deelsysteem 1: Control Data}{24}%
\contentsline {subsubsection}{Deelsysteem 1.1: Execute functions}{25}%
\contentsline {subsubsection}{Deelsysteem 1.2. Control Saved Data}{25}%
\contentsline {subsubsection}{Deelsysteem 1.3. Communicate Motor Control Unit}{25}%
\contentsline {subsection}{\numberline {3.4.2}Deelsysteem 2: Transceive Data}{26}%
\contentsline {subsection}{\numberline {3.4.3}Deelsysteem 3: Flash Light}{26}%
\contentsline {section}{\numberline {3.5}Traceerbaarheid klanteisen}{26}%
\contentsline {chapter}{\numberline {4.}Detailontwerp}{27}%
\contentsline {section}{\numberline {4.1}Deelsystemen 1 \& 2}{27}%
\contentsline {paragraph}{Nordic}{27}%
\contentsline {paragraph}{ST}{27}%
\contentsline {paragraph}{NXP}{28}%
\contentsline {paragraph}{TI}{28}%
\contentsline {paragraph}{Silicon Labs}{28}%
\contentsline {paragraph}{Conclusie}{28}%
\contentsline {section}{\numberline {4.2}Deelsysteem 1.2: Control saved data}{33}%
\contentsline {subsection}{\numberline {4.2.1}Software}{34}%
\contentsline {section}{\numberline {4.3}Deelsysteem 1.3: Communicate Motor Control Unit}{37}%
\contentsline {subsection}{\numberline {4.3.1}TX}{38}%
\contentsline {subsection}{\numberline {4.3.2}RX}{40}%
\contentsline {section}{\numberline {4.4}Unit 2: Transceive data}{42}%
\contentsline {section}{\numberline {4.5}Unit 3: LED schakeling}{48}%
\contentsline {chapter}{\numberline {5.}Realisatie}{51}%
\contentsline {section}{\numberline {5.1}Realisatie Hardware}{51}%
\contentsline {section}{\numberline {5.2}Componenten}{54}%
\contentsline {section}{\numberline {5.3}Program connector}{55}%
\contentsline {section}{\numberline {5.4}Software}{56}%
\contentsline {section}{\numberline {5.5}Crystal}{56}%
\contentsline {chapter}{\numberline {6.}Conclusie}{57}%
\contentsline {chapter}{Appendices}{58}%
\contentsline {chapter}{\numberline {A.}Onderzoek Bluetooth mesh}{59}%
\contentsline {section}{\numberline {A.1}Inleiding}{60}%
\contentsline {section}{\numberline {A.2}Bleutooth Low Energie}{60}%
\contentsline {paragraph}{Physical layer}{61}%
\contentsline {paragraph}{Link layer}{61}%
\contentsline {paragraph}{L2CAP}{61}%
\contentsline {paragraph}{GAP}{61}%
\contentsline {paragraph}{Attribute Protocol}{62}%
\contentsline {paragraph}{GATT}{63}%
\contentsline {section}{\numberline {A.3}Bluetooth mesh}{63}%
\contentsline {paragraph}{Bearer Layer}{64}%
\contentsline {paragraph}{Network Layer}{64}%
\contentsline {paragraph}{Lower Transport Layer}{64}%
\contentsline {paragraph}{Upper Transport Layer}{64}%
\contentsline {paragraph}{Acces Layer}{64}%
\contentsline {paragraph}{Foundation Model Layer}{64}%
\contentsline {paragraph}{Model layer}{64}%
\contentsline {section}{\numberline {A.4}Test mesh}{66}%
\contentsline {section}{\numberline {A.5}Conclusie}{67}%
\contentsline {chapter}{\numberline {B.}Onderzoek LED}{68}%
\contentsline {section}{\numberline {B.1}DC-DC conversie}{68}%
\contentsline {subsection}{\numberline {B.1.1}Charge pump}{69}%
\contentsline {subsubsection}{Simulatie en opbouw Charge pump}{71}%
\contentsline {subsubsection}{BOM charge pump}{75}%
\contentsline {subsection}{\numberline {B.1.2}Boost Converter}{76}%
\contentsline {subsection}{\numberline {B.1.3}Schakelen LED}{80}%
\contentsline {subsubsection}{Schakelen met transistor}{80}%
\contentsline {subsection}{\numberline {B.1.4}Schakelen met FET}{82}%
\contentsline {section}{\numberline {B.2}Conclusie}{83}%
\contentsline {chapter}{\numberline {C.}Onderzoek draadloze technologieën}{84}%
\contentsline {section}{\numberline {C.1}Inleiding}{84}%
\contentsline {section}{\numberline {C.2}Draadloze technologieën}{86}%
\contentsline {subsection}{\numberline {C.2.1}Frequentie, bereik en maximale snelheid}{87}%
\contentsline {subsection}{\numberline {C.2.2}Topologie en gebruik smartphone}{88}%
\contentsline {subsection}{\numberline {C.2.3}Kosten}{89}%
\contentsline {section}{\numberline {C.3}Conclusie}{90}%
\contentsline {chapter}{\numberline {D.}Plan Van Aanpak}{91}%
\contentsline {section}{\numberline {D.1}Achtergronden}{92}%
\contentsline {section}{\numberline {D.2}Projectresultaat}{92}%
\contentsline {section}{\numberline {D.4}Projectgrenzen en randvoorwaarden}{96}%
\contentsline {section}{\numberline {D.5}Tussenresultaten}{96}%
\contentsline {section}{\numberline {D.6}Kwaliteit}{97}%
\contentsline {section}{\numberline {D.7}Projectorganisatie}{98}%
\contentsline {section}{\numberline {D.8}Planning}{100}%
\contentsline {section}{\numberline {D.9}Kosten en baten}{103}%
\contentsline {section}{\numberline {D.10}Risico's}{103}%
\contentsline {chapter}{\numberline {E.}Acceptatie test}{106}%
\contentsline {section}{\numberline {E.1}Voorbereidingen acceptatietest}{106}%
\contentsline {subsection}{\numberline {E.1.1}Visuele testomgeving}{106}%
\contentsline {subsubsection}{Hardware}{106}%
\contentsline {subsubsection}{Software}{107}%
\contentsline {subsection}{\numberline {E.1.2}Testomgeving seriële verbinding}{107}%
\contentsline {subsubsection}{Hardware}{107}%
\contentsline {subsubsection}{Software}{107}%
\contentsline {subsubsection}{Overig}{107}%
\contentsline {subsection}{\numberline {E.1.3}Test mesh}{107}%
\contentsline {subsubsection}{Hardware}{107}%
\contentsline {subsubsection}{Software}{107}%
\contentsline {subsection}{\numberline {E.1.4}Test data opslag}{108}%
\contentsline {subsubsection}{Hardware}{108}%
\contentsline {subsubsection}{Software}{108}%
\contentsline {subsection}{\numberline {E.1.5}Test software update}{108}%
\contentsline {subsubsection}{Hardware}{108}%
\contentsline {section}{\numberline {E.2}Acceptatietestbeschrijvingen}{109}%
\contentsline {subsection}{\numberline {E.2.1}Test Bluetooth naar serieel}{109}%
\contentsline {subsubsection}{Testcase 1.1: Commando's io-net}{109}%
\contentsline {subsubsection}{Testcase 1.2: Draaien motor}{110}%
\contentsline {subsection}{\numberline {E.2.2}Test Visuele feedback}{111}%
\contentsline {subsubsection}{Testcase 2.1: Test LED}{111}%
\contentsline {subsubsection}{Testcase 2.2: Test maximale afstand}{111}%
\contentsline {subsection}{\numberline {E.2.3}Test opgeslagen data}{112}%
\contentsline {subsubsection}{Testcase 3.1: Test Opslag data}{112}%
\contentsline {subsection}{\numberline {E.2.4}Test software update}{113}%
\contentsline {section}{\numberline {E.3}Traceerbaarheid klanteisen}{113}%
\contentsline {chapter}{\numberline {F.}Integratietest}{114}%
\contentsline {section}{\numberline {F.1}Testen}{114}%
\contentsline {subsection}{\numberline {F.1.1}Test IC1: zenden UART}{115}%
\contentsline {subsection}{\numberline {F.1.2}Test IC1: ontvangen UART}{116}%
\contentsline {subsection}{\numberline {F.1.3}Test IC2: Hardware software update}{117}%
\contentsline {subsection}{\numberline {F.1.4}Test IC3: rimpel voeding}{118}%
\contentsline {subsection}{\numberline {F.1.5}Test IC4: Extern Flash}{119}%
\contentsline {subsection}{\numberline {F.1.6}Test IC5: mesh LED}{120}%
\contentsline {subsection}{\numberline {F.1.7}Test IC5: mesh UART}{121}%
\contentsline {subsection}{\numberline {F.1.8}Test IC6: LED}{122}%
\contentsline {chapter}{\numberline {G.}Unit test}{123}%
\contentsline {section}{\numberline {G.1}Test LED}{123}%
\contentsline {section}{\numberline {G.2}Test stroom LED}{124}%
\contentsline {section}{\numberline {G.3}Test UART zenden}{124}%
\contentsline {chapter}{\numberline {H.}Testresultaten}{125}%
\contentsline {section}{\numberline {H.1}Unittest}{125}%
\contentsline {section}{\numberline {H.2}Integratietest}{129}%
\contentsline {subsection}{\numberline {H.2.1}Test rimpel}{129}%
\contentsline {subsection}{\numberline {H.2.2}Test mesh LED}{130}%
\contentsline {subsection}{\numberline {H.2.3}Test mesh UART}{131}%
\contentsline {chapter}{\numberline {I.}Bill Of Materials}{132}%
\contentsline {section}{Literatuur}{134}%
