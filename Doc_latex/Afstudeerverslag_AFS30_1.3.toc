\babel@toc {dutch}{}
\contentsline {chapter}{\numberline {1.}Inleiding}{12}%
\contentsline {section}{\numberline {1.1}Bedrijfsachtergrond}{12}%
\contentsline {section}{\numberline {1.2}Probleemstelling}{15}%
\contentsline {section}{\numberline {1.3}Doelstelling}{16}%
\contentsline {chapter}{\numberline {2.}Requirements}{17}%
\contentsline {section}{\numberline {2.1}Inleiding}{17}%
\contentsline {section}{\numberline {2.2}Algemene beschrijving}{17}%
\contentsline {section}{\numberline {2.3}Systeemcontext}{17}%
\contentsline {subsection}{\numberline {2.3.1}Systeemfuncties}{17}%
\contentsline {section}{\numberline {2.4}Systeemfuncties}{19}%
\contentsline {subsection}{\numberline {2.4.1}Draadloze data overdracht}{19}%
\contentsline {subsubsection}{Stimulus en response}{19}%
\contentsline {subsubsection}{Functionele klanteisen}{19}%
\contentsline {subsection}{\numberline {2.4.2}Uitlezen en opslag data}{20}%
\contentsline {subsubsection}{Stimulus en response}{20}%
\contentsline {subsubsection}{Functionele klanteisen}{20}%
\contentsline {subsection}{\numberline {2.4.3}Updaten systeem en bestaande module (Optioneel)}{21}%
\contentsline {subsubsection}{Stimulus en response}{21}%
\contentsline {subsubsection}{Functionele klanteisen}{21}%
\contentsline {section}{\numberline {2.5}Niet-functionele klanteisen}{21}%
\contentsline {chapter}{\numberline {3.}Architectuur}{22}%
\contentsline {section}{\numberline {3.1}Ontwerpbesluiten en randvoorwaarden}{22}%
\contentsline {section}{\numberline {3.2}Deelsystemen en koppelingen}{23}%
\contentsline {section}{\numberline {3.3}Beschrijving deelsystemen}{24}%
\contentsline {section}{\numberline {3.4}Beschrijving interconnects}{25}%
\contentsline {subsection}{\numberline {3.4.1}Deelsysteem 1: Control Data}{26}%
\contentsline {subsubsection}{Deelsysteem 1.1: Execute functions}{27}%
\contentsline {subsubsection}{Deelsysteem 1.2. Control Saved Data}{27}%
\contentsline {subsubsection}{Deelsysteem 1.3. Communicate Motor Control Unit}{27}%
\contentsline {subsection}{\numberline {3.4.2}Deelsysteem 2: Transceive Data}{28}%
\contentsline {subsection}{\numberline {3.4.3}Deelsysteem 3: Flash Light}{28}%
\contentsline {section}{\numberline {3.5}Traceerbaarheid klanteisen}{28}%
\contentsline {chapter}{\numberline {4.}Detailontwerp}{29}%
\contentsline {section}{\numberline {4.1}Deelsystemen 1 \& 2}{29}%
\contentsline {paragraph}{Nordic}{29}%
\contentsline {paragraph}{ST}{29}%
\contentsline {paragraph}{NXP}{30}%
\contentsline {paragraph}{TI}{30}%
\contentsline {paragraph}{Silicon Labs}{30}%
\contentsline {paragraph}{Conclusie}{30}%
\contentsline {section}{\numberline {4.2}Deelsysteem 1.2: Control saved data}{35}%
\contentsline {subsection}{\numberline {4.2.1}Software}{36}%
\contentsline {section}{\numberline {4.3}Deelsysteem 1.3: Communicate Motor Control Unit}{39}%
\contentsline {subsection}{\numberline {4.3.1}TX}{40}%
\contentsline {subsection}{\numberline {4.3.2}RX}{42}%
\contentsline {section}{\numberline {4.4}Unit 2: Transceive data}{44}%
\contentsline {section}{\numberline {4.5}Unit 3: LED schakeling}{51}%
\contentsline {chapter}{\numberline {5.}Realisatie}{54}%
\contentsline {section}{\numberline {5.1}Realisatie Hardware}{54}%
\contentsline {section}{\numberline {5.2}Software}{57}%
\contentsline {section}{\numberline {5.3}Crystal}{57}%
\contentsline {section}{\numberline {5.4}Solderen}{58}%
\contentsline {section}{\numberline {5.5}Componenten}{58}%
\contentsline {section}{\numberline {5.6}Program connector}{59}%
\contentsline {chapter}{\numberline {6.}Conclusie}{60}%
\contentsline {chapter}{\numberline {7.}Aanbevelingen}{61}%
\contentsline {chapter}{Appendices}{62}%
\contentsline {chapter}{\numberline {A.}Onderzoek Bluetooth mesh}{63}%
\contentsline {section}{\numberline {A.1}Inleiding}{64}%
\contentsline {section}{\numberline {A.2}Bleutooth Low Energie}{64}%
\contentsline {paragraph}{Physical layer}{65}%
\contentsline {paragraph}{Link layer}{65}%
\contentsline {paragraph}{L2CAP}{65}%
\contentsline {paragraph}{GAP}{65}%
\contentsline {paragraph}{Attribute Protocol}{66}%
\contentsline {paragraph}{GATT}{67}%
\contentsline {section}{\numberline {A.3}Bluetooth mesh}{67}%
\contentsline {paragraph}{Bearer Layer}{68}%
\contentsline {paragraph}{Network Layer}{68}%
\contentsline {paragraph}{Lower Transport Layer}{68}%
\contentsline {paragraph}{Upper Transport Layer}{68}%
\contentsline {paragraph}{Acces Layer}{68}%
\contentsline {paragraph}{Foundation Model Layer}{68}%
\contentsline {paragraph}{Model layer}{68}%
\contentsline {section}{\numberline {A.4}Test mesh}{70}%
\contentsline {section}{\numberline {A.5}Conclusie}{71}%
\contentsline {chapter}{\numberline {B.}Onderzoek LED}{72}%
\contentsline {section}{\numberline {B.1}DC-DC conversie}{72}%
\contentsline {subsection}{\numberline {B.1.1}Charge pump}{73}%
\contentsline {subsubsection}{Simulatie en opbouw Charge pump}{75}%
\contentsline {subsubsection}{BOM charge pump}{79}%
\contentsline {subsection}{\numberline {B.1.2}Boost Converter}{80}%
\contentsline {subsection}{\numberline {B.1.3}Schakelen LED}{84}%
\contentsline {subsubsection}{Schakelen met transistor}{84}%
\contentsline {subsection}{\numberline {B.1.4}Schakelen met FET}{86}%
\contentsline {section}{\numberline {B.2}Conclusie}{87}%
\contentsline {chapter}{\numberline {C.}Onderzoek draadloze technologieën}{88}%
\contentsline {section}{\numberline {C.1}Inleiding}{88}%
\contentsline {section}{\numberline {C.2}Draadloze technologieën}{90}%
\contentsline {subsection}{\numberline {C.2.1}Frequentie, bereik en maximale snelheid}{91}%
\contentsline {subsection}{\numberline {C.2.2}Topologie en gebruik smartphone}{92}%
\contentsline {subsection}{\numberline {C.2.3}Kosten}{93}%
\contentsline {section}{\numberline {C.3}Conclusie}{94}%
\contentsline {chapter}{\numberline {D.}Plan Van Aanpak}{95}%
\contentsline {section}{\numberline {D.1}Achtergronden}{96}%
\contentsline {section}{\numberline {D.2}Projectresultaat}{96}%
\contentsline {section}{\numberline {D.4}Projectgrenzen en randvoorwaarden}{100}%
\contentsline {section}{\numberline {D.5}Tussenresultaten}{100}%
\contentsline {section}{\numberline {D.6}Kwaliteit}{101}%
\contentsline {section}{\numberline {D.7}Projectorganisatie}{102}%
\contentsline {section}{\numberline {D.8}Planning}{103}%
\contentsline {section}{\numberline {D.9}Kosten en baten}{106}%
\contentsline {section}{\numberline {D.10}Risico's}{106}%
\contentsline {chapter}{\numberline {E.}Acceptatie test}{109}%
\contentsline {section}{\numberline {E.1}Voorbereidingen acceptatietest}{109}%
\contentsline {subsection}{\numberline {E.1.1}Visuele testomgeving}{109}%
\contentsline {subsubsection}{Hardware}{109}%
\contentsline {subsubsection}{Software}{110}%
\contentsline {subsection}{\numberline {E.1.2}Testomgeving seriële verbinding}{110}%
\contentsline {subsubsection}{Hardware}{110}%
\contentsline {subsubsection}{Software}{110}%
\contentsline {subsubsection}{Overig}{110}%
\contentsline {subsection}{\numberline {E.1.3}Test mesh}{110}%
\contentsline {subsubsection}{Hardware}{110}%
\contentsline {subsubsection}{Software}{110}%
\contentsline {subsection}{\numberline {E.1.4}Test data opslag}{111}%
\contentsline {subsubsection}{Hardware}{111}%
\contentsline {subsubsection}{Software}{111}%
\contentsline {subsection}{\numberline {E.1.5}Test software update}{111}%
\contentsline {subsubsection}{Hardware}{111}%
\contentsline {section}{\numberline {E.2}Acceptatietestbeschrijvingen}{112}%
\contentsline {subsection}{\numberline {E.2.1}Test Bluetooth naar serieel}{112}%
\contentsline {subsubsection}{Testcase 1.1: Commando's io-net}{112}%
\contentsline {subsubsection}{Testcase 1.2: Draaien motor}{113}%
\contentsline {subsection}{\numberline {E.2.2}Test Visuele feedback}{114}%
\contentsline {subsubsection}{Testcase 2.1: Test LED}{114}%
\contentsline {subsubsection}{Testcase 2.2: Test maximale afstand}{114}%
\contentsline {subsection}{\numberline {E.2.3}Test opgeslagen data}{115}%
\contentsline {subsubsection}{Testcase 3.1: Test Opslag data}{115}%
\contentsline {subsection}{\numberline {E.2.4}Test software update}{116}%
\contentsline {subsection}{\numberline {E.2.5}Test RoHS}{116}%
\contentsline {section}{\numberline {E.3}Traceerbaarheid klanteisen}{117}%
\contentsline {chapter}{\numberline {F.}Integratietest}{118}%
\contentsline {section}{\numberline {F.1}Testen}{118}%
\contentsline {subsection}{\numberline {F.1.1}Test IC1: zenden UART}{119}%
\contentsline {subsection}{\numberline {F.1.2}Test IC1: ontvangen UART}{120}%
\contentsline {subsection}{\numberline {F.1.3}Test IC2: Hardware software update}{121}%
\contentsline {subsection}{\numberline {F.1.4}Test IC3: rimpel voeding}{122}%
\contentsline {subsection}{\numberline {F.1.5}Test IC4: Extern Flash}{123}%
\contentsline {subsection}{\numberline {F.1.6}Test IC5: mesh LED}{124}%
\contentsline {subsection}{\numberline {F.1.7}Test IC5: mesh UART}{125}%
\contentsline {subsection}{\numberline {F.1.8}Test IC6: LED}{126}%
\contentsline {chapter}{\numberline {G.}Unit test}{127}%
\contentsline {section}{\numberline {G.1}Test LED}{127}%
\contentsline {section}{\numberline {G.2}Test stroom LED}{128}%
\contentsline {section}{\numberline {G.3}Test UART zenden}{128}%
\contentsline {chapter}{\numberline {H.}Testresultaten}{129}%
\contentsline {section}{\numberline {H.1}Unittest}{129}%
\contentsline {section}{\numberline {H.2}Integratietest}{133}%
\contentsline {subsection}{\numberline {H.2.1}Test rimpel}{133}%
\contentsline {subsection}{\numberline {H.2.2}Test mesh LED}{134}%
\contentsline {subsection}{\numberline {H.2.3}Test mesh UART}{135}%
\contentsline {section}{\numberline {H.3}Acceptatietest}{136}%
\contentsline {subsection}{\numberline {H.3.1}Testcase 1.1 en testcase 1.2}{136}%
\contentsline {subsection}{\numberline {H.3.2}Testcase 2.1}{137}%
\contentsline {section}{\numberline {H.4}Testcase 2.2}{138}%
\contentsline {section}{\numberline {H.5}Testcase 3.1}{139}%
\contentsline {section}{\numberline {H.6}Testcase 5.1}{139}%
\contentsline {chapter}{\numberline {I.}Bill Of Materials}{140}%
\contentsline {chapter}{\numberline {J.}Handleiding BlueFarm}{142}%
\contentsline {section}{\numberline {J.1}Programmeren}{142}%
\contentsline {section}{\numberline {J.2}Provisioneren}{143}%
\contentsline {section}{\numberline {J.3}Probleem oplossen}{145}%
\contentsline {chapter}{\numberline {K.}Competentieverantwoording}{146}%
\contentsline {section}{\numberline {K.1}Analyseren}{146}%
\contentsline {section}{\numberline {K.2}Ontwerpen}{147}%
\contentsline {section}{\numberline {K.3}Realiseren}{147}%
\contentsline {section}{\numberline {K.4}Beheren}{147}%
\contentsline {section}{\numberline {K.5}Managen}{147}%
\contentsline {section}{\numberline {K.6}Adviseren}{148}%
\contentsline {section}{\numberline {K.7}Onderzoeken}{148}%
\contentsline {section}{\numberline {K.8}Professionaliseren}{148}%
\contentsline {chapter}{\numberline {L.}Reflectie}{149}%
\contentsline {section}{\numberline {L.1}Analyseren}{149}%
\contentsline {section}{\numberline {L.2}Ontwerpen}{149}%
\contentsline {section}{\numberline {L.3}Realiseren}{149}%
\contentsline {section}{\numberline {L.4}Beheren}{149}%
\contentsline {section}{\numberline {L.5}Managen}{150}%
\contentsline {section}{\numberline {L.6}Adviseren}{150}%
\contentsline {section}{\numberline {L.7}Onderzoeken}{150}%
\contentsline {section}{\numberline {L.8}Professionaliseren}{150}%
