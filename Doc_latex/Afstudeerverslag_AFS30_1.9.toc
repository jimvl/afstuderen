\babel@toc {dutch}{}
\contentsline {chapter}{\numberline {1.}Inleiding}{10}%
\contentsline {section}{\numberline {1.1}Bedrijfsachtergrond}{10}%
\contentsline {section}{\numberline {1.2}De klant}{10}%
\contentsline {section}{\numberline {1.3}Probleemstelling}{13}%
\contentsline {section}{\numberline {1.4}Doelstelling}{14}%
\contentsline {chapter}{\numberline {2.}Requirements}{15}%
\contentsline {section}{\numberline {2.1}Systeemcontext}{15}%
\contentsline {section}{\numberline {2.2}Functionele Systeemeisen}{16}%
\contentsline {section}{\numberline {2.3}Niet-functionele klanteisen}{18}%
\contentsline {chapter}{\numberline {3.}Onderzoek}{19}%
\contentsline {section}{\numberline {3.1}Vooronderzoek}{19}%
\contentsline {section}{\numberline {3.2}Identificeren Systeem}{20}%
\contentsline {section}{\numberline {3.3}Onderzoek Bluetooth mesh}{21}%
\contentsline {chapter}{\numberline {4.}Architectuur}{22}%
\contentsline {chapter}{\numberline {5.}Detailontwerp}{23}%
\contentsline {section}{\numberline {5.1}Traceertabel klanteisen}{23}%
\contentsline {section}{\numberline {5.2}Module}{24}%
\contentsline {subsubsection}{Return Loss}{25}%
\contentsline {subsection}{\numberline {5.2.1}Microcontroller}{26}%
\contentsline {subsection}{\numberline {5.2.2}Bluetooth 5.0 mesh}{27}%
\contentsline {subsubsection}{Node}{27}%
\contentsline {subsubsection}{Model}{28}%
\contentsline {subsection}{\numberline {5.2.3}Proxy server}{29}%
\contentsline {subsection}{\numberline {5.2.4}Long Range}{30}%
\contentsline {subsection}{\numberline {5.2.5}Terugsturen diagnostische data}{31}%
\contentsline {subsection}{\numberline {5.2.6}UART IM60/IM125}{32}%
\contentsline {subsubsection}{TX}{32}%
\contentsline {subsubsection}{RX}{32}%
\contentsline {subsubsection}{IO-NET}{33}%
\contentsline {section}{\numberline {5.3}Geheugen}{34}%
\contentsline {section}{\numberline {5.4}Flits LED}{37}%
\contentsline {subsection}{\numberline {5.4.1}Schakeling}{37}%
\contentsline {subsubsection}{Simulatie}{38}%
\contentsline {chapter}{\numberline {6.}Testresultaten}{40}%
\contentsline {section}{\numberline {6.1}Voorbereidingen acceptatietest}{40}%
\contentsline {section}{\numberline {6.2}Acceptatietestbeschrijvingen}{41}%
\contentsline {subsection}{\numberline {6.2.1}Test UART commando IO-net}{41}%
\contentsline {section}{\numberline {6.3}Test doorsturen berichten}{42}%
\contentsline {section}{\numberline {6.4}Test sturen diagnostische data}{43}%
\contentsline {subsection}{\numberline {6.4.1}Test openen sluiten directe verbinding}{44}%
\contentsline {subsection}{\numberline {6.4.2}Test bereik tussen nodes}{45}%
\contentsline {subsubsection}{Test LED}{46}%
\contentsline {subsection}{\numberline {6.4.3}Extern geheugen}{47}%
\contentsline {chapter}{\numberline {7.}Conclusie en aanbevelingen}{48}%
\contentsline {chapter}{Appendices}{49}%
\contentsline {chapter}{\numberline {A.}Onderzoek draadloze technologieën}{50}%
\contentsline {section}{\numberline {A.1}Inleiding}{50}%
\contentsline {section}{\numberline {A.2}Draadloze technologieën}{52}%
\contentsline {subsection}{\numberline {A.2.1}Frequentie, bereik en maximale snelheid}{53}%
\contentsline {subsection}{\numberline {A.2.2}Topologie en gebruik smartphone}{54}%
\contentsline {subsection}{\numberline {A.2.3}Kosten}{54}%
\contentsline {section}{\numberline {A.3}Conclusie}{55}%
\contentsline {chapter}{\numberline {B.}Onderzoek Bluetooth mesh}{56}%
\contentsline {section}{\numberline {B.1}Inleiding}{57}%
\contentsline {section}{\numberline {B.2}Bluetooth Low Energie}{57}%
\contentsline {paragraph}{Physical layer}{58}%
\contentsline {paragraph}{Link layer}{58}%
\contentsline {paragraph}{L2CAP}{58}%
\contentsline {paragraph}{GAP}{58}%
\contentsline {paragraph}{Attribute Protocol}{58}%
\contentsline {paragraph}{GATT}{59}%
\contentsline {section}{\numberline {B.3}Bluetooth mesh}{59}%
\contentsline {paragraph}{Bearer Layer}{59}%
\contentsline {paragraph}{Network Layer}{59}%
\contentsline {paragraph}{Lower Transport Layer}{60}%
\contentsline {paragraph}{Upper Transport Layer}{60}%
\contentsline {paragraph}{Acces Layer}{60}%
\contentsline {paragraph}{Foundation Model Layer}{60}%
\contentsline {paragraph}{Model layer}{60}%
\contentsline {section}{\numberline {B.4}Adressering}{61}%
\contentsline {subsection}{\numberline {B.4.1}Unicast adres}{61}%
\contentsline {subsection}{\numberline {B.4.2}Group adres}{61}%
\contentsline {subsection}{\numberline {B.4.3}Virtual adres}{62}%
\contentsline {section}{\numberline {B.5}Software}{62}%
\contentsline {subsection}{\numberline {B.5.1}Software mesh}{62}%
\contentsline {section}{\numberline {B.6}Test mesh}{62}%
\contentsline {section}{\numberline {B.7}Conclusie}{63}%
\contentsline {chapter}{\numberline {C.}Onderzoek LED}{64}%
\contentsline {section}{\numberline {C.1}DC-DC conversie}{65}%
\contentsline {subsubsection}{Componentkeuze}{65}%
\contentsline {subsubsection}{Berekeningen charge pump}{67}%
\contentsline {subsubsection}{Simulatie}{71}%
\contentsline {subsubsection}{BOM charge pump}{72}%
\contentsline {subsection}{\numberline {C.1.1}Boost Converter}{73}%
\contentsline {section}{\numberline {C.2}Conclusie}{74}%
\contentsline {chapter}{\numberline {D.}Berekeningen gekozen FET}{75}%
\contentsline {chapter}{\numberline {E.}Data Dictionary}{76}%
\contentsline {chapter}{\numberline {F.}VNA antenne testen}{80}%
\contentsline {chapter}{\numberline {G.}Tekeningen PCB en solderen}{83}%
\contentsline {section}{\numberline {G.1}Realisatie Hardware}{83}%
\contentsline {section}{\numberline {G.2}Solderen}{85}%
\contentsline {section}{\numberline {G.3}Componenten}{86}%
\contentsline {section}{\numberline {G.4}Program connector}{87}%
\contentsline {section}{\numberline {G.5}Software}{87}%
\contentsline {chapter}{\numberline {H.}Integratietest}{88}%
\contentsline {section}{\numberline {H.1}Testen}{88}%
\contentsline {subsection}{\numberline {H.1.1}Test PWM}{89}%
\contentsline {subsection}{\numberline {H.1.2}Test Pulse}{90}%
\contentsline {subsection}{\numberline {H.1.3}Test aangelegde verbindingen Software update}{91}%
\contentsline {chapter}{\numberline {I.}Unit test}{92}%
\contentsline {section}{\numberline {I.1}Test LED}{92}%
\contentsline {section}{\numberline {I.2}Test stroom LED}{93}%
\contentsline {section}{\numberline {I.3}Test UART zenden}{94}%
\contentsline {section}{\numberline {I.4}Testresultaten commando's IO-net}{95}%
\contentsline {chapter}{\numberline {J.}Bill Of Materials}{99}%
\contentsline {chapter}{\numberline {K.}Handleiding BlueFarm}{101}%
\contentsline {section}{\numberline {K.1}Programmeren}{101}%
\contentsline {section}{\numberline {K.2}Provisioneren}{102}%
\contentsline {section}{\numberline {K.3}Probleem oplossen}{104}%
\contentsline {chapter}{\numberline {L.}Plan Van Aanpak}{105}%
\contentsline {section}{\numberline {L.1}Achtergronden}{106}%
\contentsline {section}{\numberline {L.2}Projectresultaat}{106}%
\contentsline {section}{\numberline {L.4}Projectgrenzen en randvoorwaarden}{109}%
\contentsline {section}{\numberline {L.5}Tussenresultaten}{109}%
\contentsline {section}{\numberline {L.6}Kwaliteit}{110}%
\contentsline {section}{\numberline {L.7}Projectorganisatie}{111}%
\contentsline {section}{\numberline {L.8}Planning}{112}%
\contentsline {section}{\numberline {L.9}Kosten en baten}{114}%
\contentsline {section}{\numberline {L.10}Risico's}{114}%
\contentsline {chapter}{\numberline {M.}Competentieverantwoording}{117}%
\contentsline {section}{\numberline {M.1}Analyseren}{117}%
\contentsline {section}{\numberline {M.2}Ontwerpen}{118}%
\contentsline {section}{\numberline {M.3}Realiseren}{118}%
\contentsline {section}{\numberline {M.4}Beheren}{118}%
\contentsline {section}{\numberline {M.5}Managen}{118}%
\contentsline {section}{\numberline {M.6}Adviseren}{119}%
\contentsline {section}{\numberline {M.7}Onderzoeken}{119}%
\contentsline {section}{\numberline {M.8}Professionaliseren}{119}%
\contentsline {chapter}{\numberline {N.}Reflectie}{120}%
\contentsline {section}{\numberline {N.1}Analyseren}{120}%
\contentsline {section}{\numberline {N.2}Ontwerpen}{120}%
\contentsline {section}{\numberline {N.3}Realiseren}{120}%
\contentsline {section}{\numberline {N.4}Beheren}{120}%
\contentsline {section}{\numberline {N.5}Managen}{121}%
\contentsline {section}{\numberline {N.6}Adviseren}{121}%
\contentsline {section}{\numberline {N.7}Onderzoeken}{121}%
\contentsline {section}{\numberline {N.8}Professionaliseren}{121}%
\contentsline {chapter}{\numberline {O.}Beoordeling bedrijfsbegeleider}{122}%
\contentsline {chapter}{\numberline {P.}Code}{123}%
\contentsline {section}{\numberline {P.1}Main.c}{123}%
\contentsline {section}{\numberline {P.2}UART\_debug.c}{130}%
\contentsline {section}{\numberline {P.3}SPI\_Flash.c}{131}%
\contentsline {section}{\numberline {P.4}Code Diagnostische data}{132}%
\contentsline {section}{Literatuur}{134}%
