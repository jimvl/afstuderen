\babel@toc {dutch}{}
\contentsline {chapter}{\numberline {1.}Inleiding}{9}%
\contentsline {section}{\numberline {1.1}Bedrijfsachtergrond}{9}%
\contentsline {section}{\numberline {1.2}Probleemstelling}{12}%
\contentsline {section}{\numberline {1.3}Doelstelling}{13}%
\contentsline {chapter}{\numberline {2.}Requirements}{14}%
\contentsline {section}{\numberline {2.1}Inleiding}{14}%
\contentsline {section}{\numberline {2.2}Algemene beschrijving}{14}%
\contentsline {section}{\numberline {2.3}Systeemcontext}{14}%
\contentsline {subsection}{\numberline {2.3.1}Systeemfuncties}{14}%
\contentsline {section}{\numberline {2.4}Systeemfuncties}{16}%
\contentsline {subsection}{\numberline {2.4.1}Draadloze data overdracht}{16}%
\contentsline {subsubsection}{Stimulus en response}{16}%
\contentsline {subsubsection}{Functionele klanteisen}{16}%
\contentsline {subsection}{\numberline {2.4.2}Uitlezen en opslag data}{17}%
\contentsline {subsubsection}{Stimulus en response}{17}%
\contentsline {subsubsection}{Functionele klanteisen}{17}%
\contentsline {subsection}{\numberline {2.4.3}Updaten systeem en bestaande module (Optioneel)}{18}%
\contentsline {subsubsection}{Stimulus en response}{18}%
\contentsline {subsubsection}{Functionele klanteisen}{18}%
\contentsline {section}{\numberline {2.5}Niet-functionele klanteisen}{18}%
\contentsline {chapter}{\numberline {3.}Architectuur}{19}%
\contentsline {section}{\numberline {3.1}Ontwerpbesluiten}{19}%
\contentsline {section}{\numberline {3.2}Deelsystemen en koppelingen}{20}%
\contentsline {section}{\numberline {3.3}Beschrijving deelsystemen}{21}%
\contentsline {subsection}{\numberline {3.3.1}Deelsysteem 1: Control Data}{22}%
\contentsline {subsubsection}{Deelsysteem 1.1: Execute functions}{23}%
\contentsline {subsubsection}{Deelsysteem 1.2. Control Saved Data}{23}%
\contentsline {subsubsection}{Deelsysteem 1.3. Communicate Motor Control Unit}{23}%
\contentsline {subsection}{\numberline {3.3.2}Deelsysteem 2: Transeive Data}{24}%
\contentsline {subsection}{\numberline {3.3.3}Deelsysteem 3: Flash Light}{24}%
\contentsline {section}{\numberline {3.4}Beschrijving interconnects}{24}%
\contentsline {section}{\numberline {3.5}Traceerbaarheid klanteisen}{25}%
\contentsline {chapter}{\numberline {4.}Detailontwerp}{26}%
\contentsline {subsection}{\numberline {4.0.1}Deelsystemen 1 \& 2}{26}%
\contentsline {subsubsection}{Hardware deelsystemen 1 \& 2}{26}%
\contentsline {paragraph}{Nordic}{26}%
\contentsline {paragraph}{ST}{26}%
\contentsline {paragraph}{NXP}{27}%
\contentsline {paragraph}{TI}{27}%
\contentsline {paragraph}{Silicon Labs}{27}%
\contentsline {paragraph}{Conclusie}{27}%
\contentsline {subsubsection}{Software deelsysteem 1}{29}%
\contentsline {subsubsection}{Software deelsysteem 2}{29}%
\contentsline {section}{\numberline {4.1}Unit 3: LED schakeling}{30}%
\contentsline {chapter}{\numberline {5.}Realisatie}{31}%
\contentsline {section}{\numberline {5.1}Realisatie Hardware}{31}%
\contentsline {subsection}{\numberline {5.1.1}Componenten}{32}%
\contentsline {chapter}{Appendices}{33}%
\contentsline {chapter}{\numberline {A.}Onderzoek LED}{34}%
\contentsline {section}{\numberline {A.1}DC-DC conversie}{36}%
\contentsline {subsection}{\numberline {A.1.1}Charge pump}{36}%
\contentsline {subsubsection}{Simulatie en opbouw Charge pump}{38}%
\contentsline {subsubsection}{BOM charge pump}{42}%
\contentsline {subsection}{\numberline {A.1.2}Schakelen LED}{43}%
\contentsline {subsubsection}{Schakelen met transistor}{43}%
\contentsline {subsection}{\numberline {A.1.3}Schakelen met FET}{45}%
\contentsline {chapter}{\numberline {B.}Onderzoek draadloze technologieën}{47}%
\contentsline {section}{\numberline {B.1}Inleiding}{47}%
\contentsline {section}{\numberline {B.2}Draadloze technologieën}{49}%
\contentsline {subsection}{\numberline {B.2.1}Frequentie, bereik en maximale snelheid}{50}%
\contentsline {subsection}{\numberline {B.2.2}Topologie en gebruik smartphone}{51}%
\contentsline {subsection}{\numberline {B.2.3}Kosten}{52}%
\contentsline {section}{\numberline {B.3}Conclusie}{53}%
\contentsline {chapter}{\numberline {C.}Plan Van Aanpak}{54}%
\contentsline {section}{\numberline {C.1}Achtergronden}{55}%
\contentsline {section}{\numberline {C.2}Projectresultaat}{55}%
\contentsline {section}{\numberline {C.4}Projectgrenzen en randvoorwaarden}{59}%
\contentsline {section}{\numberline {C.5}Tussenresultaten}{59}%
\contentsline {section}{\numberline {C.6}Kwaliteit}{60}%
\contentsline {section}{\numberline {C.7}Projectorganisatie}{61}%
\contentsline {section}{\numberline {C.8}Planning}{63}%
\contentsline {section}{\numberline {C.9}Kosten en baten}{64}%
\contentsline {section}{\numberline {C.10}Risico's}{64}%
\contentsline {chapter}{\numberline {D.}Acceptatie test}{70}%
\contentsline {section}{\numberline {D.1}Voorbereidingen acceptatietest}{70}%
\contentsline {subsection}{\numberline {D.1.1}Visuele testomgeving}{70}%
\contentsline {subsubsection}{Hardware}{70}%
\contentsline {subsubsection}{Software}{71}%
\contentsline {subsection}{\numberline {D.1.2}Testomgeving seriële verbinding}{71}%
\contentsline {subsubsection}{Hardware}{71}%
\contentsline {subsubsection}{Software}{71}%
\contentsline {subsubsection}{Overig}{71}%
\contentsline {subsection}{\numberline {D.1.3}Test mesh}{71}%
\contentsline {subsubsection}{Hardware}{71}%
\contentsline {subsubsection}{Software}{71}%
\contentsline {subsection}{\numberline {D.1.4}Test data opslag}{72}%
\contentsline {subsubsection}{Hardware}{72}%
\contentsline {subsubsection}{Software}{72}%
\contentsline {subsection}{\numberline {D.1.5}Test software update}{72}%
\contentsline {subsubsection}{Hardware}{72}%
\contentsline {section}{\numberline {D.2}Acceptatietestbeschrijvingen}{73}%
\contentsline {subsection}{\numberline {D.2.1}Test Bluetooth naar serieel}{73}%
\contentsline {subsubsection}{Testcase 1.1: Commando's io-net}{73}%
\contentsline {subsubsection}{Testcase 1.2: Draaien motor}{74}%
\contentsline {subsection}{\numberline {D.2.2}Test Visuele feedback}{75}%
\contentsline {subsubsection}{Testcase 2.1: Test LED}{75}%
\contentsline {subsubsection}{Testcase 2.2: Test maximale afstand}{75}%
\contentsline {subsection}{\numberline {D.2.3}Test opgeslagen data}{76}%
\contentsline {subsubsection}{Testcase 3.1: Test Opslag data}{76}%
\contentsline {subsection}{\numberline {D.2.4}Test software update}{77}%
\contentsline {section}{\numberline {D.3}Traceerbaarheid klanteisen}{77}%
\contentsline {chapter}{\numberline {E.}Integratietest}{78}%
\contentsline {section}{\numberline {E.1}Testen}{78}%
\contentsline {subsection}{\numberline {E.1.1}Test 1: }{78}%
