\babel@toc {dutch}{}
\contentsline {chapter}{\numberline {1.}Inleiding}{13}%
\contentsline {section}{\numberline {1.1}Bedrijfsachtergrond}{13}%
\contentsline {section}{\numberline {1.2}Probleemstelling}{16}%
\contentsline {section}{\numberline {1.3}Doelstelling}{17}%
\contentsline {chapter}{\numberline {2.}Onderzoek}{18}%
\contentsline {chapter}{\numberline {3.}Requirements}{19}%
\contentsline {section}{\numberline {3.1}Systeemcontext}{19}%
\contentsline {subsection}{\numberline {3.1.1}Systeemfuncties}{19}%
\contentsline {section}{\numberline {3.2}Systeemfuncties}{21}%
\contentsline {subsection}{\numberline {3.2.1}Communicatie Bluetooth }{21}%
\contentsline {subsubsection}{Stimulus en response}{21}%
\contentsline {subsubsection}{Functionele klanteisen}{21}%
\contentsline {subsection}{\numberline {3.2.2}Communicatie met IM60/IM125}{22}%
\contentsline {subsubsection}{Stimulus en response}{22}%
\contentsline {subsubsection}{Functionele klanteisen}{22}%
\contentsline {subsection}{\numberline {3.2.3}Opslaan data}{23}%
\contentsline {subsubsection}{Stimulus en response}{23}%
\contentsline {subsubsection}{Functionele klanteisen}{23}%
\contentsline {subsection}{\numberline {3.2.4}Identificeren systeem}{23}%
\contentsline {subsubsection}{Stimulus en response}{23}%
\contentsline {subsubsection}{Functionele klanteisen}{23}%
\contentsline {section}{\numberline {3.3}Niet-functionele klanteisen}{24}%
\contentsline {chapter}{\numberline {4.}Architectuur}{25}%
\contentsline {section}{\numberline {4.1}Ontwerpbesluiten}{26}%
\contentsline {section}{\numberline {4.2}Deelsysteem 1: Control mesh}{26}%
\contentsline {section}{\numberline {4.3}Identificeren Systeem}{30}%
\contentsline {section}{\numberline {4.4}Opslaan gegevens}{31}%
\contentsline {chapter}{\numberline {5.}Detailontwerp}{32}%
\contentsline {section}{\numberline {5.1}Unit 1: Microcontroller}{33}%
\contentsline {section}{\numberline {5.2}Deelsysteem 1.2: Control saved data}{34}%
\contentsline {subsection}{\numberline {5.2.1}Software}{35}%
\contentsline {section}{\numberline {5.3}Deelsysteem 1.3: Communicate Motor Control Unit}{38}%
\contentsline {subsection}{\numberline {5.3.1}TX}{39}%
\contentsline {subsection}{\numberline {5.3.2}RX}{41}%
\contentsline {section}{\numberline {5.4}Unit 2: Bluetooth mesh}{43}%
\contentsline {subsection}{\numberline {5.4.1}Node}{43}%
\contentsline {subsection}{\numberline {5.4.2}Model}{44}%
\contentsline {subsection}{\numberline {5.4.3}Proxy}{45}%
\contentsline {subsection}{\numberline {5.4.4}Bereik}{46}%
\contentsline {section}{\numberline {5.5}Unit 3: LED schakeling}{48}%
\contentsline {chapter}{\numberline {6.}Realisatie}{55}%
\contentsline {section}{\numberline {6.1}Realisatie Hardware}{55}%
\contentsline {section}{\numberline {6.2}Software}{58}%
\contentsline {section}{\numberline {6.3}Solderen}{59}%
\contentsline {section}{\numberline {6.4}Componenten}{59}%
\contentsline {section}{\numberline {6.5}Program connector}{60}%
\contentsline {chapter}{\numberline {7.}Conclusie}{61}%
\contentsline {chapter}{\numberline {8.}Aanbevelingen}{62}%
\contentsline {section}{\numberline {8.1}Module}{62}%
\contentsline {section}{\numberline {8.2}Plaatsing op IM60/IM125}{63}%
\contentsline {chapter}{Appendices}{64}%
\contentsline {chapter}{\numberline {A.}Onderzoek Bluetooth mesh}{65}%
\contentsline {section}{\numberline {A.1}Inleiding}{66}%
\contentsline {section}{\numberline {A.2}Bluetooth Low Energie}{66}%
\contentsline {paragraph}{Physical layer}{67}%
\contentsline {paragraph}{Link layer}{67}%
\contentsline {paragraph}{L2CAP}{67}%
\contentsline {paragraph}{GAP}{67}%
\contentsline {paragraph}{Attribute Protocol}{68}%
\contentsline {paragraph}{GATT}{69}%
\contentsline {section}{\numberline {A.3}Bluetooth mesh}{69}%
\contentsline {paragraph}{Bearer Layer}{70}%
\contentsline {paragraph}{Network Layer}{70}%
\contentsline {paragraph}{Lower Transport Layer}{70}%
\contentsline {paragraph}{Upper Transport Layer}{70}%
\contentsline {paragraph}{Acces Layer}{70}%
\contentsline {paragraph}{Foundation Model Layer}{70}%
\contentsline {paragraph}{Model layer}{70}%
\contentsline {section}{\numberline {A.4}Test mesh}{72}%
\contentsline {section}{\numberline {A.5}Conclusie}{73}%
\contentsline {chapter}{\numberline {B.}Onderzoek LED}{74}%
\contentsline {section}{\numberline {B.1}DC-DC conversie}{74}%
\contentsline {subsection}{\numberline {B.1.1}Charge pump}{75}%
\contentsline {subsubsection}{Simulatie en opbouw Charge pump}{77}%
\contentsline {subsubsection}{BOM charge pump}{81}%
\contentsline {subsection}{\numberline {B.1.2}Boost Converter}{82}%
\contentsline {subsection}{\numberline {B.1.3}Schakelen LED}{85}%
\contentsline {subsubsection}{Schakelen met transistor}{85}%
\contentsline {subsection}{\numberline {B.1.4}Schakelen met FET}{87}%
\contentsline {section}{\numberline {B.2}Berekeningen gekozen FET}{88}%
\contentsline {section}{\numberline {B.3}Conclusie}{89}%
\contentsline {chapter}{\numberline {C.}Onderzoek draadloze technologieën}{90}%
\contentsline {section}{\numberline {C.1}Inleiding}{90}%
\contentsline {section}{\numberline {C.2}Draadloze technologieën}{92}%
\contentsline {subsection}{\numberline {C.2.1}Frequentie, bereik en maximale snelheid}{93}%
\contentsline {subsection}{\numberline {C.2.2}Topologie en gebruik smartphone}{94}%
\contentsline {subsection}{\numberline {C.2.3}Kosten}{95}%
\contentsline {section}{\numberline {C.3}Conclusie}{96}%
\contentsline {chapter}{\numberline {D.}Plan Van Aanpak}{97}%
\contentsline {section}{\numberline {D.1}Achtergronden}{98}%
\contentsline {section}{\numberline {D.2}Projectresultaat}{98}%
\contentsline {section}{\numberline {D.4}Projectgrenzen en randvoorwaarden}{102}%
\contentsline {section}{\numberline {D.5}Tussenresultaten}{102}%
\contentsline {section}{\numberline {D.6}Kwaliteit}{103}%
\contentsline {section}{\numberline {D.7}Projectorganisatie}{104}%
\contentsline {section}{\numberline {D.8}Planning}{105}%
\contentsline {section}{\numberline {D.9}Kosten en baten}{108}%
\contentsline {section}{\numberline {D.10}Risico's}{108}%
\contentsline {chapter}{\numberline {E.}Volledige keuze verantwoording draadloze microcontroller}{111}%
\contentsline {paragraph}{Nordic}{111}%
\contentsline {paragraph}{ST}{111}%
\contentsline {paragraph}{NXP}{111}%
\contentsline {paragraph}{TI}{112}%
\contentsline {paragraph}{Silicon Labs}{112}%
\contentsline {paragraph}{Conclusie}{112}%
\contentsline {chapter}{\numberline {F.}Acceptatie test}{113}%
\contentsline {section}{\numberline {F.1}Voorbereidingen acceptatietest}{113}%
\contentsline {subsection}{\numberline {F.1.1}Visuele testomgeving}{113}%
\contentsline {subsubsection}{Hardware}{113}%
\contentsline {subsubsection}{Software}{114}%
\contentsline {subsection}{\numberline {F.1.2}Testomgeving seriële verbinding}{114}%
\contentsline {subsubsection}{Hardware}{114}%
\contentsline {subsubsection}{Software}{114}%
\contentsline {subsubsection}{Overig}{114}%
\contentsline {subsection}{\numberline {F.1.3}Test mesh}{114}%
\contentsline {subsubsection}{Hardware}{114}%
\contentsline {subsubsection}{Software}{114}%
\contentsline {subsection}{\numberline {F.1.4}Test data opslag}{115}%
\contentsline {subsubsection}{Hardware}{115}%
\contentsline {subsubsection}{Software}{115}%
\contentsline {subsection}{\numberline {F.1.5}Test software update}{115}%
\contentsline {subsubsection}{Hardware}{115}%
\contentsline {section}{\numberline {F.2}Acceptatietestbeschrijvingen}{116}%
\contentsline {subsection}{\numberline {F.2.1}Test Bluetooth naar serieel}{116}%
\contentsline {subsubsection}{Testcase 1.1: Commando's io-net}{116}%
\contentsline {subsubsection}{Testcase 1.2: Draaien motor}{117}%
\contentsline {subsection}{\numberline {F.2.2}Test Visuele feedback}{118}%
\contentsline {subsubsection}{Testcase 2.1: Test LED}{118}%
\contentsline {subsubsection}{Testcase 2.2: Test maximale afstand}{119}%
\contentsline {subsection}{\numberline {F.2.3}Test opgeslagen data}{120}%
\contentsline {subsubsection}{Testcase 3.1: Test Opslag data}{120}%
\contentsline {subsection}{\numberline {F.2.4}Test software update}{120}%
\contentsline {subsection}{\numberline {F.2.5}Test RoHS}{121}%
\contentsline {section}{\numberline {F.3}Traceerbaarheid klanteisen}{122}%
\contentsline {chapter}{\numberline {G.}Integratietest}{123}%
\contentsline {section}{\numberline {G.1}Testen}{123}%
\contentsline {subsection}{\numberline {G.1.1}Test IC1: zenden UART}{124}%
\contentsline {subsection}{\numberline {G.1.2}Test IC1: ontvangen UART}{125}%
\contentsline {subsection}{\numberline {G.1.3}Test IC2: Hardware software update}{126}%
\contentsline {subsection}{\numberline {G.1.4}Test IC3: rimpel voeding}{127}%
\contentsline {subsection}{\numberline {G.1.5}Test IC4: Extern Flash}{128}%
\contentsline {subsection}{\numberline {G.1.6}Test IC5: mesh LED}{129}%
\contentsline {subsection}{\numberline {G.1.7}Test IC5: mesh UART}{130}%
\contentsline {subsection}{\numberline {G.1.8}Test IC6: LED}{131}%
\contentsline {chapter}{\numberline {H.}Unit test}{132}%
\contentsline {section}{\numberline {H.1}Test LED}{132}%
\contentsline {section}{\numberline {H.2}Test stroom LED}{133}%
\contentsline {section}{\numberline {H.3}Test UART zenden}{133}%
\contentsline {chapter}{\numberline {I.}Testresultaten}{134}%
\contentsline {section}{\numberline {I.1}Unittest}{134}%
\contentsline {section}{\numberline {I.2}Integratietest}{138}%
\contentsline {subsection}{\numberline {I.2.1}Test rimpel}{138}%
\contentsline {subsection}{\numberline {I.2.2}Test mesh LED}{139}%
\contentsline {subsection}{\numberline {I.2.3}Test mesh UART}{140}%
\contentsline {section}{\numberline {I.3}Acceptatietest}{141}%
\contentsline {subsection}{\numberline {I.3.1}Testcase 1.1 en testcase 1.2}{141}%
\contentsline {subsection}{\numberline {I.3.2}Testcase 2.1}{142}%
\contentsline {section}{\numberline {I.4}Testcase 2.2}{143}%
\contentsline {section}{\numberline {I.5}Testcase 3.1}{144}%
\contentsline {section}{\numberline {I.6}Testcase 5.1}{144}%
\contentsline {chapter}{\numberline {J.}Bill Of Materials}{145}%
\contentsline {chapter}{\numberline {K.}Handleiding BlueFarm}{147}%
\contentsline {section}{\numberline {K.1}Programmeren}{147}%
\contentsline {section}{\numberline {K.2}Provisioneren}{148}%
\contentsline {section}{\numberline {K.3}Probleem oplossen}{150}%
\contentsline {chapter}{\numberline {L.}Competentieverantwoording}{151}%
\contentsline {section}{\numberline {L.1}Analyseren}{151}%
\contentsline {section}{\numberline {L.2}Ontwerpen}{152}%
\contentsline {section}{\numberline {L.3}Realiseren}{152}%
\contentsline {section}{\numberline {L.4}Beheren}{152}%
\contentsline {section}{\numberline {L.5}Managen}{152}%
\contentsline {section}{\numberline {L.6}Adviseren}{153}%
\contentsline {section}{\numberline {L.7}Onderzoeken}{153}%
\contentsline {section}{\numberline {L.8}Professionaliseren}{153}%
\contentsline {chapter}{\numberline {M.}Reflectie}{154}%
\contentsline {section}{\numberline {M.1}Analyseren}{154}%
\contentsline {section}{\numberline {M.2}Ontwerpen}{154}%
\contentsline {section}{\numberline {M.3}Realiseren}{154}%
\contentsline {section}{\numberline {M.4}Beheren}{155}%
\contentsline {section}{\numberline {M.5}Managen}{155}%
\contentsline {section}{\numberline {M.6}Adviseren}{155}%
\contentsline {section}{\numberline {M.7}Onderzoeken}{155}%
\contentsline {section}{\numberline {M.8}Professionaliseren}{155}%
\contentsline {chapter}{\numberline {N.}Beoordeling bedrijfsbegeleider}{156}%
\contentsline {chapter}{\numberline {O.}Code}{157}%
\contentsline {section}{\numberline {O.1}Main.c}{157}%
\contentsline {section}{\numberline {O.2}UART\_debug.c}{164}%
\contentsline {section}{Literatuur}{166}%
