\babel@toc {dutch}{}
\contentsline {chapter}{\numberline {1.}Inleiding}{9}%
\contentsline {section}{\numberline {1.1}Bedrijfsachtergrond}{9}%
\contentsline {section}{\numberline {1.2}Probleemstelling}{12}%
\contentsline {section}{\numberline {1.3}Doelstelling}{13}%
\contentsline {chapter}{\numberline {2.}Requirements}{14}%
\contentsline {section}{\numberline {2.1}Inleiding}{14}%
\contentsline {section}{\numberline {2.2}Algemene beschrijving}{14}%
\contentsline {section}{\numberline {2.3}Systeemcontext}{14}%
\contentsline {subsection}{\numberline {2.3.1}Systeemfuncties}{14}%
\contentsline {section}{\numberline {2.4}Systeemfuncties}{16}%
\contentsline {subsection}{\numberline {2.4.1}Draadloze data overdracht}{16}%
\contentsline {subsubsection}{Stimulus en response}{16}%
\contentsline {subsubsection}{Functionele klanteisen}{16}%
\contentsline {subsection}{\numberline {2.4.2}Uitlezen en opslag data}{17}%
\contentsline {subsubsection}{Stimulus en response}{17}%
\contentsline {subsubsection}{Functionele klanteisen}{17}%
\contentsline {subsection}{\numberline {2.4.3}Updaten systeem en bestaande module (Optioneel)}{18}%
\contentsline {subsubsection}{Stimulus en response}{18}%
\contentsline {subsubsection}{Functionele klanteisen}{18}%
\contentsline {section}{\numberline {2.5}Niet-functionele klanteisen}{18}%
\contentsline {chapter}{\numberline {3.}Architectuur}{19}%
\contentsline {section}{\numberline {3.1}Ontwerpbesluiten}{19}%
\contentsline {section}{\numberline {3.2}Deelsystemen}{20}%
\contentsline {chapter}{\numberline {4.}Detailontwerp}{21}%
\contentsline {subsection}{\numberline {4.0.1}Deelsystemen 1 \& 2}{21}%
\contentsline {subsubsection}{Hardware deelsystemen 1 \& 2}{21}%
\contentsline {paragraph}{Nordic}{21}%
\contentsline {paragraph}{ST}{21}%
\contentsline {paragraph}{NXP}{22}%
\contentsline {paragraph}{TI}{22}%
\contentsline {paragraph}{Silicon Labs}{22}%
\contentsline {paragraph}{Conclusie}{22}%
\contentsline {subsubsection}{Software deelsysteem 1}{24}%
\contentsline {subsubsection}{Software deelsysteem 2}{24}%
\contentsline {chapter}{Appendices}{25}%
\contentsline {chapter}{\numberline {A.}Onderzoek draadloze technologieën}{26}%
\contentsline {section}{\numberline {A.1}Inleiding}{26}%
\contentsline {section}{\numberline {A.2}Draadloze technologieën}{28}%
\contentsline {subsection}{\numberline {A.2.1}Frequentie, bereik en maximale snelheid}{29}%
\contentsline {subsection}{\numberline {A.2.2}Topologie en gebruik smartphone}{30}%
\contentsline {subsection}{\numberline {A.2.3}Kosten}{31}%
\contentsline {section}{\numberline {A.3}Conclusie}{32}%
\contentsline {chapter}{\numberline {B.}Plan Van Aanpak}{33}%
\contentsline {section}{\numberline {B.1}Achtergronden}{35}%
\contentsline {subsection}{\numberline {B.1.1}De opdrachtgever}{35}%
\contentsline {subsection}{\numberline {B.1.2}De opdrachtnemers}{35}%
\contentsline {subsection}{\numberline {B.1.3}Aanleiding}{35}%
\contentsline {section}{\numberline {B.2}Projectresultaat}{35}%
\contentsline {subsection}{\numberline {B.2.1}Doelstelling}{35}%
\contentsline {subsection}{\numberline {B.2.2}Resultaat}{35}%
\contentsline {section}{\numberline {B.3}Projectactiviteiten}{36}%
\contentsline {subsection}{\numberline {B.3.1}Projectmanagement}{36}%
\contentsline {subsection}{\numberline {B.3.2}Projectmanagement}{36}%
\contentsline {section}{\numberline {B.4}Projectgrenzen en randvoorwaarden}{38}%
\contentsline {subsection}{\numberline {B.4.1}Projectomvang}{38}%
\contentsline {subsection}{\numberline {B.4.2}Randvoorwaarden}{38}%
\contentsline {section}{\numberline {B.5}Tussenresultaten}{38}%
\contentsline {subsection}{\numberline {B.5.1}Tussenresultaten in documentvorm: }{38}%
\contentsline {subsection}{\numberline {B.5.2}Tussenresultaten in de vorm van een deelsysteem }{38}%
\contentsline {section}{\numberline {B.6}Kwaliteit}{39}%
\contentsline {subsection}{\numberline {B.6.1}Kwaliteitsbewaking eindproduct}{39}%
\contentsline {subsection}{\numberline {B.6.2}Tussenresultaten}{39}%
\contentsline {subsection}{\numberline {B.6.3}Documenten}{39}%
\contentsline {subsection}{\numberline {B.6.4}Afwijkingen}{39}%
\contentsline {section}{\numberline {B.7}Projectorganisatie}{40}%
\contentsline {subsection}{\numberline {B.7.1}Rolverdeling met persoonlijke gegevens}{40}%
\contentsline {subsection}{\numberline {B.7.2}Functieomschrijving}{40}%
\contentsline {subsection}{\numberline {B.7.3}Informatie}{40}%
\contentsline {subsection}{\numberline {B.7.4}Coördinatie}{40}%
\contentsline {section}{\numberline {B.8}Planning}{41}%
\contentsline {section}{\numberline {B.9}Kosten en baten}{44}%
\contentsline {subsection}{\numberline {B.9.1}Kosten voor hulpmiddelen}{44}%
\contentsline {subsection}{\numberline {B.9.2}Arbeidskosten uitgedrukt in werktijd}{44}%
\contentsline {subsection}{\numberline {B.9.3}Baten}{44}%
\contentsline {section}{\numberline {B.10}Risico's}{44}%
\contentsline {subsection}{\numberline {B.10.1}Interne risico’s}{44}%
\contentsline {subsection}{\numberline {B.10.2}Externe risico’s}{44}%
\contentsline {subsection}{\numberline {B.10.3}Externe risico’s}{45}%
\contentsline {chapter}{\numberline {C.}Acceptatie test}{47}%
\contentsline {section}{\numberline {C.1}Voorbereidingen acceptatietest}{47}%
\contentsline {subsection}{\numberline {C.1.1}Visuele testomgeving}{47}%
\contentsline {subsubsection}{Hardware}{47}%
\contentsline {subsubsection}{Software}{47}%
\contentsline {subsection}{\numberline {C.1.2}Testomgeving seriële verbinding}{48}%
\contentsline {subsubsection}{Hardware}{48}%
\contentsline {subsubsection}{Software}{48}%
\contentsline {subsubsection}{Overig}{48}%
\contentsline {subsection}{\numberline {C.1.3}Test mesh}{48}%
\contentsline {subsubsection}{Hardware}{48}%
\contentsline {subsubsection}{Software}{48}%
\contentsline {subsection}{\numberline {C.1.4}Test data opslag}{49}%
\contentsline {subsubsection}{Hardware}{49}%
\contentsline {subsubsection}{Software}{49}%
\contentsline {subsection}{\numberline {C.1.5}Test software update}{49}%
\contentsline {subsubsection}{Hardware}{49}%
\contentsline {section}{\numberline {C.2}Acceptatietestbeschrijvingen}{50}%
\contentsline {subsection}{\numberline {C.2.1}Test Bluetooth naar serieel}{50}%
\contentsline {subsubsection}{Testcase 1.1: Commando's io-net}{50}%
\contentsline {subsubsection}{Testcase 1.2: Draaien motor}{51}%
\contentsline {subsection}{\numberline {C.2.2}Test Visuele feedback}{52}%
\contentsline {subsubsection}{Testcase 2.1: Test LED}{52}%
\contentsline {subsubsection}{Testcase 2.2: Test maximale afstand}{52}%
\contentsline {subsection}{\numberline {C.2.3}Test opgeslagen data}{53}%
\contentsline {subsubsection}{Testcase 3.1: Test Opslag data}{53}%
\contentsline {subsection}{\numberline {C.2.4}Test software update}{54}%
\contentsline {section}{Literatuur}{55}%
