\babel@toc {dutch}{}
\contentsline {chapter}{\numberline {1.}Inleiding}{10}%
\contentsline {section}{\numberline {1.1}Bedrijfsachtergrond}{10}%
\contentsline {section}{\numberline {1.2}Probleemstelling}{13}%
\contentsline {section}{\numberline {1.3}Doelstelling}{14}%
\contentsline {chapter}{\numberline {2.}Requirements}{15}%
\contentsline {section}{\numberline {2.1}Inleiding}{15}%
\contentsline {section}{\numberline {2.2}Algemene beschrijving}{15}%
\contentsline {section}{\numberline {2.3}Systeemcontext}{15}%
\contentsline {subsection}{\numberline {2.3.1}Systeemfuncties}{15}%
\contentsline {section}{\numberline {2.4}Systeemfuncties}{17}%
\contentsline {subsection}{\numberline {2.4.1}Draadloze data overdracht}{17}%
\contentsline {subsubsection}{Stimulus en response}{17}%
\contentsline {subsubsection}{Functionele klanteisen}{17}%
\contentsline {subsection}{\numberline {2.4.2}Uitlezen en opslag data}{18}%
\contentsline {subsubsection}{Stimulus en response}{18}%
\contentsline {subsubsection}{Functionele klanteisen}{18}%
\contentsline {subsection}{\numberline {2.4.3}Updaten systeem en bestaande module (Optioneel)}{19}%
\contentsline {subsubsection}{Stimulus en response}{19}%
\contentsline {subsubsection}{Functionele klanteisen}{19}%
\contentsline {section}{\numberline {2.5}Niet-functionele klanteisen}{19}%
\contentsline {chapter}{\numberline {3.}Architectuur}{20}%
\contentsline {section}{\numberline {3.1}Ontwerpbesluiten}{20}%
\contentsline {section}{\numberline {3.2}Deelsystemen en koppelingen}{21}%
\contentsline {section}{\numberline {3.3}Beschrijving deelsystemen}{22}%
\contentsline {subsection}{\numberline {3.3.1}Deelsysteem 1: Control Data}{23}%
\contentsline {subsubsection}{Deelsysteem 1.1: Execute functions}{24}%
\contentsline {subsubsection}{Deelsysteem 1.2. Control Saved Data}{24}%
\contentsline {subsubsection}{Deelsysteem 1.3. Communicate Motor Control Unit}{24}%
\contentsline {subsection}{\numberline {3.3.2}Deelsysteem 2: Transeive Data}{25}%
\contentsline {subsection}{\numberline {3.3.3}Deelsysteem 3: Flash Light}{25}%
\contentsline {section}{\numberline {3.4}Beschrijving interconnects}{25}%
\contentsline {section}{\numberline {3.5}Traceerbaarheid klanteisen}{26}%
\contentsline {chapter}{\numberline {4.}Detailontwerp}{27}%
\contentsline {section}{\numberline {4.1}Deelsystemen 1 \& 2}{27}%
\contentsline {subsection}{\numberline {4.1.1}Hardware deelsystemen 1 \& 2}{27}%
\contentsline {paragraph}{Nordic}{27}%
\contentsline {paragraph}{ST}{27}%
\contentsline {paragraph}{NXP}{28}%
\contentsline {paragraph}{TI}{28}%
\contentsline {paragraph}{Silicon Labs}{28}%
\contentsline {paragraph}{Conclusie}{28}%
\contentsline {section}{\numberline {4.2}Deelsysteem 1.2: Control saved data}{34}%
\contentsline {subsubsection}{Software deelsysteem 1}{35}%
\contentsline {subsubsection}{Software deelsysteem 2}{35}%
\contentsline {section}{\numberline {4.3}Unit 2: Transeive data}{36}%
\contentsline {section}{\numberline {4.4}Unit 3: LED schakeling}{39}%
\contentsline {chapter}{\numberline {5.}Realisatie}{40}%
\contentsline {section}{\numberline {5.1}Realisatie Hardware}{40}%
\contentsline {subsection}{\numberline {5.1.1}Componenten}{43}%
\contentsline {chapter}{Appendices}{44}%
\contentsline {chapter}{\numberline {A.}Onderzoek Bluetooth mesh}{45}%
\contentsline {section}{\numberline {A.1}Inleiding}{46}%
\contentsline {section}{\numberline {A.2}Bleutooth Low Energie}{46}%
\contentsline {paragraph}{Physical layer}{47}%
\contentsline {paragraph}{Link layer}{47}%
\contentsline {paragraph}{L2CAP}{47}%
\contentsline {paragraph}{GAP}{47}%
\contentsline {paragraph}{Attribute Protocol}{48}%
\contentsline {paragraph}{GATT}{49}%
\contentsline {section}{\numberline {A.3}Bluetooth mesh}{49}%
\contentsline {paragraph}{Bearer Layer}{50}%
\contentsline {paragraph}{Network Layer}{50}%
\contentsline {paragraph}{Lower Transport Layer}{50}%
\contentsline {paragraph}{Upper Transport Layer}{50}%
\contentsline {paragraph}{Acces Layer}{50}%
\contentsline {paragraph}{Foundation Model Layer}{50}%
\contentsline {paragraph}{Model layer}{50}%
\contentsline {section}{\numberline {A.4}Test mesh}{52}%
\contentsline {section}{\numberline {A.5}Conclusie}{53}%
\contentsline {chapter}{\numberline {B.}Onderzoek LED}{54}%
\contentsline {section}{\numberline {B.1}DC-DC conversie}{54}%
\contentsline {subsection}{\numberline {B.1.1}Charge pump}{55}%
\contentsline {subsubsection}{Simulatie en opbouw Charge pump}{57}%
\contentsline {subsubsection}{BOM charge pump}{61}%
\contentsline {subsection}{\numberline {B.1.2}Boost Converter}{62}%
\contentsline {subsection}{\numberline {B.1.3}Schakelen LED}{66}%
\contentsline {subsubsection}{Schakelen met transistor}{66}%
\contentsline {subsection}{\numberline {B.1.4}Schakelen met FET}{68}%
\contentsline {section}{\numberline {B.2}Conclusie}{69}%
\contentsline {chapter}{\numberline {C.}Onderzoek draadloze technologieën}{70}%
\contentsline {section}{\numberline {C.1}Inleiding}{70}%
\contentsline {section}{\numberline {C.2}Draadloze technologieën}{72}%
\contentsline {subsection}{\numberline {C.2.1}Frequentie, bereik en maximale snelheid}{73}%
\contentsline {subsection}{\numberline {C.2.2}Topologie en gebruik smartphone}{74}%
\contentsline {subsection}{\numberline {C.2.3}Kosten}{75}%
\contentsline {section}{\numberline {C.3}Conclusie}{76}%
\contentsline {chapter}{\numberline {D.}Plan Van Aanpak}{77}%
\contentsline {section}{\numberline {D.1}Achtergronden}{78}%
\contentsline {section}{\numberline {D.2}Projectresultaat}{78}%
\contentsline {section}{\numberline {D.4}Projectgrenzen en randvoorwaarden}{82}%
\contentsline {section}{\numberline {D.5}Tussenresultaten}{82}%
\contentsline {section}{\numberline {D.6}Kwaliteit}{83}%
\contentsline {section}{\numberline {D.7}Projectorganisatie}{84}%
\contentsline {section}{\numberline {D.8}Planning}{86}%
\contentsline {section}{\numberline {D.9}Kosten en baten}{87}%
\contentsline {section}{\numberline {D.10}Risico's}{87}%
\contentsline {chapter}{\numberline {E.}Acceptatie test}{93}%
\contentsline {section}{\numberline {E.1}Voorbereidingen acceptatietest}{93}%
\contentsline {subsection}{\numberline {E.1.1}Visuele testomgeving}{93}%
\contentsline {subsubsection}{Hardware}{93}%
\contentsline {subsubsection}{Software}{94}%
\contentsline {subsection}{\numberline {E.1.2}Testomgeving seriële verbinding}{94}%
\contentsline {subsubsection}{Hardware}{94}%
\contentsline {subsubsection}{Software}{94}%
\contentsline {subsubsection}{Overig}{94}%
\contentsline {subsection}{\numberline {E.1.3}Test mesh}{94}%
\contentsline {subsubsection}{Hardware}{94}%
\contentsline {subsubsection}{Software}{94}%
\contentsline {subsection}{\numberline {E.1.4}Test data opslag}{95}%
\contentsline {subsubsection}{Hardware}{95}%
\contentsline {subsubsection}{Software}{95}%
\contentsline {subsection}{\numberline {E.1.5}Test software update}{95}%
\contentsline {subsubsection}{Hardware}{95}%
\contentsline {section}{\numberline {E.2}Acceptatietestbeschrijvingen}{96}%
\contentsline {subsection}{\numberline {E.2.1}Test Bluetooth naar serieel}{96}%
\contentsline {subsubsection}{Testcase 1.1: Commando's io-net}{96}%
\contentsline {subsubsection}{Testcase 1.2: Draaien motor}{97}%
\contentsline {subsection}{\numberline {E.2.2}Test Visuele feedback}{98}%
\contentsline {subsubsection}{Testcase 2.1: Test LED}{98}%
\contentsline {subsubsection}{Testcase 2.2: Test maximale afstand}{98}%
\contentsline {subsection}{\numberline {E.2.3}Test opgeslagen data}{99}%
\contentsline {subsubsection}{Testcase 3.1: Test Opslag data}{99}%
\contentsline {subsection}{\numberline {E.2.4}Test software update}{100}%
\contentsline {section}{\numberline {E.3}Traceerbaarheid klanteisen}{100}%
\contentsline {chapter}{\numberline {F.}Integratietest}{101}%
\contentsline {section}{\numberline {F.1}Testen}{101}%
\contentsline {subsection}{\numberline {F.1.1}Test rimpel voeding}{101}%
\contentsline {subsection}{\numberline {F.1.2}Test output spanning Charge-pump}{102}%
\contentsline {subsection}{\numberline {F.1.3}Test stroom LED}{103}%
\contentsline {subsection}{\numberline {F.1.4}Test zenden UART}{104}%
\contentsline {subsection}{\numberline {F.1.5}Test ontvangen UART}{104}%
\contentsline {subsection}{\numberline {F.1.6}Test mesh LED}{104}%
\contentsline {section}{Literatuur}{105}%
