\babel@toc {dutch}{}
\contentsline {chapter}{\numberline {1.}Inleiding}{8}%
\contentsline {section}{\numberline {1.1}Bedrijfsachtergrond}{8}%
\contentsline {section}{\numberline {1.2}Probleemstelling}{11}%
\contentsline {section}{\numberline {1.3}Doelstelling}{12}%
\contentsline {chapter}{\numberline {2.}Requirements}{13}%
\contentsline {section}{\numberline {2.1}Inleiding}{13}%
\contentsline {section}{\numberline {2.2}Algemene beschrijving}{13}%
\contentsline {section}{\numberline {2.3}Systeemcontext}{13}%
\contentsline {subsection}{\numberline {2.3.1}Systeemfuncties}{13}%
\contentsline {section}{\numberline {2.4}Systeemfuncties}{15}%
\contentsline {subsection}{\numberline {2.4.1}Draadloze data overdracht}{15}%
\contentsline {subsubsection}{Stimulus en response}{15}%
\contentsline {subsubsection}{Functionele klanteisen}{15}%
\contentsline {subsection}{\numberline {2.4.2}Uitlezen en opslag data}{16}%
\contentsline {subsubsection}{Stimulus en response}{16}%
\contentsline {subsubsection}{Functionele klanteisen}{16}%
\contentsline {subsection}{\numberline {2.4.3}Updaten systeem en bestaande module (Optioneel)}{17}%
\contentsline {subsubsection}{Stimulus en response}{17}%
\contentsline {subsubsection}{Functionele klanteisen}{17}%
\contentsline {section}{\numberline {2.5}Niet-functionele klanteisen}{17}%
\contentsline {chapter}{\numberline {3.}Architectuur}{18}%
\contentsline {chapter}{Appendices}{20}%
\contentsline {chapter}{\numberline {A.}Onderzoek draadloze technologieën}{21}%
\contentsline {section}{\numberline {A.1}Inleiding}{21}%
\contentsline {section}{\numberline {A.2}Draadloze technologieën}{23}%
\contentsline {subsection}{\numberline {A.2.1}Frequentie, bereik en maximale snelheid}{24}%
\contentsline {subsection}{\numberline {A.2.2}Topologie en gebruik smartphone}{25}%
\contentsline {subsection}{\numberline {A.2.3}Kosten}{26}%
\contentsline {subsection}{\numberline {A.2.4}Conclusie}{27}%
\contentsline {section}{Literatuur}{28}%
