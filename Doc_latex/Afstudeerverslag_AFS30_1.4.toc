\babel@toc {dutch}{}
\contentsline {chapter}{\numberline {1.}Inleiding}{13}%
\contentsline {section}{\numberline {1.1}Bedrijfsachtergrond}{13}%
\contentsline {section}{\numberline {1.2}Probleemstelling}{16}%
\contentsline {section}{\numberline {1.3}Doelstelling}{17}%
\contentsline {chapter}{\numberline {2.}Requirements}{18}%
\contentsline {section}{\numberline {2.1}Inleiding}{18}%
\contentsline {section}{\numberline {2.2}Algemene beschrijving}{18}%
\contentsline {section}{\numberline {2.3}Systeemcontext}{18}%
\contentsline {subsection}{\numberline {2.3.1}Systeemfuncties}{18}%
\contentsline {section}{\numberline {2.4}Systeemfuncties}{20}%
\contentsline {subsection}{\numberline {2.4.1}Draadloze data overdracht}{20}%
\contentsline {subsubsection}{Stimulus en response}{20}%
\contentsline {subsubsection}{Functionele klanteisen}{20}%
\contentsline {subsection}{\numberline {2.4.2}Uitlezen en opslag data}{21}%
\contentsline {subsubsection}{Stimulus en response}{21}%
\contentsline {subsubsection}{Functionele klanteisen}{21}%
\contentsline {subsection}{\numberline {2.4.3}Updaten systeem en bestaande module (Optioneel)}{22}%
\contentsline {subsubsection}{Stimulus en response}{22}%
\contentsline {subsubsection}{Functionele klanteisen}{22}%
\contentsline {section}{\numberline {2.5}Niet-functionele klanteisen}{22}%
\contentsline {chapter}{\numberline {3.}Architectuur}{23}%
\contentsline {section}{\numberline {3.1}Ontwerpbesluiten en randvoorwaarden}{23}%
\contentsline {section}{\numberline {3.2}Deelsystemen en koppelingen}{24}%
\contentsline {section}{\numberline {3.3}Beschrijving deelsystemen}{25}%
\contentsline {section}{\numberline {3.4}Beschrijving interconnects}{26}%
\contentsline {subsection}{\numberline {3.4.1}Deelsysteem 1: Control Data}{27}%
\contentsline {subsubsection}{Deelsysteem 1.1: Execute functions}{28}%
\contentsline {subsubsection}{Deelsysteem 1.2. Control Saved Data}{28}%
\contentsline {subsubsection}{Deelsysteem 1.3. Communicate Motor Control Unit}{28}%
\contentsline {subsection}{\numberline {3.4.2}Deelsysteem 2: Transceive Data}{29}%
\contentsline {subsection}{\numberline {3.4.3}Deelsysteem 3: Flash Light}{29}%
\contentsline {section}{\numberline {3.5}Traceerbaarheid klanteisen}{29}%
\contentsline {chapter}{\numberline {4.}Detailontwerp}{30}%
\contentsline {section}{\numberline {4.1}Deelsystemen 1 \& 2}{30}%
\contentsline {section}{\numberline {4.2}Deelsysteem 1.2: Control saved data}{35}%
\contentsline {subsection}{\numberline {4.2.1}Software}{36}%
\contentsline {section}{\numberline {4.3}Deelsysteem 1.3: Communicate Motor Control Unit}{39}%
\contentsline {subsection}{\numberline {4.3.1}TX}{40}%
\contentsline {subsection}{\numberline {4.3.2}RX}{42}%
\contentsline {section}{\numberline {4.4}Unit 2: Transceive data}{44}%
\contentsline {section}{\numberline {4.5}Unit 3: LED schakeling}{51}%
\contentsline {chapter}{\numberline {5.}Realisatie}{53}%
\contentsline {section}{\numberline {5.1}Realisatie Hardware}{53}%
\contentsline {section}{\numberline {5.2}Software}{56}%
\contentsline {section}{\numberline {5.3}Solderen}{57}%
\contentsline {section}{\numberline {5.4}Componenten}{57}%
\contentsline {section}{\numberline {5.5}Program connector}{58}%
\contentsline {chapter}{\numberline {6.}Conclusie}{59}%
\contentsline {chapter}{\numberline {7.}Aanbevelingen}{60}%
\contentsline {section}{\numberline {7.1}Module}{60}%
\contentsline {section}{\numberline {7.2}Plaatsing op IM60/IM125}{61}%
\contentsline {chapter}{Appendices}{62}%
\contentsline {chapter}{\numberline {A.}Onderzoek Bluetooth mesh}{63}%
\contentsline {section}{\numberline {A.1}Inleiding}{64}%
\contentsline {section}{\numberline {A.2}Bluetooth Low Energie}{64}%
\contentsline {paragraph}{Physical layer}{65}%
\contentsline {paragraph}{Link layer}{65}%
\contentsline {paragraph}{L2CAP}{65}%
\contentsline {paragraph}{GAP}{65}%
\contentsline {paragraph}{Attribute Protocol}{66}%
\contentsline {paragraph}{GATT}{67}%
\contentsline {section}{\numberline {A.3}Bluetooth mesh}{67}%
\contentsline {paragraph}{Bearer Layer}{68}%
\contentsline {paragraph}{Network Layer}{68}%
\contentsline {paragraph}{Lower Transport Layer}{68}%
\contentsline {paragraph}{Upper Transport Layer}{68}%
\contentsline {paragraph}{Acces Layer}{68}%
\contentsline {paragraph}{Foundation Model Layer}{68}%
\contentsline {paragraph}{Model layer}{68}%
\contentsline {section}{\numberline {A.4}Test mesh}{70}%
\contentsline {section}{\numberline {A.5}Conclusie}{71}%
\contentsline {chapter}{\numberline {B.}Onderzoek LED}{72}%
\contentsline {section}{\numberline {B.1}DC-DC conversie}{72}%
\contentsline {subsection}{\numberline {B.1.1}Charge pump}{73}%
\contentsline {subsubsection}{Simulatie en opbouw Charge pump}{75}%
\contentsline {subsubsection}{BOM charge pump}{79}%
\contentsline {subsection}{\numberline {B.1.2}Boost Converter}{80}%
\contentsline {subsection}{\numberline {B.1.3}Schakelen LED}{83}%
\contentsline {subsubsection}{Schakelen met transistor}{83}%
\contentsline {subsection}{\numberline {B.1.4}Schakelen met FET}{85}%
\contentsline {section}{\numberline {B.2}Berekeningen gekozen FET}{86}%
\contentsline {section}{\numberline {B.3}Conclusie}{87}%
\contentsline {chapter}{\numberline {C.}Onderzoek draadloze technologieën}{88}%
\contentsline {section}{\numberline {C.1}Inleiding}{88}%
\contentsline {section}{\numberline {C.2}Draadloze technologieën}{90}%
\contentsline {subsection}{\numberline {C.2.1}Frequentie, bereik en maximale snelheid}{91}%
\contentsline {subsection}{\numberline {C.2.2}Topologie en gebruik smartphone}{92}%
\contentsline {subsection}{\numberline {C.2.3}Kosten}{93}%
\contentsline {section}{\numberline {C.3}Conclusie}{94}%
\contentsline {chapter}{\numberline {D.}Plan Van Aanpak}{95}%
\contentsline {section}{\numberline {D.1}Achtergronden}{96}%
\contentsline {section}{\numberline {D.2}Projectresultaat}{96}%
\contentsline {section}{\numberline {D.4}Projectgrenzen en randvoorwaarden}{100}%
\contentsline {section}{\numberline {D.5}Tussenresultaten}{100}%
\contentsline {section}{\numberline {D.6}Kwaliteit}{101}%
\contentsline {section}{\numberline {D.7}Projectorganisatie}{102}%
\contentsline {section}{\numberline {D.8}Planning}{103}%
\contentsline {section}{\numberline {D.9}Kosten en baten}{106}%
\contentsline {section}{\numberline {D.10}Risico's}{106}%
\contentsline {chapter}{\numberline {E.}Volledige keuze verantwoording draadloze microcontroller}{109}%
\contentsline {paragraph}{Nordic}{109}%
\contentsline {paragraph}{ST}{109}%
\contentsline {paragraph}{NXP}{109}%
\contentsline {paragraph}{TI}{110}%
\contentsline {paragraph}{Silicon Labs}{110}%
\contentsline {paragraph}{Conclusie}{110}%
\contentsline {chapter}{\numberline {F.}Acceptatie test}{111}%
\contentsline {section}{\numberline {F.1}Voorbereidingen acceptatietest}{111}%
\contentsline {subsection}{\numberline {F.1.1}Visuele testomgeving}{111}%
\contentsline {subsubsection}{Hardware}{111}%
\contentsline {subsubsection}{Software}{112}%
\contentsline {subsection}{\numberline {F.1.2}Testomgeving seriële verbinding}{112}%
\contentsline {subsubsection}{Hardware}{112}%
\contentsline {subsubsection}{Software}{112}%
\contentsline {subsubsection}{Overig}{112}%
\contentsline {subsection}{\numberline {F.1.3}Test mesh}{112}%
\contentsline {subsubsection}{Hardware}{112}%
\contentsline {subsubsection}{Software}{112}%
\contentsline {subsection}{\numberline {F.1.4}Test data opslag}{113}%
\contentsline {subsubsection}{Hardware}{113}%
\contentsline {subsubsection}{Software}{113}%
\contentsline {subsection}{\numberline {F.1.5}Test software update}{113}%
\contentsline {subsubsection}{Hardware}{113}%
\contentsline {section}{\numberline {F.2}Acceptatietestbeschrijvingen}{114}%
\contentsline {subsection}{\numberline {F.2.1}Test Bluetooth naar serieel}{114}%
\contentsline {subsubsection}{Testcase 1.1: Commando's io-net}{114}%
\contentsline {subsubsection}{Testcase 1.2: Draaien motor}{115}%
\contentsline {subsection}{\numberline {F.2.2}Test Visuele feedback}{116}%
\contentsline {subsubsection}{Testcase 2.1: Test LED}{116}%
\contentsline {subsubsection}{Testcase 2.2: Test maximale afstand}{117}%
\contentsline {subsection}{\numberline {F.2.3}Test opgeslagen data}{118}%
\contentsline {subsubsection}{Testcase 3.1: Test Opslag data}{118}%
\contentsline {subsection}{\numberline {F.2.4}Test software update}{118}%
\contentsline {subsection}{\numberline {F.2.5}Test RoHS}{119}%
\contentsline {section}{\numberline {F.3}Traceerbaarheid klanteisen}{120}%
\contentsline {chapter}{\numberline {G.}Integratietest}{121}%
\contentsline {section}{\numberline {G.1}Testen}{121}%
\contentsline {subsection}{\numberline {G.1.1}Test IC1: zenden UART}{122}%
\contentsline {subsection}{\numberline {G.1.2}Test IC1: ontvangen UART}{123}%
\contentsline {subsection}{\numberline {G.1.3}Test IC2: Hardware software update}{124}%
\contentsline {subsection}{\numberline {G.1.4}Test IC3: rimpel voeding}{125}%
\contentsline {subsection}{\numberline {G.1.5}Test IC4: Extern Flash}{126}%
\contentsline {subsection}{\numberline {G.1.6}Test IC5: mesh LED}{127}%
\contentsline {subsection}{\numberline {G.1.7}Test IC5: mesh UART}{128}%
\contentsline {subsection}{\numberline {G.1.8}Test IC6: LED}{129}%
\contentsline {chapter}{\numberline {H.}Unit test}{130}%
\contentsline {section}{\numberline {H.1}Test LED}{130}%
\contentsline {section}{\numberline {H.2}Test stroom LED}{131}%
\contentsline {section}{\numberline {H.3}Test UART zenden}{131}%
\contentsline {chapter}{\numberline {I.}Testresultaten}{132}%
\contentsline {section}{\numberline {I.1}Unittest}{132}%
\contentsline {section}{\numberline {I.2}Integratietest}{136}%
\contentsline {subsection}{\numberline {I.2.1}Test rimpel}{136}%
\contentsline {subsection}{\numberline {I.2.2}Test mesh LED}{137}%
\contentsline {subsection}{\numberline {I.2.3}Test mesh UART}{138}%
\contentsline {section}{\numberline {I.3}Acceptatietest}{139}%
\contentsline {subsection}{\numberline {I.3.1}Testcase 1.1 en testcase 1.2}{139}%
\contentsline {subsection}{\numberline {I.3.2}Testcase 2.1}{140}%
\contentsline {section}{\numberline {I.4}Testcase 2.2}{141}%
\contentsline {section}{\numberline {I.5}Testcase 3.1}{142}%
\contentsline {section}{\numberline {I.6}Testcase 5.1}{142}%
\contentsline {chapter}{\numberline {J.}Bill Of Materials}{143}%
\contentsline {chapter}{\numberline {K.}Handleiding BlueFarm}{145}%
\contentsline {section}{\numberline {K.1}Programmeren}{145}%
\contentsline {section}{\numberline {K.2}Provisioneren}{146}%
\contentsline {section}{\numberline {K.3}Probleem oplossen}{148}%
\contentsline {chapter}{\numberline {L.}Competentieverantwoording}{149}%
\contentsline {section}{\numberline {L.1}Analyseren}{149}%
\contentsline {section}{\numberline {L.2}Ontwerpen}{150}%
\contentsline {section}{\numberline {L.3}Realiseren}{150}%
\contentsline {section}{\numberline {L.4}Beheren}{150}%
\contentsline {section}{\numberline {L.5}Managen}{150}%
\contentsline {section}{\numberline {L.6}Adviseren}{152}%
\contentsline {section}{\numberline {L.7}Onderzoeken}{152}%
\contentsline {section}{\numberline {L.8}Professionaliseren}{152}%
\contentsline {chapter}{\numberline {M.}Reflectie}{153}%
\contentsline {section}{\numberline {M.1}Analyseren}{153}%
\contentsline {section}{\numberline {M.2}Ontwerpen}{153}%
\contentsline {section}{\numberline {M.3}Realiseren}{153}%
\contentsline {section}{\numberline {M.4}Beheren}{153}%
\contentsline {section}{\numberline {M.5}Managen}{154}%
\contentsline {section}{\numberline {M.6}Adviseren}{154}%
\contentsline {section}{\numberline {M.7}Onderzoeken}{154}%
\contentsline {section}{\numberline {M.8}Professionaliseren}{154}%
\contentsline {section}{Literatuur}{155}%
