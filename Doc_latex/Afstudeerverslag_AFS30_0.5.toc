\babel@toc {dutch}{}
\contentsline {chapter}{\numberline {1.}Inleiding}{9}%
\contentsline {section}{\numberline {1.1}Bedrijfsachtergrond}{9}%
\contentsline {section}{\numberline {1.2}Probleemstelling}{12}%
\contentsline {section}{\numberline {1.3}Doelstelling}{13}%
\contentsline {chapter}{\numberline {2.}Requirements}{14}%
\contentsline {section}{\numberline {2.1}Inleiding}{14}%
\contentsline {section}{\numberline {2.2}Algemene beschrijving}{14}%
\contentsline {section}{\numberline {2.3}Systeemcontext}{14}%
\contentsline {subsection}{\numberline {2.3.1}Systeemfuncties}{14}%
\contentsline {section}{\numberline {2.4}Systeemfuncties}{16}%
\contentsline {subsection}{\numberline {2.4.1}Draadloze data overdracht}{16}%
\contentsline {subsubsection}{Stimulus en response}{16}%
\contentsline {subsubsection}{Functionele klanteisen}{16}%
\contentsline {subsection}{\numberline {2.4.2}Uitlezen en opslag data}{17}%
\contentsline {subsubsection}{Stimulus en response}{17}%
\contentsline {subsubsection}{Functionele klanteisen}{17}%
\contentsline {subsection}{\numberline {2.4.3}Updaten systeem en bestaande module (Optioneel)}{18}%
\contentsline {subsubsection}{Stimulus en response}{18}%
\contentsline {subsubsection}{Functionele klanteisen}{18}%
\contentsline {section}{\numberline {2.5}Niet-functionele klanteisen}{18}%
\contentsline {chapter}{\numberline {3.}Architectuur}{19}%
\contentsline {chapter}{Appendices}{21}%
\contentsline {chapter}{\numberline {A.}Onderzoek draadloze technologieën}{22}%
\contentsline {section}{\numberline {A.1}Inleiding}{22}%
\contentsline {section}{\numberline {A.2}Draadloze technologieën}{24}%
\contentsline {subsection}{\numberline {A.2.1}Frequentie, bereik en maximale snelheid}{25}%
\contentsline {subsection}{\numberline {A.2.2}Topologie en gebruik smartphone}{26}%
\contentsline {subsection}{\numberline {A.2.3}Kosten}{27}%
\contentsline {section}{\numberline {A.3}Conclusie}{28}%
\contentsline {chapter}{\numberline {B.}Plan Van Aanpak}{29}%
\contentsline {section}{\numberline {B.1}Achtergronden}{31}%
\contentsline {subsection}{\numberline {B.1.1}De opdrachtgever}{31}%
\contentsline {subsection}{\numberline {B.1.2}De opdrachtnemers}{31}%
\contentsline {subsection}{\numberline {B.1.3}Aanleiding}{31}%
\contentsline {section}{\numberline {B.2}Projectresultaat}{31}%
\contentsline {subsection}{\numberline {B.2.1}Doelstelling}{31}%
\contentsline {subsection}{\numberline {B.2.2}Resultaat}{31}%
\contentsline {section}{\numberline {B.3}Projectactiviteiten}{32}%
\contentsline {subsection}{\numberline {B.3.1}Projectmanagement}{32}%
\contentsline {subsection}{\numberline {B.3.2}Projectmanagement}{32}%
\contentsline {section}{\numberline {B.4}Projectgrenzen en randvoorwaarden}{34}%
\contentsline {subsection}{\numberline {B.4.1}Projectomvang}{34}%
\contentsline {subsection}{\numberline {B.4.2}Randvoorwaarden}{34}%
\contentsline {section}{\numberline {B.5}Tussenresultaten}{34}%
\contentsline {subsection}{\numberline {B.5.1}Tussenresultaten in documentvorm: }{34}%
\contentsline {subsection}{\numberline {B.5.2}Tussenresultaten in de vorm van een deelsysteem }{34}%
\contentsline {section}{\numberline {B.6}Kwaliteit}{35}%
\contentsline {subsection}{\numberline {B.6.1}Kwaliteitsbewaking eindproduct}{35}%
\contentsline {subsection}{\numberline {B.6.2}Tussenresultaten}{35}%
\contentsline {subsection}{\numberline {B.6.3}Documenten}{35}%
\contentsline {subsection}{\numberline {B.6.4}Afwijkingen}{35}%
\contentsline {section}{\numberline {B.7}Projectorganisatie}{36}%
\contentsline {subsection}{\numberline {B.7.1}Rolverdeling met persoonlijke gegevens}{36}%
\contentsline {subsection}{\numberline {B.7.2}Functieomschrijving}{36}%
\contentsline {subsection}{\numberline {B.7.3}Informatie}{36}%
\contentsline {subsection}{\numberline {B.7.4}Coördinatie}{36}%
\contentsline {section}{\numberline {B.8}Planning}{37}%
\contentsline {section}{\numberline {B.9}Kosten en baten}{40}%
\contentsline {subsection}{\numberline {B.9.1}Kosten voor hulpmiddelen}{40}%
\contentsline {subsection}{\numberline {B.9.2}Arbeidskosten uitgedrukt in werktijd}{40}%
\contentsline {subsection}{\numberline {B.9.3}Baten}{40}%
\contentsline {section}{\numberline {B.10}Risico's}{40}%
\contentsline {subsection}{\numberline {B.10.1}Interne risico’s}{40}%
\contentsline {subsection}{\numberline {B.10.2}Externe risico’s}{40}%
\contentsline {subsection}{\numberline {B.10.3}Externe risico’s}{41}%
\contentsline {chapter}{\numberline {C.}Acceptatie test}{43}%
\contentsline {section}{\numberline {C.1}Voorbereidingen acceptatietest}{43}%
\contentsline {subsection}{\numberline {C.1.1}Visuele testomgeving}{43}%
\contentsline {subsubsection}{Hardware}{43}%
\contentsline {subsubsection}{Software}{43}%
\contentsline {subsection}{\numberline {C.1.2}Testomgeving seriële verbinding}{44}%
\contentsline {subsubsection}{Hardware}{44}%
\contentsline {subsubsection}{Software}{44}%
\contentsline {subsubsection}{Overig}{44}%
\contentsline {subsection}{\numberline {C.1.3}Test mesh}{44}%
\contentsline {subsubsection}{Hardware}{44}%
\contentsline {subsubsection}{Software}{44}%
\contentsline {section}{\numberline {C.2}Acceptatietestbeschrijvingen}{45}%
\contentsline {subsection}{\numberline {C.2.1}Test Bluetooth naar serieel}{45}%
\contentsline {subsubsection}{Testcase 1.1: commando's}{45}%
\contentsline {section}{Literatuur}{46}%
