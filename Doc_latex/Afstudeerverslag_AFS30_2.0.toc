\babel@toc {dutch}{}
\contentsline {chapter}{\numberline {1.}Inleiding}{11}%
\contentsline {section}{\numberline {1.1}Bedrijfsachtergrond}{11}%
\contentsline {section}{\numberline {1.2}De klant}{11}%
\contentsline {section}{\numberline {1.3}Probleemstelling}{14}%
\contentsline {section}{\numberline {1.4}Doelstelling}{14}%
\contentsline {chapter}{\numberline {2.}Requirements}{17}%
\contentsline {section}{\numberline {2.1}Systeemcontext}{17}%
\contentsline {section}{\numberline {2.2}Functionele Systeemeisen}{18}%
\contentsline {subsection}{\numberline {2.2.1}Communicatie Bluetooth }{18}%
\contentsline {subsubsection}{Stimulus en response}{18}%
\contentsline {subsubsection}{Functionele klanteisen}{18}%
\contentsline {subsection}{\numberline {2.2.2}Communicatie met IM60/IM125}{19}%
\contentsline {subsubsection}{Stimulus en response}{19}%
\contentsline {subsubsection}{Functionele klanteisen}{19}%
\contentsline {subsection}{\numberline {2.2.3}Opslaan data}{20}%
\contentsline {subsubsection}{Stimulus en response}{20}%
\contentsline {subsubsection}{Functionele klanteisen}{20}%
\contentsline {subsection}{\numberline {2.2.4}Identificeren systeem}{20}%
\contentsline {subsubsection}{Stimulus en response}{20}%
\contentsline {subsubsection}{Functionele klanteisen}{20}%
\contentsline {section}{\numberline {2.3}Niet-functionele klanteisen}{20}%
\contentsline {chapter}{\numberline {3.}Architectuur}{21}%
\contentsline {section}{\numberline {3.1}Ontwerpbesluiten}{23}%
\contentsline {section}{\numberline {3.2}Deelsysteem 1: Control mesh}{23}%
\contentsline {section}{\numberline {3.3}Identificeren Systeem}{24}%
\contentsline {section}{\numberline {3.4}Opslaan gegevens}{25}%
\contentsline {chapter}{\numberline {4.}Detailontwerp}{26}%
\contentsline {section}{\numberline {4.1}Module}{28}%
\contentsline {subsubsection}{Return Loss}{29}%
\contentsline {subsection}{\numberline {4.1.1}Microcontroller}{30}%
\contentsline {subsection}{\numberline {4.1.2}Unit 1.2: UART IM60/IM125}{31}%
\contentsline {subsubsection}{TX}{32}%
\contentsline {subsubsection}{RX}{34}%
\contentsline {subsection}{\numberline {4.1.3}Unit 1.3: Bluetooth 5.0 mesh}{36}%
\contentsline {subsubsection}{Node}{36}%
\contentsline {subsubsection}{Model}{37}%
\contentsline {subsection}{\numberline {4.1.4}Unit 1.4: Proxy server}{38}%
\contentsline {subsection}{\numberline {4.1.5}Unit 1.5: Long Range}{39}%
\contentsline {section}{\numberline {4.2}Unit 2: Opslaan data}{40}%
\contentsline {subsection}{\numberline {4.2.1}Software}{41}%
\contentsline {section}{\numberline {4.3}Unit3: LED schakeling}{44}%
\contentsline {subsubsection}{Componentkeuze}{44}%
\contentsline {subsubsection}{Berekeningen charge pump}{46}%
\contentsline {subsubsection}{Simulatie}{50}%
\contentsline {chapter}{\numberline {5.}Testresultaten}{52}%
\contentsline {section}{\numberline {5.1}Test tussen smartphone en node}{52}%
\contentsline {section}{\numberline {5.2}Test doorsturen berichten}{53}%
\contentsline {section}{\numberline {5.3}Test bereik tussen nodes}{54}%
\contentsline {section}{\numberline {5.4}Smartphone aansturing UART}{55}%
\contentsline {section}{\numberline {5.5}Extern geheugen}{56}%
\contentsline {chapter}{\numberline {6.}Conclusie}{57}%
\contentsline {chapter}{\numberline {7.}Aanbevelingen}{58}%
\contentsline {section}{\numberline {7.1}Plaatsing op IM60/IM125}{58}%
\contentsline {chapter}{Appendices}{59}%
\contentsline {chapter}{\numberline {A.}Onderzoek draadloze technologieën}{60}%
\contentsline {section}{\numberline {A.1}Inleiding}{60}%
\contentsline {section}{\numberline {A.2}Draadloze technologieën}{62}%
\contentsline {subsection}{\numberline {A.2.1}Frequentie, bereik en maximale snelheid}{63}%
\contentsline {subsection}{\numberline {A.2.2}Topologie en gebruik smartphone}{64}%
\contentsline {subsection}{\numberline {A.2.3}Kosten}{64}%
\contentsline {section}{\numberline {A.3}Conclusie}{66}%
\contentsline {chapter}{\numberline {B.}Onderzoek Bluetooth mesh}{67}%
\contentsline {section}{\numberline {B.1}Inleiding}{68}%
\contentsline {section}{\numberline {B.2}Bluetooth Low Energie}{68}%
\contentsline {paragraph}{Physical layer}{69}%
\contentsline {paragraph}{Link layer}{69}%
\contentsline {paragraph}{L2CAP}{69}%
\contentsline {paragraph}{GAP}{69}%
\contentsline {paragraph}{Attribute Protocol}{69}%
\contentsline {paragraph}{GATT}{71}%
\contentsline {section}{\numberline {B.3}Bluetooth mesh}{71}%
\contentsline {paragraph}{Bearer Layer}{72}%
\contentsline {paragraph}{Network Layer}{72}%
\contentsline {paragraph}{Lower Transport Layer}{72}%
\contentsline {paragraph}{Upper Transport Layer}{72}%
\contentsline {paragraph}{Acces Layer}{72}%
\contentsline {paragraph}{Foundation Model Layer}{72}%
\contentsline {paragraph}{Model layer}{72}%
\contentsline {section}{\numberline {B.4}Test mesh}{73}%
\contentsline {section}{\numberline {B.5}Conclusie}{74}%
\contentsline {chapter}{\numberline {C.}Onderzoek LED}{75}%
\contentsline {section}{\numberline {C.1}DC-DC conversie}{76}%
\contentsline {subsection}{\numberline {C.1.1}Charge pump}{76}%
\contentsline {subsubsection}{Simulatie en opbouw Charge pump}{77}%
\contentsline {subsubsection}{BOM charge pump}{81}%
\contentsline {subsection}{\numberline {C.1.2}Boost Converter}{82}%
\contentsline {subsection}{\numberline {C.1.3}Schakelen LED}{85}%
\contentsline {subsubsection}{Schakelen met transistor}{85}%
\contentsline {subsection}{\numberline {C.1.4}Schakelen met FET}{87}%
\contentsline {section}{\numberline {C.2}Berekeningen gekozen FET}{88}%
\contentsline {section}{\numberline {C.3}Conclusie}{89}%
\contentsline {chapter}{\numberline {D.}Data Dictionary}{90}%
\contentsline {chapter}{\numberline {E.}VNA antenne testen}{91}%
\contentsline {chapter}{\numberline {F.}Plan Van Aanpak}{94}%
\contentsline {section}{\numberline {F.1}Achtergronden}{95}%
\contentsline {section}{\numberline {F.2}Projectresultaat}{95}%
\contentsline {section}{\numberline {F.4}Projectgrenzen en randvoorwaarden}{98}%
\contentsline {section}{\numberline {F.5}Tussenresultaten}{98}%
\contentsline {section}{\numberline {F.6}Kwaliteit}{99}%
\contentsline {section}{\numberline {F.7}Projectorganisatie}{100}%
\contentsline {section}{\numberline {F.8}Planning}{100}%
\contentsline {section}{\numberline {F.9}Kosten en baten}{103}%
\contentsline {section}{\numberline {F.10}Risico's}{103}%
\contentsline {chapter}{\numberline {G.}Tekeningen PCB en solderen}{105}%
\contentsline {section}{\numberline {G.1}Realisatie Hardware}{105}%
\contentsline {section}{\numberline {G.2}Software}{108}%
\contentsline {section}{\numberline {G.3}Solderen}{109}%
\contentsline {section}{\numberline {G.4}Componenten}{109}%
\contentsline {section}{\numberline {G.5}Program connector}{110}%
\contentsline {chapter}{\numberline {H.}Acceptatie test}{111}%
\contentsline {section}{\numberline {H.1}Voorbereidingen acceptatietest}{111}%
\contentsline {subsection}{\numberline {H.1.1}Visuele testomgeving}{111}%
\contentsline {subsubsection}{Hardware}{111}%
\contentsline {subsubsection}{Software}{111}%
\contentsline {subsection}{\numberline {H.1.2}Testomgeving seriële verbinding}{111}%
\contentsline {subsubsection}{Hardware}{111}%
\contentsline {subsubsection}{Software}{112}%
\contentsline {subsubsection}{Overig}{112}%
\contentsline {subsection}{\numberline {H.1.3}Test mesh}{112}%
\contentsline {subsubsection}{Hardware}{112}%
\contentsline {subsubsection}{Software}{112}%
\contentsline {subsection}{\numberline {H.1.4}Test data opslag}{112}%
\contentsline {subsubsection}{Hardware}{112}%
\contentsline {subsubsection}{Software}{112}%
\contentsline {subsection}{\numberline {H.1.5}Test software update}{112}%
\contentsline {subsubsection}{Hardware}{112}%
\contentsline {section}{\numberline {H.2}Acceptatietestbeschrijvingen}{113}%
\contentsline {subsection}{\numberline {H.2.1}Test Bluetooth naar serieel}{113}%
\contentsline {subsubsection}{Testcase 1.1: Commando's }{113}%
\contentsline {subsubsection}{Testcase 1.2: Draaien motor}{114}%
\contentsline {subsection}{\numberline {H.2.2}Test Visuele feedback}{114}%
\contentsline {subsubsection}{Testcase 2.1: Test LED}{114}%
\contentsline {subsection}{\numberline {H.2.3}Testen Draadloze verbinding}{115}%
\contentsline {subsubsection}{Testcase 3.1: Test maximale afstand tussen nodes}{115}%
\contentsline {subsubsection}{Testcase 3.2: Verbinding tussen smartphone en node}{116}%
\contentsline {subsubsection}{Testcase 3.3: Doorsturen proxy}{116}%
\contentsline {subsubsection}{Testcase 3.4: Doorsturen mesh}{117}%
\contentsline {subsubsection}{Testcase 3.5: Sturen diagnostische data}{117}%
\contentsline {subsubsection}{Testcase 3.6: Identificeren systeem}{118}%
\contentsline {subsection}{\numberline {H.2.4}Test opgeslagen data}{119}%
\contentsline {subsubsection}{Testcase 4.1: Test Opslag data}{119}%
\contentsline {subsection}{\numberline {H.2.5}Test RoHS}{119}%
\contentsline {section}{\numberline {H.3}Traceerbaarheid klanteisen}{120}%
\contentsline {chapter}{\numberline {I.}Integratietest}{121}%
\contentsline {section}{\numberline {I.1}Testen}{121}%
\contentsline {subsection}{\numberline {I.1.1}Test IC1: zenden UART}{122}%
\contentsline {subsection}{\numberline {I.1.2}Test IC1: ontvangen UART}{122}%
\contentsline {subsection}{\numberline {I.1.3}Test IC2: Hardware software update}{123}%
\contentsline {subsection}{\numberline {I.1.4}Test IC3: rimpel voeding}{123}%
\contentsline {subsection}{\numberline {I.1.5}Test IC4: Extern Flash}{124}%
\contentsline {subsection}{\numberline {I.1.6}Test IC5: mesh LED}{124}%
\contentsline {subsection}{\numberline {I.1.7}Test IC5: mesh UART}{125}%
\contentsline {subsection}{\numberline {I.1.8}Test IC6: LED}{125}%
\contentsline {chapter}{\numberline {J.}Unit test}{126}%
\contentsline {section}{\numberline {J.1}Test LED}{126}%
\contentsline {section}{\numberline {J.2}Test stroom LED}{127}%
\contentsline {section}{\numberline {J.3}Test UART zenden}{127}%
\contentsline {chapter}{\numberline {K.}Testresultaten}{128}%
\contentsline {section}{\numberline {K.1}Unittest}{128}%
\contentsline {section}{\numberline {K.2}Integratietest}{132}%
\contentsline {subsection}{\numberline {K.2.1}Test rimpel}{132}%
\contentsline {subsection}{\numberline {K.2.2}Test mesh LED}{133}%
\contentsline {subsection}{\numberline {K.2.3}Test mesh UART}{134}%
\contentsline {section}{\numberline {K.3}Acceptatietest}{135}%
\contentsline {subsection}{\numberline {K.3.1}Testcase 1.1 en testcase 1.2}{135}%
\contentsline {subsection}{\numberline {K.3.2}Testcase 2.1}{136}%
\contentsline {section}{\numberline {K.4}Testcase 2.2}{137}%
\contentsline {section}{\numberline {K.5}Testcase 3.1}{138}%
\contentsline {section}{\numberline {K.6}Testcase 5.1}{138}%
\contentsline {chapter}{\numberline {L.}Bill Of Materials}{139}%
\contentsline {chapter}{\numberline {M.}Handleiding BlueFarm}{141}%
\contentsline {section}{\numberline {M.1}Programmeren}{141}%
\contentsline {section}{\numberline {M.2}Provisioneren}{142}%
\contentsline {section}{\numberline {M.3}Probleem oplossen}{144}%
\contentsline {chapter}{\numberline {N.}Competentieverantwoording}{145}%
\contentsline {section}{\numberline {N.1}Analyseren}{145}%
\contentsline {section}{\numberline {N.2}Ontwerpen}{146}%
\contentsline {section}{\numberline {N.3}Realiseren}{146}%
\contentsline {section}{\numberline {N.4}Beheren}{146}%
\contentsline {section}{\numberline {N.5}Managen}{146}%
\contentsline {section}{\numberline {N.6}Adviseren}{146}%
\contentsline {section}{\numberline {N.7}Onderzoeken}{147}%
\contentsline {section}{\numberline {N.8}Professionaliseren}{147}%
\contentsline {chapter}{\numberline {O.}Reflectie}{148}%
\contentsline {section}{\numberline {O.1}Analyseren}{148}%
\contentsline {section}{\numberline {O.2}Ontwerpen}{148}%
\contentsline {section}{\numberline {O.3}Realiseren}{148}%
\contentsline {section}{\numberline {O.4}Beheren}{148}%
\contentsline {section}{\numberline {O.5}Managen}{148}%
\contentsline {section}{\numberline {O.6}Adviseren}{149}%
\contentsline {section}{\numberline {O.7}Onderzoeken}{149}%
\contentsline {section}{\numberline {O.8}Professionaliseren}{149}%
\contentsline {chapter}{\numberline {P.}Beoordeling bedrijfsbegeleider}{150}%
\contentsline {chapter}{\numberline {Q.}Code}{151}%
\contentsline {section}{\numberline {Q.1}Main.c}{151}%
\contentsline {section}{\numberline {Q.2}UART\_debug.c}{157}%
\contentsline {section}{\numberline {Q.3}SPI\_Flash.c}{158}%
