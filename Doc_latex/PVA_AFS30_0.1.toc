\contentsline {section}{\numberline {1}Achtergronden}{3}%
\contentsline {subsection}{\numberline {1.1}De opdrachtgever}{3}%
\contentsline {subsection}{\numberline {1.2}De opdrachtnemers}{3}%
\contentsline {subsection}{\numberline {1.3}Aanleiding}{3}%
\contentsline {section}{\numberline {2}Projectresultaat}{3}%
\contentsline {subsection}{\numberline {2.1}Doelstelling}{3}%
\contentsline {subsection}{\numberline {2.2}Resultaat}{3}%
\contentsline {section}{\numberline {3}Projectactiviteiten}{4}%
\contentsline {subsection}{\numberline {3.1}Projectmanagement}{4}%
\contentsline {subsection}{\numberline {3.2}Projectmanagement}{4}%
\contentsline {section}{\numberline {4}Projectgrenzen en randvoorwaarden}{5}%
\contentsline {subsection}{\numberline {4.1}Projectomvang}{5}%
\contentsline {subsection}{\numberline {4.2}Randvoorwaarden}{5}%
\contentsline {section}{\numberline {5}Tussenresultaten}{5}%
\contentsline {subsection}{\numberline {5.1}Tussenresultaten in documentvorm: }{5}%
\contentsline {subsection}{\numberline {5.2}Tussenresultaten in de vorm van een deelsysteem }{5}%
\contentsline {section}{\numberline {6}Kwaliteit}{6}%
\contentsline {subsection}{\numberline {6.1}Kwaliteitsbewaking eindproduct}{6}%
\contentsline {subsection}{\numberline {6.2}Tussenresultaten}{6}%
\contentsline {subsection}{\numberline {6.3}Documenten}{6}%
\contentsline {subsection}{\numberline {6.4}Afwijkingen}{6}%
\contentsline {section}{\numberline {7}Projectorganisatie}{7}%
\contentsline {subsection}{\numberline {7.1}Rolverdeling met persoonlijke gegevens}{7}%
\contentsline {subsection}{\numberline {7.2}Functieomschrijving}{7}%
\contentsline {subsection}{\numberline {7.3}Informatie}{7}%
\contentsline {subsection}{\numberline {7.4}Coördinatie}{7}%
\contentsline {section}{\numberline {8}Planning}{8}%
\contentsline {section}{\numberline {9}Kosten en baten}{10}%
\contentsline {subsection}{\numberline {9.1}Kosten voor hulpmiddelen}{10}%
\contentsline {subsection}{\numberline {9.2}Arbeidskosten uitgedrukt in werktijd}{10}%
\contentsline {subsection}{\numberline {9.3}Baten}{10}%
\contentsline {section}{\numberline {10}Risico's}{10}%
\contentsline {subsection}{\numberline {10.1}Interne risico’s}{10}%
\contentsline {subsection}{\numberline {10.2}Externe risico’s}{10}%
\contentsline {subsection}{\numberline {10.3}Externe risico’s}{11}%
