\begin{thebibliography}{}

\bibitem [\protect \citeauthoryear {%
Ali-rantala%
, Sydanheimo%
, Keskilammi%
\BCBL {}\ \BBA {} Kivikoski%
}{%
Ali-rantala%
\ \protect \BOthers {.}}{%
{\protect \APACyear {{\protect \bibnodate {}}}}%
}]{%
Ali-rantala}
\APACinsertmetastar {%
Ali-rantala}%
\begin{APACrefauthors}%
Ali-rantala, P.%
, Sydanheimo, L.%
, Keskilammi, M.%
\BCBL {}\ \BBA {} Kivikoski, M.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{{\protect \bibnodate {}}}{}{}.
\newblock
{\BBOQ}\APACrefatitle {{2 . 45GHz AND 433MHz TRANSMISSIONS}} {{2 . 45GHz AND
  433MHz TRANSMISSIONS}}.{\BBCQ}
\newblock
\APACjournalVolNumPages{}{}{}{240--243}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Alliance%
}{%
Alliance%
}{%
{\protect \APACyear {2020}}%
}]{%
zigbee}
\APACinsertmetastar {%
zigbee}%
\begin{APACrefauthors}%
Alliance, Z.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2020}{}{}.
\newblock
\APACrefbtitle {What is Zigbee?} {What is zigbee?}
\newblock
\begin{APACrefURL} \url{{https://zigbeealliance.org/solution/zigbee/}}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Bluetooth%
}{%
Bluetooth%
}{%
{\protect \APACyear {2020}}%
{\protect \APACexlab {{\protect \BCnt {1}}}}}]{%
meshfaq}
\APACinsertmetastar {%
meshfaq}%
\begin{APACrefauthors}%
Bluetooth.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2020{\protect \BCnt {1}}}{}{}.
\newblock
\APACrefbtitle {Bluetooth mesh networking FAQs.} {Bluetooth mesh networking
  faqs.}
\newblock
\begin{APACrefURL}
  \url{{https://www.bluetooth.com/learn-about-bluetooth/bluetooth-technology/topology-options/le-mesh/mesh-faq/}}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Bluetooth%
}{%
Bluetooth%
}{%
{\protect \APACyear {2020}}%
{\protect \APACexlab {{\protect \BCnt {2}}}}}]{%
blfees}
\APACinsertmetastar {%
blfees}%
\begin{APACrefauthors}%
Bluetooth.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2020{\protect \BCnt {2}}}{}{}.
\newblock
\APACrefbtitle {Qualification fees fund the future of Bluetooth.}
  {Qualification fees fund the future of bluetooth.}
\newblock
\begin{APACrefURL}
  \url{{https://www.bluetooth.com/develop-with-bluetooth/qualification-listing/qualification-listing-fees/}}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Campbell%
\ \BBA {} Gubisch%
}{%
Campbell%
\ \BBA {} Gubisch%
}{%
{\protect \APACyear {1966}}%
}]{%
Campbell1966}
\APACinsertmetastar {%
Campbell1966}%
\begin{APACrefauthors}%
Campbell, B\BPBI Y\BPBI F\BPBI W.%
\BCBT {}\ \BBA {} Gubisch, R\BPBI W.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{1966}{}{}.
\newblock
{\BBOQ}\APACrefatitle {{OPTICAL QUALITY OF THE HUMAN EYE}} {{OPTICAL QUALITY OF
  THE HUMAN EYE}}.{\BBCQ}
\newblock
\APACjournalVolNumPages{J. Physiol.}{186}{}{558--578}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
CBS%
}{%
CBS%
}{%
{\protect \APACyear {2019}}%
}]{%
cbs}
\APACinsertmetastar {%
cbs}%
\begin{APACrefauthors}%
CBS.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2019}{}{}.
\newblock
\APACrefbtitle {Meeste Nederlanders beschermen gegevens op smartphone.} {Meeste
  nederlanders beschermen gegevens op smartphone.}
\newblock
\begin{APACrefURL}
  \url{{https://www.cbs.nl/nl-nl/nieuws/2019/06/meeste-nederlanders-beschermen-gegevens-op-smartphone}}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Chruszczyk%
}{%
Chruszczyk%
}{%
{\protect \APACyear {2017}}%
}]{%
Chruszczyk2017}
\APACinsertmetastar {%
Chruszczyk2017}%
\begin{APACrefauthors}%
Chruszczyk, L.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2017}{}{}.
\newblock
{\BBOQ}\APACrefatitle {{Statistical Analysis of Indoor RSSI Read-outs for 433
  MHz, 868 MHz, 2.4 GHz and 5 GHz ISM Bands}} {{Statistical Analysis of Indoor
  RSSI Read-outs for 433 MHz, 868 MHz, 2.4 GHz and 5 GHz ISM Bands}}.{\BBCQ}
\newblock
\APACjournalVolNumPages{International Journal of Electronics and
  Telecommunications}{63}{1}{33--38}.
\newblock
\begin{APACrefDOI} \doi{10.1515/eletel-2017-0005} \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
{Dela Cruz}%
, Parabuac%
\BCBL {}\ \BBA {} Tiglao%
}{%
{Dela Cruz}%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2016}}%
}]{%
DelaCruz2016}
\APACinsertmetastar {%
DelaCruz2016}%
\begin{APACrefauthors}%
{Dela Cruz}, A\BPBI A.%
, Parabuac, M\BPBI L\BPBI A.%
\BCBL {}\ \BBA {} Tiglao, N\BPBI M\BPBI C.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2016}{}{}.
\newblock
{\BBOQ}\APACrefatitle {{Design and implementation of a low-cost and reliable
  wireless mesh network for first-response communications}} {{Design and
  implementation of a low-cost and reliable wireless mesh network for
  first-response communications}}.{\BBCQ}
\newblock
\APACjournalVolNumPages{GHTC 2016 - IEEE Global Humanitarian Technology
  Conference: Technology for the Benefit of Humanity, Conference
  Proceedings}{}{}{40--46}.
\newblock
\begin{APACrefDOI} \doi{10.1109/GHTC.2016.7857258} \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Hu%
, Fu%
, She%
\BCBL {}\ \BBA {} Yao%
}{%
Hu%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2013}}%
}]{%
hu2013}
\APACinsertmetastar {%
hu2013}%
\begin{APACrefauthors}%
Hu, S\BPBI Q.%
, Fu, Y.%
, She, C\BPBI D.%
\BCBL {}\ \BBA {} Yao, H.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{}{}.
\newblock
{\BBOQ}\APACrefatitle {{Enabling zigbee communications in android devices}}
  {{Enabling zigbee communications in android devices}}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Advanced Materials
  Research}{756-759}{Iccia}{2125--2130}.
\newblock
\begin{APACrefDOI} \doi{10.4028/www.scientific.net/AMR.756-759.2125}
  \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
KPN%
}{%
KPN%
}{%
{\protect \APACyear {2020}}%
}]{%
lora}
\APACinsertmetastar {%
lora}%
\begin{APACrefauthors}%
KPN.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2020}{}{}.
\newblock
\APACrefbtitle {Connectiviteit via het LoRa-netwerk.} {Connectiviteit via het
  lora-netwerk.}
\newblock
\begin{APACrefURL}
  \url{{https://www.kpn.com/zakelijk/internet-of-things/en/lora-connectivity.htm}}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Nordic%
}{%
Nordic%
}{%
{\protect \APACyear {2018}}%
}]{%
Specification2018}
\APACinsertmetastar {%
Specification2018}%
\begin{APACrefauthors}%
Nordic.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2018}{}{}.
\newblock
{\BBOQ}\APACrefatitle {{Product specification}} {{Product
  specification}}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Building Research {\&} Information}{21}{1}{21--22}.
\newblock
\begin{APACrefDOI} \doi{10.1080/09613219308727250} \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Tablo%
}{%
Tablo%
}{%
{\protect \APACyear {2018}}%
}]{%
los}
\APACinsertmetastar {%
los}%
\begin{APACrefauthors}%
Tablo.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2018}{}{}.
\newblock
\APACrefbtitle {How Your Location Impacts Your Over-the-Air TV Reception.} {How
  your location impacts your over-the-air tv reception.}
\newblock
\begin{APACrefURL}
  \url{{https://www.tablotv.com/blog/how-location-impacts-your-over-air-tv-reception/}}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Techdifferences%
}{%
Techdifferences%
}{%
{\protect \APACyear {2018}}%
}]{%
starmesh}
\APACinsertmetastar {%
starmesh}%
\begin{APACrefauthors}%
Techdifferences.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2018}{}{}.
\newblock
\APACrefbtitle {Difference Between Star and Mesh Topology.} {Difference between
  star and mesh topology.}
\newblock
\begin{APACrefURL}
  \url{{https://techdifferences.com/difference-between-star-and-mesh-topology.html}}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Verma%
, Fakharzadeh%
\BCBL {}\ \BBA {} Technologies%
}{%
Verma%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2013}}%
}]{%
Verma2013}
\APACinsertmetastar {%
Verma2013}%
\begin{APACrefauthors}%
Verma, L.%
, Fakharzadeh, M.%
\BCBL {}\ \BBA {} Technologies, P.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{}{}.
\newblock
{\BBOQ}\APACrefatitle {{WIFIONSTEROIDS: 802.11AC AND802.11AD}}
  {{WIFIONSTEROIDS: 802.11AC AND802.11AD}}.{\BBCQ}
\newblock
\APACjournalVolNumPages{IEEE Wireless Communications}{}{December}{30--35}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Wikipedia%
}{%
Wikipedia%
}{%
{\protect \APACyear {2018}}%
}]{%
topo}
\APACinsertmetastar {%
topo}%
\begin{APACrefauthors}%
Wikipedia.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2018}{}{}.
\newblock
\APACrefbtitle {Netwerktopologie.} {Netwerktopologie.}
\newblock
\begin{APACrefURL} \url{{https://nl.wikipedia.org/wiki/Netwerktopologie}}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Yaakop%
, Malik%
, {Bin Suboh}%
, Ramli%
\BCBL {}\ \BBA {} Abu%
}{%
Yaakop%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2017}}%
}]{%
Yaakop2017}
\APACinsertmetastar {%
Yaakop2017}%
\begin{APACrefauthors}%
Yaakop, M\BPBI B.%
, Malik, I\BPBI A\BPBI A.%
, {Bin Suboh}, Z.%
, Ramli, A\BPBI F.%
\BCBL {}\ \BBA {} Abu, M\BPBI A.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2017}{}{}.
\newblock
{\BBOQ}\APACrefatitle {{Bluetooth 5.0 throughput comparison for internet of
  thing usability a survey}} {{Bluetooth 5.0 throughput comparison for internet
  of thing usability a survey}}.{\BBCQ}
\newblock
\APACjournalVolNumPages{2017 International Conference on Engineering Technology
  and Technopreneurship, ICE2T 2017}{2017-Janua}{}{1--6}.
\newblock
\begin{APACrefDOI} \doi{10.1109/ICE2T.2017.8215995} \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Yourdon%
}{%
Yourdon%
}{%
{\protect \APACyear {1979}}%
}]{%
Yourdon1979}
\APACinsertmetastar {%
Yourdon1979}%
\begin{APACrefauthors}%
Yourdon, E\BPBI N.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYear{1979}.
\newblock
\APACrefbtitle {{Classics in software engineering}} {{Classics in software
  engineering}}.
\newblock
\APACaddressPublisher{}{Yourdon Press}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Zhao%
, Xiao%
, Markham%
, Trigoni%
\BCBL {}\ \BBA {} Ren%
}{%
Zhao%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2014}}%
}]{%
Zhao2014}
\APACinsertmetastar {%
Zhao2014}%
\begin{APACrefauthors}%
Zhao, X.%
, Xiao, Z.%
, Markham, A.%
, Trigoni, N.%
\BCBL {}\ \BBA {} Ren, Y.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2014}{}{}.
\newblock
{\BBOQ}\APACrefatitle {{Does BTLE measure up against wifi? A comparison of
  indoor location performance}} {{Does BTLE measure up against wifi? A
  comparison of indoor location performance}}.{\BBCQ}
\newblock
\APACjournalVolNumPages{20th European Wireless Conference, EW
  2014}{}{}{263--268}.
\PrintBackRefs{\CurrentBib}

\end{thebibliography}
