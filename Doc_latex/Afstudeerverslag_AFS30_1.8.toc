\babel@toc {dutch}{}
\contentsline {chapter}{\numberline {1.}Inleiding}{12}%
\contentsline {section}{\numberline {1.1}Bedrijfsachtergrond}{12}%
\contentsline {section}{\numberline {1.2}Probleemstelling}{14}%
\contentsline {section}{\numberline {1.3}Doelstelling}{16}%
\contentsline {chapter}{\numberline {2.}Onderzoek}{17}%
\contentsline {chapter}{\numberline {3.}Requirements}{18}%
\contentsline {section}{\numberline {3.1}Systeemcontext}{18}%
\contentsline {subsection}{\numberline {3.1.1}Systeemfuncties}{18}%
\contentsline {section}{\numberline {3.2}Systeemfuncties}{20}%
\contentsline {subsection}{\numberline {3.2.1}Communicatie Bluetooth }{20}%
\contentsline {subsubsection}{Stimulus en response}{20}%
\contentsline {subsubsection}{Functionele klanteisen}{20}%
\contentsline {subsection}{\numberline {3.2.2}Communicatie met IM60/IM125}{21}%
\contentsline {subsubsection}{Stimulus en response}{21}%
\contentsline {subsubsection}{Functionele klanteisen}{21}%
\contentsline {subsection}{\numberline {3.2.3}Opslaan data}{22}%
\contentsline {subsubsection}{Stimulus en response}{22}%
\contentsline {subsubsection}{Functionele klanteisen}{22}%
\contentsline {subsection}{\numberline {3.2.4}Identificeren systeem}{22}%
\contentsline {subsubsection}{Stimulus en response}{22}%
\contentsline {subsubsection}{Functionele klanteisen}{22}%
\contentsline {section}{\numberline {3.3}Niet-functionele klanteisen}{23}%
\contentsline {chapter}{\numberline {4.}Architectuur}{24}%
\contentsline {section}{\numberline {4.1}Ontwerpbesluiten}{25}%
\contentsline {section}{\numberline {4.2}Deelsysteem 1: Control mesh}{25}%
\contentsline {section}{\numberline {4.3}Identificeren Systeem}{29}%
\contentsline {section}{\numberline {4.4}Opslaan gegevens}{30}%
\contentsline {chapter}{\numberline {5.}Detailontwerp}{31}%
\contentsline {section}{\numberline {5.1}Unit 1: Microcontroller}{34}%
\contentsline {subsection}{\numberline {5.1.1}Unit 1.2: UART IM60/IM125}{34}%
\contentsline {subsection}{\numberline {5.1.2}TX}{35}%
\contentsline {subsection}{\numberline {5.1.3}RX}{37}%
\contentsline {subsection}{\numberline {5.1.4}Unit 1.2: Bluetooth 5.0 mesh}{39}%
\contentsline {subsubsection}{Node}{39}%
\contentsline {subsubsection}{Model}{40}%
\contentsline {subsubsection}{Proxy}{41}%
\contentsline {subsubsection}{Bereik}{42}%
\contentsline {section}{\numberline {5.2}Unit 1.3: Opslaam data}{43}%
\contentsline {subsection}{\numberline {5.2.1}Software}{44}%
\contentsline {section}{\numberline {5.3}Unit 2: LED schakeling}{47}%
\contentsline {chapter}{\numberline {6.}Testen}{53}%
\contentsline {chapter}{\numberline {7.}Conclusie}{54}%
\contentsline {chapter}{\numberline {8.}Aanbevelingen}{55}%
\contentsline {section}{\numberline {8.1}Plaatsing op IM60/IM125}{55}%
\contentsline {chapter}{Appendices}{56}%
\contentsline {chapter}{\numberline {A.}Onderzoek draadloze technologieën}{57}%
\contentsline {section}{\numberline {A.1}Inleiding}{57}%
\contentsline {section}{\numberline {A.2}Draadloze technologieën}{59}%
\contentsline {subsection}{\numberline {A.2.1}Frequentie, bereik en maximale snelheid}{60}%
\contentsline {subsection}{\numberline {A.2.2}Topologie en gebruik smartphone}{61}%
\contentsline {subsection}{\numberline {A.2.3}Kosten}{61}%
\contentsline {section}{\numberline {A.3}Conclusie}{63}%
\contentsline {chapter}{\numberline {B.}Onderzoek Bluetooth mesh}{64}%
\contentsline {section}{\numberline {B.1}Inleiding}{65}%
\contentsline {section}{\numberline {B.2}Bluetooth Low Energie}{65}%
\contentsline {paragraph}{Physical layer}{66}%
\contentsline {paragraph}{Link layer}{66}%
\contentsline {paragraph}{L2CAP}{66}%
\contentsline {paragraph}{GAP}{66}%
\contentsline {paragraph}{Attribute Protocol}{66}%
\contentsline {paragraph}{GATT}{68}%
\contentsline {section}{\numberline {B.3}Bluetooth mesh}{68}%
\contentsline {paragraph}{Bearer Layer}{69}%
\contentsline {paragraph}{Network Layer}{69}%
\contentsline {paragraph}{Lower Transport Layer}{69}%
\contentsline {paragraph}{Upper Transport Layer}{69}%
\contentsline {paragraph}{Acces Layer}{69}%
\contentsline {paragraph}{Foundation Model Layer}{69}%
\contentsline {paragraph}{Model layer}{69}%
\contentsline {section}{\numberline {B.4}Test mesh}{70}%
\contentsline {section}{\numberline {B.5}Conclusie}{71}%
\contentsline {chapter}{\numberline {C.}Onderzoek LED}{72}%
\contentsline {section}{\numberline {C.1}DC-DC conversie}{73}%
\contentsline {subsection}{\numberline {C.1.1}Charge pump}{73}%
\contentsline {subsubsection}{Simulatie en opbouw Charge pump}{74}%
\contentsline {subsubsection}{BOM charge pump}{78}%
\contentsline {subsection}{\numberline {C.1.2}Boost Converter}{79}%
\contentsline {subsection}{\numberline {C.1.3}Schakelen LED}{82}%
\contentsline {subsubsection}{Schakelen met transistor}{82}%
\contentsline {subsection}{\numberline {C.1.4}Schakelen met FET}{84}%
\contentsline {section}{\numberline {C.2}Berekeningen gekozen FET}{85}%
\contentsline {section}{\numberline {C.3}Conclusie}{86}%
\contentsline {chapter}{\numberline {D.}Plan Van Aanpak}{87}%
\contentsline {section}{\numberline {D.1}Achtergronden}{88}%
\contentsline {section}{\numberline {D.2}Projectresultaat}{88}%
\contentsline {section}{\numberline {D.4}Projectgrenzen en randvoorwaarden}{91}%
\contentsline {section}{\numberline {D.5}Tussenresultaten}{91}%
\contentsline {section}{\numberline {D.6}Kwaliteit}{92}%
\contentsline {section}{\numberline {D.7}Projectorganisatie}{93}%
\contentsline {section}{\numberline {D.8}Planning}{93}%
\contentsline {section}{\numberline {D.9}Kosten en baten}{96}%
\contentsline {section}{\numberline {D.10}Risico's}{96}%
\contentsline {chapter}{\numberline {E.}Tekeningen PCB en solderen}{98}%
\contentsline {section}{\numberline {E.1}Realisatie Hardware}{98}%
\contentsline {section}{\numberline {E.2}Software}{101}%
\contentsline {section}{\numberline {E.3}Solderen}{102}%
\contentsline {section}{\numberline {E.4}Componenten}{102}%
\contentsline {section}{\numberline {E.5}Program connector}{103}%
\contentsline {chapter}{\numberline {F.}Volledige keuze verantwoording draadloze microcontroller}{104}%
\contentsline {paragraph}{Nordic}{104}%
\contentsline {paragraph}{ST}{104}%
\contentsline {paragraph}{NXP}{104}%
\contentsline {paragraph}{TI}{105}%
\contentsline {paragraph}{Silicon Labs}{105}%
\contentsline {paragraph}{Conclusie}{105}%
\contentsline {chapter}{\numberline {G.}Acceptatie test}{106}%
\contentsline {section}{\numberline {G.1}Voorbereidingen acceptatietest}{106}%
\contentsline {subsection}{\numberline {G.1.1}Visuele testomgeving}{106}%
\contentsline {subsubsection}{Hardware}{106}%
\contentsline {subsubsection}{Software}{106}%
\contentsline {subsection}{\numberline {G.1.2}Testomgeving seriële verbinding}{106}%
\contentsline {subsubsection}{Hardware}{106}%
\contentsline {subsubsection}{Software}{107}%
\contentsline {subsubsection}{Overig}{107}%
\contentsline {subsection}{\numberline {G.1.3}Test mesh}{107}%
\contentsline {subsubsection}{Hardware}{107}%
\contentsline {subsubsection}{Software}{107}%
\contentsline {subsection}{\numberline {G.1.4}Test data opslag}{108}%
\contentsline {subsubsection}{Hardware}{108}%
\contentsline {subsubsection}{Software}{108}%
\contentsline {subsection}{\numberline {G.1.5}Test software update}{108}%
\contentsline {subsubsection}{Hardware}{108}%
\contentsline {section}{\numberline {G.2}Acceptatietestbeschrijvingen}{109}%
\contentsline {subsection}{\numberline {G.2.1}Test Bluetooth naar serieel}{109}%
\contentsline {subsubsection}{Testcase 1.1: Commando's io-net}{109}%
\contentsline {subsubsection}{Testcase 1.2: Draaien motor}{110}%
\contentsline {subsection}{\numberline {G.2.2}Test Visuele feedback}{111}%
\contentsline {subsubsection}{Testcase 2.1: Test LED}{111}%
\contentsline {subsubsection}{Testcase 2.2: Test maximale afstand}{112}%
\contentsline {subsection}{\numberline {G.2.3}Test opgeslagen data}{113}%
\contentsline {subsubsection}{Testcase 3.1: Test Opslag data}{113}%
\contentsline {subsection}{\numberline {G.2.4}Test RoHS}{113}%
\contentsline {section}{\numberline {G.3}Traceerbaarheid klanteisen}{114}%
\contentsline {chapter}{\numberline {H.}Integratietest}{115}%
\contentsline {section}{\numberline {H.1}Testen}{115}%
\contentsline {subsection}{\numberline {H.1.1}Test IC1: zenden UART}{116}%
\contentsline {subsection}{\numberline {H.1.2}Test IC1: ontvangen UART}{117}%
\contentsline {subsection}{\numberline {H.1.3}Test IC2: Hardware software update}{118}%
\contentsline {subsection}{\numberline {H.1.4}Test IC3: rimpel voeding}{119}%
\contentsline {subsection}{\numberline {H.1.5}Test IC4: Extern Flash}{120}%
\contentsline {subsection}{\numberline {H.1.6}Test IC5: mesh LED}{121}%
\contentsline {subsection}{\numberline {H.1.7}Test IC5: mesh UART}{122}%
\contentsline {subsection}{\numberline {H.1.8}Test IC6: LED}{123}%
\contentsline {chapter}{\numberline {I.}Unit test}{124}%
\contentsline {section}{\numberline {I.1}Test LED}{124}%
\contentsline {section}{\numberline {I.2}Test stroom LED}{125}%
\contentsline {section}{\numberline {I.3}Test UART zenden}{125}%
\contentsline {chapter}{\numberline {J.}Testresultaten}{126}%
\contentsline {section}{\numberline {J.1}Unittest}{126}%
\contentsline {section}{\numberline {J.2}Integratietest}{130}%
\contentsline {subsection}{\numberline {J.2.1}Test rimpel}{130}%
\contentsline {subsection}{\numberline {J.2.2}Test mesh LED}{131}%
\contentsline {subsection}{\numberline {J.2.3}Test mesh UART}{132}%
\contentsline {section}{\numberline {J.3}Acceptatietest}{133}%
\contentsline {subsection}{\numberline {J.3.1}Testcase 1.1 en testcase 1.2}{133}%
\contentsline {subsection}{\numberline {J.3.2}Testcase 2.1}{134}%
\contentsline {section}{\numberline {J.4}Testcase 2.2}{135}%
\contentsline {section}{\numberline {J.5}Testcase 3.1}{136}%
\contentsline {section}{\numberline {J.6}Testcase 5.1}{136}%
\contentsline {chapter}{\numberline {K.}Bill Of Materials}{137}%
\contentsline {chapter}{\numberline {L.}Handleiding BlueFarm}{139}%
\contentsline {section}{\numberline {L.1}Programmeren}{139}%
\contentsline {section}{\numberline {L.2}Provisioneren}{140}%
\contentsline {section}{\numberline {L.3}Probleem oplossen}{142}%
\contentsline {chapter}{\numberline {M.}Competentieverantwoording}{143}%
\contentsline {section}{\numberline {M.1}Analyseren}{143}%
\contentsline {section}{\numberline {M.2}Ontwerpen}{144}%
\contentsline {section}{\numberline {M.3}Realiseren}{144}%
\contentsline {section}{\numberline {M.4}Beheren}{144}%
\contentsline {section}{\numberline {M.5}Managen}{144}%
\contentsline {section}{\numberline {M.6}Adviseren}{144}%
\contentsline {section}{\numberline {M.7}Onderzoeken}{145}%
\contentsline {section}{\numberline {M.8}Professionaliseren}{145}%
\contentsline {chapter}{\numberline {N.}Reflectie}{146}%
\contentsline {section}{\numberline {N.1}Analyseren}{146}%
\contentsline {section}{\numberline {N.2}Ontwerpen}{146}%
\contentsline {section}{\numberline {N.3}Realiseren}{146}%
\contentsline {section}{\numberline {N.4}Beheren}{146}%
\contentsline {section}{\numberline {N.5}Managen}{146}%
\contentsline {section}{\numberline {N.6}Adviseren}{147}%
\contentsline {section}{\numberline {N.7}Onderzoeken}{147}%
\contentsline {section}{\numberline {N.8}Professionaliseren}{147}%
\contentsline {chapter}{\numberline {O.}Beoordeling bedrijfsbegeleider}{148}%
\contentsline {chapter}{\numberline {P.}Code}{149}%
\contentsline {section}{\numberline {P.1}Main.c}{149}%
\contentsline {section}{\numberline {P.2}UART\_debug.c}{155}%
\contentsline {section}{Literatuur}{156}%
