\babel@toc {dutch}{}
\contentsline {chapter}{\numberline {1.}Inleiding}{9}%
\contentsline {section}{\numberline {1.1}Bedrijfsachtergrond}{9}%
\contentsline {section}{\numberline {1.2}Probleemstelling}{12}%
\contentsline {section}{\numberline {1.3}Doelstelling}{13}%
\contentsline {chapter}{\numberline {2.}Requirements}{14}%
\contentsline {section}{\numberline {2.1}Inleiding}{14}%
\contentsline {section}{\numberline {2.2}Algemene beschrijving}{14}%
\contentsline {section}{\numberline {2.3}Systeemcontext}{14}%
\contentsline {subsection}{\numberline {2.3.1}Systeemfuncties}{14}%
\contentsline {section}{\numberline {2.4}Systeemfuncties}{16}%
\contentsline {subsection}{\numberline {2.4.1}Draadloze data overdracht}{16}%
\contentsline {subsubsection}{Stimulus en response}{16}%
\contentsline {subsubsection}{Functionele klanteisen}{16}%
\contentsline {subsection}{\numberline {2.4.2}Uitlezen en opslag data}{17}%
\contentsline {subsubsection}{Stimulus en response}{17}%
\contentsline {subsubsection}{Functionele klanteisen}{17}%
\contentsline {subsection}{\numberline {2.4.3}Updaten systeem en bestaande module (Optioneel)}{18}%
\contentsline {subsubsection}{Stimulus en response}{18}%
\contentsline {subsubsection}{Functionele klanteisen}{18}%
\contentsline {section}{\numberline {2.5}Niet-functionele klanteisen}{18}%
\contentsline {chapter}{\numberline {3.}Architectuur}{19}%
\contentsline {section}{\numberline {3.1}Ontwerpbesluiten}{19}%
\contentsline {section}{\numberline {3.2}Deelsystemen en koppelingen}{20}%
\contentsline {section}{\numberline {3.3}Beschrijving deelsystemen}{21}%
\contentsline {subsection}{\numberline {3.3.1}Deelsysteem 1: Control Data}{22}%
\contentsline {subsubsection}{Deelsysteem 1.1: Execute functions}{23}%
\contentsline {subsubsection}{Deelsysteem 1.2. Control Saved Data}{23}%
\contentsline {subsubsection}{Deelsysteem 1.3. Communicate Motor Control Unit}{23}%
\contentsline {subsection}{\numberline {3.3.2}Deelsysteem 2: Transeive Data}{24}%
\contentsline {subsection}{\numberline {3.3.3}Deelsysteem 3: Flash Light}{24}%
\contentsline {section}{\numberline {3.4}Beschrijving interconnects}{24}%
\contentsline {section}{\numberline {3.5}Traceerbaarheid klanteisen}{25}%
\contentsline {chapter}{\numberline {4.}Detailontwerp}{26}%
\contentsline {subsection}{\numberline {4.0.1}Deelsystemen 1 \& 2}{26}%
\contentsline {subsubsection}{Hardware deelsystemen 1 \& 2}{26}%
\contentsline {paragraph}{Nordic}{26}%
\contentsline {paragraph}{ST}{26}%
\contentsline {paragraph}{NXP}{27}%
\contentsline {paragraph}{TI}{27}%
\contentsline {paragraph}{Silicon Labs}{27}%
\contentsline {paragraph}{Conclusie}{27}%
\contentsline {subsubsection}{Software deelsysteem 1}{29}%
\contentsline {subsubsection}{Software deelsysteem 2}{29}%
\contentsline {chapter}{Appendices}{30}%
\contentsline {chapter}{\numberline {A.}Onderzoek LED}{31}%
\contentsline {chapter}{\numberline {B.}Onderzoek draadloze technologieën}{32}%
\contentsline {section}{\numberline {B.1}Inleiding}{32}%
\contentsline {section}{\numberline {B.2}Draadloze technologieën}{34}%
\contentsline {subsection}{\numberline {B.2.1}Frequentie, bereik en maximale snelheid}{35}%
\contentsline {subsection}{\numberline {B.2.2}Topologie en gebruik smartphone}{36}%
\contentsline {subsection}{\numberline {B.2.3}Kosten}{37}%
\contentsline {section}{\numberline {B.3}Conclusie}{38}%
\contentsline {chapter}{\numberline {C.}Plan Van Aanpak}{39}%
\contentsline {section}{\numberline {C.1}Achtergronden}{40}%
\contentsline {section}{\numberline {C.2}Projectresultaat}{40}%
\contentsline {section}{\numberline {C.4}Projectgrenzen en randvoorwaarden}{44}%
\contentsline {section}{\numberline {C.5}Tussenresultaten}{44}%
\contentsline {section}{\numberline {C.6}Kwaliteit}{45}%
\contentsline {section}{\numberline {C.7}Projectorganisatie}{46}%
\contentsline {section}{\numberline {C.8}Planning}{48}%
\contentsline {section}{\numberline {C.9}Kosten en baten}{49}%
\contentsline {section}{\numberline {C.10}Risico's}{49}%
\contentsline {chapter}{\numberline {D.}Acceptatie test}{55}%
\contentsline {section}{\numberline {D.1}Voorbereidingen acceptatietest}{55}%
\contentsline {subsection}{\numberline {D.1.1}Visuele testomgeving}{55}%
\contentsline {subsubsection}{Hardware}{55}%
\contentsline {subsubsection}{Software}{56}%
\contentsline {subsection}{\numberline {D.1.2}Testomgeving seriële verbinding}{56}%
\contentsline {subsubsection}{Hardware}{56}%
\contentsline {subsubsection}{Software}{56}%
\contentsline {subsubsection}{Overig}{56}%
\contentsline {subsection}{\numberline {D.1.3}Test mesh}{56}%
\contentsline {subsubsection}{Hardware}{56}%
\contentsline {subsubsection}{Software}{56}%
\contentsline {subsection}{\numberline {D.1.4}Test data opslag}{57}%
\contentsline {subsubsection}{Hardware}{57}%
\contentsline {subsubsection}{Software}{57}%
\contentsline {subsection}{\numberline {D.1.5}Test software update}{57}%
\contentsline {subsubsection}{Hardware}{57}%
\contentsline {section}{\numberline {D.2}Acceptatietestbeschrijvingen}{58}%
\contentsline {subsection}{\numberline {D.2.1}Test Bluetooth naar serieel}{58}%
\contentsline {subsubsection}{Testcase 1.1: Commando's io-net}{58}%
\contentsline {subsubsection}{Testcase 1.2: Draaien motor}{59}%
\contentsline {subsection}{\numberline {D.2.2}Test Visuele feedback}{60}%
\contentsline {subsubsection}{Testcase 2.1: Test LED}{60}%
\contentsline {subsubsection}{Testcase 2.2: Test maximale afstand}{60}%
\contentsline {subsection}{\numberline {D.2.3}Test opgeslagen data}{61}%
\contentsline {subsubsection}{Testcase 3.1: Test Opslag data}{61}%
\contentsline {subsection}{\numberline {D.2.4}Test software update}{62}%
\contentsline {section}{\numberline {D.3}Traceerbaarheid klanteisen}{62}%
\contentsline {section}{Literatuur}{63}%
