\babel@toc {dutch}{}
\contentsline {chapter}{\numberline {1.}Inleiding}{8}%
\contentsline {section}{\numberline {1.1}Bedrijfsachtergrond}{8}%
\contentsline {section}{\numberline {1.2}Probleemstelling}{10}%
\contentsline {section}{\numberline {1.3}Doelstelling}{11}%
\contentsline {chapter}{\numberline {2.}Requirements}{12}%
\contentsline {section}{\numberline {2.1}Inleiding}{12}%
\contentsline {section}{\numberline {2.2}Algemene beschrijving}{12}%
\contentsline {section}{\numberline {2.3}Systeemcontext}{12}%
\contentsline {subsection}{\numberline {2.3.1}Systeemfuncties}{12}%
\contentsline {section}{\numberline {2.4}Systeemfuncties}{14}%
\contentsline {subsection}{\numberline {2.4.1}Draadloze data overdracht}{14}%
\contentsline {subsubsection}{Stimulus en response}{14}%
\contentsline {subsubsection}{Functionele klanteisen}{14}%
\contentsline {subsection}{\numberline {2.4.2}Uitlezen en opslag data}{15}%
\contentsline {subsubsection}{Stimulus en response}{15}%
\contentsline {subsubsection}{Functionele klanteisen}{15}%
\contentsline {subsection}{\numberline {2.4.3}Updaten systeem en bestaande module (Optioneel)}{16}%
\contentsline {subsubsection}{Stimulus en response}{16}%
\contentsline {subsubsection}{Functionele klanteisen}{16}%
\contentsline {section}{\numberline {2.5}Niet-functionele klanteisen}{16}%
\contentsline {chapter}{\numberline {3.}Architectuur}{17}%
\contentsline {chapter}{Appendices}{18}%
\contentsline {chapter}{\numberline {A.}Onderzoek draadloze technologieën}{19}%
\contentsline {section}{\numberline {A.1}Inleiding}{19}%
\contentsline {section}{\numberline {A.2}Draadloze technologieën}{21}%
\contentsline {subsection}{\numberline {A.2.1}Frequentie, bereik en maximale snelheid}{22}%
\contentsline {subsection}{\numberline {A.2.2}Topologie en gebruik smartphone}{23}%
\contentsline {subsection}{\numberline {A.2.3}Kosten}{24}%
\contentsline {subsection}{\numberline {A.2.4}Conclusie}{25}%
\contentsline {section}{Literatuur}{26}%
