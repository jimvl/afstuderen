\babel@toc {dutch}{}
\contentsline {chapter}{\numberline {1.}Inleiding}{9}%
\contentsline {section}{\numberline {1.1}Bedrijfsachtergrond}{9}%
\contentsline {section}{\numberline {1.2}Probleemstelling}{12}%
\contentsline {section}{\numberline {1.3}Doelstelling}{13}%
\contentsline {chapter}{\numberline {2.}Requirements}{14}%
\contentsline {section}{\numberline {2.1}Inleiding}{14}%
\contentsline {section}{\numberline {2.2}Algemene beschrijving}{14}%
\contentsline {section}{\numberline {2.3}Systeemcontext}{14}%
\contentsline {subsection}{\numberline {2.3.1}Systeemfuncties}{14}%
\contentsline {section}{\numberline {2.4}Systeemfuncties}{16}%
\contentsline {subsection}{\numberline {2.4.1}Draadloze data overdracht}{16}%
\contentsline {subsubsection}{Stimulus en response}{16}%
\contentsline {subsubsection}{Functionele klanteisen}{16}%
\contentsline {subsection}{\numberline {2.4.2}Uitlezen en opslag data}{17}%
\contentsline {subsubsection}{Stimulus en response}{17}%
\contentsline {subsubsection}{Functionele klanteisen}{17}%
\contentsline {subsection}{\numberline {2.4.3}Updaten systeem en bestaande module (Optioneel)}{18}%
\contentsline {subsubsection}{Stimulus en response}{18}%
\contentsline {subsubsection}{Functionele klanteisen}{18}%
\contentsline {section}{\numberline {2.5}Niet-functionele klanteisen}{18}%
\contentsline {chapter}{\numberline {3.}Architectuur}{19}%
\contentsline {section}{\numberline {3.1}Ontwerpbesluiten}{19}%
\contentsline {section}{\numberline {3.2}Deelsystemen en koppelingen}{20}%
\contentsline {section}{\numberline {3.3}Beschrijving deelsystemen}{21}%
\contentsline {subsection}{\numberline {3.3.1}Deelsysteem 1: Control Data}{22}%
\contentsline {subsubsection}{Deelsysteem 1.1: Execute functions}{23}%
\contentsline {subsubsection}{Deelsysteem 1.2. Control Saved Data}{23}%
\contentsline {subsubsection}{Deelsysteem 1.3. Communicate Motor Control Unit}{23}%
\contentsline {subsubsection}{Deelsysteem 1.4. Control LED}{24}%
\contentsline {subsection}{\numberline {3.3.2}Deelsysteem 2: Transeive Data}{24}%
\contentsline {subsection}{\numberline {3.3.3}Deelsysteem 3: Flash Light}{24}%
\contentsline {section}{\numberline {3.4}Beschrijving interconnects}{24}%
\contentsline {section}{\numberline {3.5}Traceerbaarheid klanteisen}{25}%
\contentsline {chapter}{\numberline {4.}Detailontwerp}{26}%
\contentsline {subsection}{\numberline {4.0.1}Deelsystemen 1 \& 2}{26}%
\contentsline {subsubsection}{Hardware deelsystemen 1 \& 2}{26}%
\contentsline {paragraph}{Nordic}{26}%
\contentsline {paragraph}{ST}{26}%
\contentsline {paragraph}{NXP}{27}%
\contentsline {paragraph}{TI}{27}%
\contentsline {paragraph}{Silicon Labs}{27}%
\contentsline {paragraph}{Conclusie}{27}%
\contentsline {subsubsection}{Software deelsysteem 1}{29}%
\contentsline {subsubsection}{Software deelsysteem 2}{29}%
\contentsline {section}{\numberline {4.1}Unit 3: LED schakeling}{30}%
\contentsline {chapter}{Appendices}{31}%
\contentsline {chapter}{\numberline {A.}Onderzoek LED}{32}%
\contentsline {section}{\numberline {A.1}DC-DC conversie}{34}%
\contentsline {subsection}{\numberline {A.1.1}Charge pump}{34}%
\contentsline {subsubsection}{Simulatie en opbouw Charge pump}{36}%
\contentsline {subsubsection}{BOM charge pump}{40}%
\contentsline {subsection}{\numberline {A.1.2}Schakelen LED}{41}%
\contentsline {subsubsection}{Schakelen met transistor}{41}%
\contentsline {subsection}{\numberline {A.1.3}Schakelen met FET}{43}%
\contentsline {chapter}{\numberline {B.}Onderzoek draadloze technologieën}{45}%
\contentsline {section}{\numberline {B.1}Inleiding}{45}%
\contentsline {section}{\numberline {B.2}Draadloze technologieën}{47}%
\contentsline {subsection}{\numberline {B.2.1}Frequentie, bereik en maximale snelheid}{48}%
\contentsline {subsection}{\numberline {B.2.2}Topologie en gebruik smartphone}{49}%
\contentsline {subsection}{\numberline {B.2.3}Kosten}{50}%
\contentsline {section}{\numberline {B.3}Conclusie}{51}%
\contentsline {chapter}{\numberline {C.}Plan Van Aanpak}{52}%
\contentsline {section}{\numberline {C.1}Achtergronden}{53}%
\contentsline {section}{\numberline {C.2}Projectresultaat}{53}%
\contentsline {section}{\numberline {C.4}Projectgrenzen en randvoorwaarden}{57}%
\contentsline {section}{\numberline {C.5}Tussenresultaten}{57}%
\contentsline {section}{\numberline {C.6}Kwaliteit}{58}%
\contentsline {section}{\numberline {C.7}Projectorganisatie}{59}%
\contentsline {section}{\numberline {C.8}Planning}{61}%
\contentsline {section}{\numberline {C.9}Kosten en baten}{62}%
\contentsline {section}{\numberline {C.10}Risico's}{62}%
\contentsline {chapter}{\numberline {D.}Acceptatie test}{68}%
\contentsline {section}{\numberline {D.1}Voorbereidingen acceptatietest}{68}%
\contentsline {subsection}{\numberline {D.1.1}Visuele testomgeving}{68}%
\contentsline {subsubsection}{Hardware}{68}%
\contentsline {subsubsection}{Software}{69}%
\contentsline {subsection}{\numberline {D.1.2}Testomgeving seriële verbinding}{69}%
\contentsline {subsubsection}{Hardware}{69}%
\contentsline {subsubsection}{Software}{69}%
\contentsline {subsubsection}{Overig}{69}%
\contentsline {subsection}{\numberline {D.1.3}Test mesh}{69}%
\contentsline {subsubsection}{Hardware}{69}%
\contentsline {subsubsection}{Software}{69}%
\contentsline {subsection}{\numberline {D.1.4}Test data opslag}{70}%
\contentsline {subsubsection}{Hardware}{70}%
\contentsline {subsubsection}{Software}{70}%
\contentsline {subsection}{\numberline {D.1.5}Test software update}{70}%
\contentsline {subsubsection}{Hardware}{70}%
\contentsline {section}{\numberline {D.2}Acceptatietestbeschrijvingen}{71}%
\contentsline {subsection}{\numberline {D.2.1}Test Bluetooth naar serieel}{71}%
\contentsline {subsubsection}{Testcase 1.1: Commando's io-net}{71}%
\contentsline {subsubsection}{Testcase 1.2: Draaien motor}{72}%
\contentsline {subsection}{\numberline {D.2.2}Test Visuele feedback}{73}%
\contentsline {subsubsection}{Testcase 2.1: Test LED}{73}%
\contentsline {subsubsection}{Testcase 2.2: Test maximale afstand}{73}%
\contentsline {subsection}{\numberline {D.2.3}Test opgeslagen data}{74}%
\contentsline {subsubsection}{Testcase 3.1: Test Opslag data}{74}%
\contentsline {subsection}{\numberline {D.2.4}Test software update}{75}%
\contentsline {section}{\numberline {D.3}Traceerbaarheid klanteisen}{75}%
\contentsline {chapter}{\numberline {E.}Integratietest}{76}%
\contentsline {section}{\numberline {E.1}Testen}{76}%
\contentsline {subsection}{\numberline {E.1.1}Test 1: }{76}%
