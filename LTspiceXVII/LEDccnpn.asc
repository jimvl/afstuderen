Version 4
SHEET 1 1088 680
WIRE 608 -112 208 -112
WIRE 848 -112 608 -112
WIRE 976 -112 848 -112
WIRE 976 -80 976 -112
WIRE 208 -48 208 -112
WIRE 848 -32 848 -112
WIRE 208 80 208 32
WIRE 208 80 144 80
WIRE 336 80 208 80
WIRE 848 80 848 32
WIRE 144 96 144 80
WIRE 608 128 608 -32
WIRE 608 128 400 128
WIRE 784 128 608 128
WIRE -64 144 -160 144
WIRE 80 144 16 144
WIRE 608 144 608 128
WIRE 336 176 336 80
WIRE -160 192 -160 144
WIRE 848 192 848 176
WIRE 848 192 672 192
WIRE 848 208 848 192
WIRE -160 336 -160 272
WIRE 144 336 144 192
WIRE 144 336 -160 336
WIRE 400 336 400 224
WIRE 400 336 144 336
WIRE 608 336 608 240
WIRE 608 336 400 336
WIRE 848 336 848 288
WIRE 848 336 608 336
WIRE 976 336 976 0
WIRE 976 336 848 336
WIRE 608 368 608 336
FLAG 608 368 0
SYMBOL npn 336 128 R0
SYMATTR InstName Q1
SYMATTR Value BC547C
SYMBOL npn 672 144 M0
SYMATTR InstName Q2
SYMATTR Value BC547C
SYMBOL npn 784 80 R0
SYMATTR InstName Q3
SYMATTR Value BC547C
SYMBOL res 192 -64 R0
SYMATTR InstName R1
SYMATTR Value 100k
SYMBOL res 592 -128 R0
SYMATTR InstName R2
SYMATTR Value 10k
SYMBOL res 832 192 R0
SYMATTR InstName R3
SYMATTR Value 10
SYMBOL voltage 976 -96 R0
WINDOW 123 0 0 Left 0
WINDOW 39 0 0 Left 0
SYMATTR InstName V1
SYMATTR Value 3.3
SYMBOL voltage -160 176 R0
WINDOW 123 0 0 Left 0
WINDOW 39 0 0 Left 0
SYMATTR InstName V2
SYMATTR Value 3.3
SYMBOL LED 832 -32 R0
SYMATTR InstName D1
SYMATTR Value LXK2-PW14
SYMBOL npn 80 96 R0
SYMATTR InstName Q4
SYMATTR Value BC547C
SYMBOL res 32 128 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R4
SYMATTR Value 100
TEXT -194 392 Left 2 !.tran 0.1
