#include "BluetoothSerial.h"

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

char bluetooth;
byte bluetooth_nummer;

BluetoothSerial SerialBT;

void setup() { //Setup van de ESP32 zodat de bluetooth kan werken
  pinMode (22, OUTPUT);
  pinMode (17, OUTPUT);
  Serial.begin(9600);
  SerialBT.begin("ESP32test"); 
  Serial.println("The device started, now you can pair it with bluetooth!");
}

void loop() 
{
  if (Serial.available()) //Zenden via BT naar de app
  {
    SerialBT.available() == false;
    SerialBT.write(Serial.read());
    delay(20);
  }

  if (SerialBT.available()) //Gegevens binnenkrijgen van de app
    {
      Serial.available() == false;
      bluetooth = SerialBT.read();
      if (bluetooth == 'X' && bluetooth) //Water gedeelte (X)
      {      
        //Serial.println(" ");
        bluetooth_nummer = SerialBT.read();
        //digitalWrite(17, HIGH);
        //delay(4000);
        //digitalWrite(17, LOW); 
        Serial.print(bluetooth);
        bluetooth = NULL;
      } 
      /*if (bluetooth == 'Y' && bluetooth) //Voer gedeelte (Y)
      {      
        Serial.println(" ");
        bluetooth_nummer = SerialBT.read();
        Serial.write(bluetooth);
        bluetooth = NULL;
      }
      if (bluetooth) //Printen van de tijd waardes
      {
        Serial.write(bluetooth);
      }*/
    }
    delay(20);
}  