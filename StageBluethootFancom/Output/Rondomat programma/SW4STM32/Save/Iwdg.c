/**
  ******************************************************************************
  * @file    iwdg.c
  *
  * @note
  *  Copyright(C) PigTek Europe GmbH, 2018
  *  All rights reserved.
  *
  *  Created on: 02.06.2018
  *      Author: fnordbeck
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2013 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "board.h"
#include "stm32f0xx_hal.h"

#ifdef IWDG_ENABLE
#include "stm32f0xx_hal_iwdg.h"
#include "iwdg.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* IWDG and TIM handlers declaration */
static IWDG_HandleTypeDef   IwdgHandle;
TIM_HandleTypeDef           Input_Handle;
RCC_ClkInitTypeDef          RCC_ClkInitStruct;
RCC_OscInitTypeDef          RCC_OscInitStruct;

uint16_t tmpCC4[2] = {0, 0};
__IO uint32_t uwLsiFreq = 0;
__IO uint32_t uwCaptureNumber = 0;


/* Private function prototypes -----------------------------------------------*/
static uint32_t GetLSIFrequency(void);

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Get the iwdg reset status
  * @param  None
  * @retval IWDG_RESET if iwdg reset has occurred otherwise NO_IWDG_RESET
  */
Iwdg_StatusTypeDef iwdgIfReset(void){
	bool_t resetStatus;

		// Check if the system has resumed from IWDG reset
	if (__HAL_RCC_GET_FLAG(RCC_FLAG_IWDGRST) == IWDG_RESET)
	{
		// IWDGRST flag set: do anything, switch on a LED or something else ...
	  resetStatus = IWDG_RESET;
	}else{
	  resetStatus = NO_IWDG_RESET;
	}
		// Clear reset flags in any cases
	__HAL_RCC_CLEAR_RESET_FLAGS();

	return resetStatus;
}

/**
  * @brief  Configure & Start the IWDG peripheral
  * @note	is not used when the iwdg is created with CubeMX
  * @param  None
  * @retval HAL status
  */
HAL_StatusTypeDef iwdgConfig(void){

	// Clear reset flags in any cases
	__HAL_RCC_CLEAR_RESET_FLAGS();

	// Get the LSI frequency: TIM14 is used to measure the LSI frequency
	uwLsiFreq = GetLSIFrequency();

	/* Set counter reload value to obtain 2 sec. IWDG TimeOut.
	 * IWDG counter clock Frequency = uwLsiFreq
	 * Set Prescaler to 32 (IWDG_PRESCALER_32)
	 * Timeout Period = (Reload Counter Value * 32) / uwLsiFreq
	 * So Set Reload Counter Value = (2 * uwLsiFreq) / 32
	 */
	IwdgHandle.Instance = IWDG;
	IwdgHandle.Init.Prescaler = IWDG_PRESCALER_32;
	IwdgHandle.Init.Reload = (2 * uwLsiFreq / 32);
	IwdgHandle.Init.Window = IWDG_WINDOW_DISABLE;

	if(HAL_IWDG_Init(&IwdgHandle) != HAL_OK)
	{
		/* Initialization Error */
		return HAL_ERROR;
	}
	return HAL_OK;
}

/**
  * @brief  Refresh the IWDG reload counter
  * @param  None
  * @retval HAL status
  */
HAL_StatusTypeDef iwdgRefresh(IWDG_HandleTypeDef *hiwdg){

	if(HAL_IWDG_Refresh(hiwdg) != HAL_OK)
	{
	  /* Refresh Error */
	  return HAL_ERROR;
	}
	return HAL_OK;
}


/**
  * @brief  Configures TIM14 to measure the LSI oscillator frequency.
  * @param  None
  * @retval LSI Frequency
  */
static uint32_t GetLSIFrequency(void)
{
  TIM_IC_InitTypeDef    TIMInput_Config;


  /* Configure the TIM peripheral *********************************************/
  /* Set TIMx instance */
  Input_Handle.Instance = TIM14;

  /* TIM14 configuration: Input Capture mode ---------------------
     The LSI oscillator is connected to TIM14 CH1.
     The Rising edge is used as active edge.
     The TIM14 CCR1 is used to compute the frequency value.
  ------------------------------------------------------------ */
  Input_Handle.Init.Prescaler         = 0;
  Input_Handle.Init.CounterMode       = TIM_COUNTERMODE_UP;
  Input_Handle.Init.Period            = 0xFFFF;
  Input_Handle.Init.ClockDivision     = 0;
  Input_Handle.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if(HAL_TIM_IC_Init(&Input_Handle) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }

  /* Connect internally the TIM14_CH1 Input Capture to the LSI clock output */
  HAL_TIMEx_RemapConfig(&Input_Handle, TIM_TIM14_MCO);

  /* Connect internally the MCO to LSI */
  HAL_RCC_MCOConfig(RCC_MCO, RCC_MCO1SOURCE_LSI, RCC_MCODIV_1);

  /* Configure the Input Capture of channel 1 */
  TIMInput_Config.ICPolarity  = TIM_ICPOLARITY_RISING;
  TIMInput_Config.ICSelection = TIM_ICSELECTION_DIRECTTI;
  TIMInput_Config.ICPrescaler = TIM_ICPSC_DIV8;
  TIMInput_Config.ICFilter    = 0;
  if(HAL_TIM_IC_ConfigChannel(&Input_Handle, &TIMInput_Config, TIM_CHANNEL_1) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }

  /* Start the TIM Input Capture measurement in interrupt mode */
  if(HAL_TIM_IC_Start_IT(&Input_Handle, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }

  /* Wait until the TIM14 get 2 LSI edges */
  while(uwCaptureNumber != 2)
  {
  }

  /* Disable TIM14 CC1 Interrupt Request */
  HAL_TIM_IC_Stop_IT(&Input_Handle, TIM_CHANNEL_1);

  /* Deinitialize the TIM14 peripheral registers to their default reset values */
  HAL_TIM_IC_DeInit(&Input_Handle);

  return uwLsiFreq;
}

/**
  * @brief  Input Capture callback in non blocking mode
  * @param  htim : TIM IC handle
  * @retval None
*/
void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)
{
  uint32_t lsiperiod = 0;

  /* Get the Input Capture value */
  tmpCC4[uwCaptureNumber++] = HAL_TIM_ReadCapturedValue(&Input_Handle, TIM_CHANNEL_1);

  if (uwCaptureNumber >= 2)
  {
    /* Compute the period length */
    lsiperiod = (uint16_t)(0xFFFF - tmpCC4[0] + tmpCC4[1] + 1);

    /* Frequency computation */
    uwLsiFreq = (uint32_t) SystemCoreClock / lsiperiod;
    uwLsiFreq *= 8;
  }
}

const struct iwdgApi Iwdg = {
		iwdgIfReset,
		iwdgConfig,
		iwdgRefresh
};
#endif
