/*
  ******************************************************************************
  * @file    wwdg.h
  *
  * @brief   wwdg declarations and defines
  *
  *
  * @note
  *  Copyright(C) PigTek Europe GmbH, 2018
  *  All rights reserved.
  *
  *  Created on: 01.06.2018
  *      Author: fnordbeck
  ******************************************************************************
  *
  * 1-- enable and configure the wwdg with CubeMX
  *
  * 2-- after MX_WWDG_Init(); is executed calculate the timeout for the wwdg
  *  	  // calculate time out for the wwdg. Add 1ms to secure round number to upper number
  * wdgTimeOut = Wwdg.TimeoutCalculation((hwwdg.Init.Counter-hwwdg.Init.Window) + 1) + 1;
  *
  * 3-- insert the wwdg refresh code in a desired task to trigger the wwdg in the desired time
  * if(i >= wdgTimeOut){
  * i = 0;
  * 	if (Wwdg.Refresh() != HAL_OK){
  * 		Error_Handler();
  * 	}
  * }
  * i++;
  *
  */

#ifndef __WWDG_H_
#define __WWDG_H_

/* Includes ------------------------------------------------------------------*/


typedef enum
{
  NO_WWDG_RESET,
  WWDG_RESET
} Wwdg_StatusTypeDef;

/* WWDG APIs */
struct wwdgApi{

	/**
	  * @brief  Get the WWDG reset status
	  * @param  None
	  * @retval WWDG_RESET if wwdg reset has occurred otherwise NO_WWDG_RESET
	  */
	Wwdg_StatusTypeDef ( *IfReset )(void);

	/** WWDG clock counter = (PCLK1 (8MHz)/4096)/1) = 1953.125 Hz (~512 us)
	  * WWDG Window value = 80 means that the WWDG counter should be refreshed only
	  * when the counter is below 80 (and greater than 64 (63+1)) otherwise a reset will
	  * be generated.
	  * WWDG Counter value = 127, WWDG timeout = ~512 us * 64 = 32.7 ms
	  * In this case the refresh window is comprised between : ~512 * (127-80) = 24.1 ms and ~512 * 64 = 32.7 ms
	  */
	/**
	  * @brief  Timeout calculation function for the WWDG.
	  *         This function calculates any timeout related to
	  *         WWDG with given prescaler and system clock.
	  *         Must be called after "MX_WWDG_Init" is called.
	  * @param  timevalue: period in term of WWDG counter cycle.
	  * @retval timeoutValue
	  */
	uint32_t ( *TimeoutCalculation )(uint32_t timeValue);

	/**
	  * @brief  Refresh the WWDG reload counter
	  * @param  None
	  * @retval HAL status
	  */
	HAL_StatusTypeDef ( *Refresh )(void);
};

extern const struct wwdgApi Wwdg;

#endif /* __WWDG_H_ */
