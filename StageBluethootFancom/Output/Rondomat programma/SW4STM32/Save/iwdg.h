/*
  ******************************************************************************
  * @file    iwdg.h
  *
  * @brief   iwdg declarations and defines
  *
  *
  * @note
  *  Copyright(C) PigTek Europe GmbH, 2018
  *  All rights reserved.
  *
  *  Created on: 02.06.2018
  *      Author: fnordbeck
  ******************************************************************************
  */

#ifndef __IWDG_H_
#define __IWDG_H_

/* Includes ------------------------------------------------------------------*/


typedef enum
{
  NO_IWDG_RESET,
  IWDG_RESET
} Iwdg_StatusTypeDef;

/* IWDG APIs */
struct iwdgApi{

	/**
	  * @brief  Get the iwdg reset status
	  * @param  None
	  * @retval IWDG_RESET if iwdg reset has occurred otherwise NO_IWDG_RESET
	  */
	Iwdg_StatusTypeDef ( *IfReset )(void);

	/**
	  * @brief  Configure & Start the IWDG peripheral
	  * @note	is not used when the iwdg is created with CubeMX
	  * @param  None
	  * @retval HAL status
	  */
	HAL_StatusTypeDef ( *Config )(void);

	/**
	  * @brief  Refresh the IWDG reload counter
	  * @param  None
	  * @retval HAL status
	  */
	HAL_StatusTypeDef ( *Refresh )(IWDG_HandleTypeDef *hiwdg);
};

extern const struct iwdgApi Iwdg;

#endif /* __IWDG_H_ */
