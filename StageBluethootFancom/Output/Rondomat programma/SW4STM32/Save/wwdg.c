/**
  ******************************************************************************
  * @file    wwdg.c
  *
  * @note
  *  Copyright(C) PigTek Europe GmbH, 2018
  *  All rights reserved.
  *
  *  Created on: 01.06.2018
  *      Author: fnordbeck
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2013 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */
#ifdef WWDG_ENABLE
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "board.h"
#include "stm32f0xx_hal.h"
#include "stm32f0xx_hal_wwdg.h"
#include "wwdg.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* WWDG and TIM handlers declaration */
extern WWDG_HandleTypeDef hwwdg;

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Get the iwdg reset status
  * @param  None
  * @retval WWDG_RESET if wwdg reset has occurred otherwise NO_WWDG_RESET
  */
Wwdg_StatusTypeDef wwdgIfReset(void){
	bool_t resetStatus;

		// Check if the system has resumed from IWDG reset
	if (__HAL_RCC_GET_FLAG(RCC_FLAG_WWDGRST) == WWDG_RESET)
	{
		// WWDGRST flag set: do anything, switch on a LED or something else...
	  resetStatus = WWDG_RESET;
	}else{
	  resetStatus = NO_WWDG_RESET;
	}
		// Clear reset flags in any cases
	__HAL_RCC_CLEAR_RESET_FLAGS();

	return resetStatus;
}

/** WWDG clock counter = (PCLK1 (8MHz)/4096)/1) = 1953.125 Hz (~512 us)
  * WWDG Window value = 80 means that the WWDG counter should be refreshed only
  * when the counter is below 80 (and greater than 64 (63+1)) otherwise a reset will
  * be generated.
  * WWDG Counter value = 127, WWDG timeout = ~512 us * 64 = 32.7 ms
  * In this case the refresh window is comprised between : ~512 * (127-80) = 24.1 ms and ~512 * 64 = 32.7 ms
  */
/**
  * @brief  Timeout calculation function for the WWDG.
  *         This function calculates any timeout related to
  *         WWDG with given prescaler and system clock.
  *         Must be called after "MX_WWDG_Init" is called.
  * @param  timevalue: period in term of WWDG counter cycle.
  * @retval timeoutValue
  */
uint32_t wwdgTimeoutCalculation(uint32_t timeValue)
{
	uint32_t timeoutValue = 0;
	uint32_t pclk1 = 0;
	uint32_t wdgtb = 0;

	/* considering APB divider is still 1, use HCLK value */
	pclk1 = HAL_RCC_GetPCLK1Freq();

	/* get prescaler */
	wdgtb = (1 << ((hwwdg.Init.Prescaler) >> 7)); /* 2^WDGTB[1:0] */

	/* calculate timeout */
	timeoutValue = ((4096 * wdgtb * timeValue) / (pclk1 / 1000));

	return timeoutValue;
}

/**
  * @brief  Refresh the WWDG reload counter
  * @param  None
  * @retval HAL status
  */
HAL_StatusTypeDef wwdgRefresh(void){

	if (HAL_WWDG_Refresh(&hwwdg) != HAL_OK)
	{
	  /* Refresh Error */
	  return HAL_ERROR;
	}
	return HAL_OK;
}



const struct wwdgApi Wwdg = {
		wwdgIfReset,
		wwdgTimeoutCalculation,
		wwdgRefresh
};
#endif
