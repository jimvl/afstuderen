################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/adc.c \
../Src/crc16.c \
../Src/debug.c \
../Src/display.c \
../Src/freertos.c \
../Src/iap.c \
../Src/iof.c \
../Src/keyboard.c \
../Src/main.c \
../Src/menu.c \
../Src/rondomat.c \
../Src/spi.c \
../Src/stm32f0xx_hal_msp.c \
../Src/stm32f0xx_hal_timebase_TIM.c \
../Src/stm32f0xx_it.c \
../Src/system_stm32f0xx.c 

OBJS += \
./Src/adc.o \
./Src/crc16.o \
./Src/debug.o \
./Src/display.o \
./Src/freertos.o \
./Src/iap.o \
./Src/iof.o \
./Src/keyboard.o \
./Src/main.o \
./Src/menu.o \
./Src/rondomat.o \
./Src/spi.o \
./Src/stm32f0xx_hal_msp.o \
./Src/stm32f0xx_hal_timebase_TIM.o \
./Src/stm32f0xx_it.o \
./Src/system_stm32f0xx.o 

C_DEPS += \
./Src/adc.d \
./Src/crc16.d \
./Src/debug.d \
./Src/display.d \
./Src/freertos.d \
./Src/iap.d \
./Src/iof.d \
./Src/keyboard.d \
./Src/main.d \
./Src/menu.d \
./Src/rondomat.d \
./Src/spi.d \
./Src/stm32f0xx_hal_msp.d \
./Src/stm32f0xx_hal_timebase_TIM.d \
./Src/stm32f0xx_it.d \
./Src/system_stm32f0xx.d 


# Each subdirectory must supply rules for building sources it contributes
Src/%.o: ../Src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -mfloat-abi=soft '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F030x8 -I"D:/Stage Remote control Martijn/Rondomat programma/SW4STM32/Rondomat/Inc" -I"D:/Stage Remote control Martijn/Rondomat programma/SW4STM32/Rondomat/Drivers/STM32F0xx_HAL_Driver/Inc" -I"D:/Stage Remote control Martijn/Rondomat programma/SW4STM32/Rondomat/Drivers/STM32F0xx_HAL_Driver/Inc/Legacy" -I"D:/Stage Remote control Martijn/Rondomat programma/SW4STM32/Rondomat/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM0" -I"D:/Stage Remote control Martijn/Rondomat programma/SW4STM32/Rondomat/Drivers/CMSIS/Device/ST/STM32F0xx/Include" -I"D:/Stage Remote control Martijn/Rondomat programma/SW4STM32/Rondomat/Middlewares/Third_Party/FreeRTOS/Source/include" -I"D:/Stage Remote control Martijn/Rondomat programma/SW4STM32/Rondomat/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"D:/Stage Remote control Martijn/Rondomat programma/SW4STM32/Rondomat/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


