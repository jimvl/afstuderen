/*
 * rondomat.h
 *
 *@brief Common display defines and API declarations
 *
 * @note
 * Copyright(C) PigTek Europe GmbH, 2017
 * All rights reserved.
 *
 *  Created on: 07.07.2017
 *      Author: fnordbeck
 */

#ifndef __RONDOMAT_H_
#define __RONDOMAT_H_

#define MIN_DOSING_TIME 		4		// Min dosing time

#define TROUGH_MESURE_TIME		5		// Timout f�r Trogvollmessung
#define ADC_SAMPLES				4		// Anzahl von Sampels, die gemittelt werden	(muss = 2^n sein! Also: 2, 4, 8, 16, ...)

#define RONDO_IOPORT_TMO		3		//  => TMO = 300ms, Zeit mit denen die IO Funktionen alle 100ms gesetzt wird

#define START					1
#define DETECT_ZERO				2
#define MONITOR_TROUGH_LEVEL	3
#define FINISHED				4
#define STOP					0
#define TROUGH_FULL				1
#define TROUGH_EMPTY			0
#define FEED_SENS_ACTIVE		1
#define FEED_SENS_NOT_ACTIVE	0

typedef enum eRondo {
	WAIT_TROUGH_EMPTY,
	WAIT_PAUSE,
	FEEDING,
	MEASURE_TROUGH_VALUE,
} eRondo;

extern volatile int8_t GradientTroughValue;
extern volatile uint16_t FilteredTroughValue;
extern volatile uint8_t TroughFullLevel;
extern volatile bool_t TriggerFeeding;

/* Rondomat APIs */
struct RondomatApi{


	void ( *SetMotorAndWaterManually )(bool_t state);

	void ( *SetWaterManually )(bool_t state);

	void ( *ControlOutputStates )(void);

	void ( *ReadTroughSensor )(void);

	void ( *Function )(void);

	void ( *Bluetooth )(void); //Toegevoegd door martijn

};

extern const struct RondomatApi Rondomat;

#endif /* __RONDOMAT_H_ */
