/*
 *crc16.h
 *@brief
 *
 * @note
 * Copyright(C) PigTek Europe GmbH, 2017
 * All rights reserved.
 *
 * Created on: 14.06.2014
 *     Author: fnordbeck
 */

#ifndef __CRC16_H_
#define __CRC16_H_

uint16_t CRC16LoopTable(volatile uint8_t* ptr, uint8_t len);

uint16_t CRC16(volatile uint16_t crc, uint8_t val);

uint16_t CRC16Loop(volatile uint8_t* ptr, uint8_t len);

#endif /* __CRC16_H_ */
