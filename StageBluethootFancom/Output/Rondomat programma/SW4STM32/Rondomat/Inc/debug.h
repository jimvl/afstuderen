/*
  ******************************************************************************
  * @file    debug.h
  *
  * @brief   debug declarations and defines
  *
  *
  * @note
  *  Copyright(C) PigTek Europe GmbH, 2018
  *  All rights reserved.
  *
  *  Created on: 11.06.2018
  *      Author: fnordbeck
  ******************************************************************************
  */

#ifndef __DEBUG_H_
#define __DEBUG_H_

#include "stdio.h"

#ifdef DEBUG_ENABLE 
#define DEBUG_PRINTF(...) printf(__VA_ARGS__)
#else
#define DEBUG_PRINTF(...)
#endif

#endif /* __DEBUG_H_ */
