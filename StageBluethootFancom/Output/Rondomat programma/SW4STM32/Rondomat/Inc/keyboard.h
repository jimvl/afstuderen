/*
 * keybord.h
 *
 ** @brief Common keyboard defines and API functions
 *
 * @note
 * Copyright(C) PigTek Europe GmbH, 2017
 * All rights reserved.
 *
 *  Created on: 04.07.2017
 *      Author: fnordbeck
 */

#ifndef __KEYBORD_H_
#define __KEYBORD_H_

// Defines
#define TIME_SHORT_PRESS	7				// Short pressed time
#define TIME_LONG_PRESS		40				// Long pressed time
#define PRESSED				0				// Status of the port pin when the button is pressed

/* Keyboard APIs */
struct KeyboardApi{

	/**
	 * @brief	Initialize the keyboard functions.
	 * @return	None
	 */
	void ( *Init )(void);

	/**
	 * @brief	Get the status of the button
	 * @param	ID	: Button ID
	 * @return	Status
	 */
	Button_StatusTypeDef ( *GetStatus )(Button_TypeDef ID);

	/**
	 * @brief	Analyzes the button and set the status
	 * @param	pinState, ID	: State of the pin and the button ID
	 * @return	None
	 */
	void ( *Analyze_Key )(GPIO_PinState pinState, Button_TypeDef ID);

};
extern const struct KeyboardApi Keyboard;

#endif /* __KEYBORD_H_ */
