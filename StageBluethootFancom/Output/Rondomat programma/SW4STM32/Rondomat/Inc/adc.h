/*
  ******************************************************************************
  * @file    adc.h
  *
  * @brief   adc declarations and defines
  *
  *
  * @note
  *  Copyright(C) PigTek Europe GmbH, 2018
  *  All rights reserved.
  *
  *  Created on: 31.05.2018
  *      Author: fnordbeck
  ******************************************************************************
  */

#ifndef __ADC_H_
#define __ADC_H_

#define _8BIT 	4	// 8 bit AD conversion
#define _9BIT 	3	// 9 bit AD conversion
#define _10BIT	2	// 10 bit AD conversion
#define _11BIT	1	// 11 bit AD conversion
#define _12BIT 	0	// 12 bit AD conversion

#define ADC_RESOLUTION _8BIT		// Set the AD conversion of the trough value to the required bits

/* Private typedef -----------------------------------------------------------*/

/* public variables ---------------------------------------------------------*/

/* public functions ---------------------------------------------------------*/

/* ADC APIs */
struct adcApi{

	/**
	  * @brief  Calibrate the adc
	  * @param  none
	  * @retval HAL status
	  */
	HAL_StatusTypeDef ( *Calibrate )(void);

	/**
	  * @brief  Start the adc in DMA mode
	  * @param  none
	  * @retval HAL status
	  */
	HAL_StatusTypeDef ( *Start )(void);

	/**
	  * @brief  Stop the adc conversion
	  * @param  none
	  * @retval HAL status
	  */
	HAL_StatusTypeDef ( *Stop )(void);

	/**
	  * @brief  switch on the trough sensor
	  * @param  none
	  * @retval none
	  */
	void ( *SwitchOnTroughSensor )(void);

	/**
	  * @brief  switch of the trough sensor
	  * @param  none
	  * @retval none
	  */
	void ( *SwitchOffTroughSensor )(void);

	/**
	  * @brief  get the raw adc troug hvalue
	  * @param  none
	  * @retval raw adc value or on error with 0xffff
	  */
	uint16_t ( *GetTroughValue)();

	/**
	  * @brief  get the raw adc 24V power value
	  * @param  none
	  * @retval raw adc value or on error with 0xffff
	  */
	uint16_t ( *Get24VPowerValue)(void);

	/**
	  * @brief  sample the raw adc values, must called cyclic
	  * @param  none
	  * @retval none
	  */
	void ( *Sample )(void);
};

extern const struct adcApi Adc;

#endif /* __ADC_H_ */
