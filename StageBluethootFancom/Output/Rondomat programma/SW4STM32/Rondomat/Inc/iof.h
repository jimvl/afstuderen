/*
 * iof.h
 *
 *@brief Input output functions
 *
 * @note
 * Copyright(C) PigTek Europe GmbH, 2017
 * All rights reserved.
 *
 *  Created on: 06.07.2017
 *      Author: fnordbeck
 */

#ifndef __IOF_H_
#define __IOF_H_

// Enum of the output states
typedef enum {
    OFF		= 0x00U,            // Output is OFF
    ON		= 0x01U             // Output is ON
} Output_StatusTypeDef;


typedef struct {
	uint32_t Time;		 // On time in ms
} Output_CtrDataTypeDef; //, *pOutputCtrReg;


// public

/* IO APIs */
struct IOApi{

	/**
	 * @brief	Initialize the output control register.
	 * @return	None
	 */
	void ( *InitOutputCtrRegs )(void);

	/**
	 * @brief	Set the output
	 * @param	ID	: Output ID
	 * @param	time: On_time of the output
	 * @return	None
	 */
	void ( *SetOutput )(uint8_t ID, uint32_t time);

	/**
	 * @brief	Get the status of the output
	 * @param	ID	: Output ID
	 * @return	Status
	 */
	Output_StatusTypeDef ( *AnalyzeOutput )(Output_TypeDef ID);
};

extern const struct IOApi IO;

#endif /* __IOF_H_ */
