/*
  ******************************************************************************
  * @file    spi.h
  *
  * @brief   spi declarations and defines
  *
  *
  * @note
  *  Copyright(C) PigTek Europe GmbH, 2018
  *  All rights reserved.
  *
  *  Created on: 24.04.2018
  *      Author: fnordbeck
  ******************************************************************************
  */

#ifndef __SPI_H_
#define __SPI_H_

#define SPI1_TIMEOUT	100 	// 100ms timeout for SPI1 transmit data

/* Includes ------------------------------------------------------------------*/

/* SPI APIs */
struct spiApi{

	/**
	 * @brief	Start the transmission communication process
	 * @return	HAL Status
	 */
	HAL_StatusTypeDef ( *WriteData )(uint8_t *dataIn, uint16_t dataLength);
};

extern const struct spiApi SPI;

#endif /* __SPI_H_ */
