/*
 * display.h
 *
 ** @brief Common display defines and API declarations
 *
 * @note
 * Copyright(C) PigTek Europe GmbH, 2017
 * All rights reserved.
 *
 *  Created on: 07.07.2017
 *      Author: fnordbeck
 */

#ifndef __DISPLAY_H_
#define __DISPLAY_H_

// defines
typedef enum {
	SYM_BLANK = 10,
	SYM_MINUS =	11,
	SYM_DOT = 12,
	SYM_F = 13,
}SymDefines_TypDef;

/* Display APIs */
struct DisplayApi{

	/**
	 * @brief	Sets the color of the duo LED
	 * @param	color	: Color of the LED
	 * @return	None
	 */
	void ( *SetDuoLedtoColor )(uint8_t color);


	/**
	 * @brief	Toggles the color of the duo LED
	 * @param	color	: Color of the LED
	 * @return	None
	 */
	void ( *ToggleDuoLed)(uint8_t color);

	/**
	 * @brief	Set up and initialize the 7 segment display.
	 * @return	None
	 */
	void ( *Init )(void);

	/**
	 * @brief	Sets the display value
	 * @param	value
	 * @return	None
	 */
	void ( *WriteTo7Seg ) (uint8_t *value, uint8_t dotPos);

	/**
	 * @brief	set the brightness and send data to the 7 segment displays
	 * @param	brightness: brightness of the 7 segment display in steps from 1 to 10
	 * @return	None
	 */
	void ( *SetBrightness)(uint8_t brightness);

};

extern const struct DisplayApi Display;

#endif /* __DISPLAY_H_ */
