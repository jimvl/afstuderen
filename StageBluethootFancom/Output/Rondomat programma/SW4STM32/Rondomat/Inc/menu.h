/*
 * menu.h
 * @brief Common menu defines and API declarations
 *
 * @note
 * Copyright(C) PigTek Europe GmbH, 2017
 * All rights reserved.
 *
 *  Created on: 06.07.2017
 *      Author: fnordbeck
 */

#ifndef __MENU_H_
#define __MENU_H_

#include "keyboard.h"

//#define DEBUG_FLASH_INFO

typedef enum eMenuID {
	M_DEFAULT,				// Menü Futter- und Wassereinstellungen (default)
	M_PAUSE,				// Menü Futterpause
	M_SENSOR_SENSITIVETY,	// Menü Sensorempfindlichkeit
	M_RESET_DEF,			// Menü Reset der Rondomatdaten
	M_VERSION,				// Menü Anzeige der Programmversion
	M_WORKING_VERSION,		// Menü working version
	M_DISP_BRIGHTNESS,		// brightness of the 7 segment displays
	M_S_VALUE,				// Menü Anzeige des Futter-Sensorwertes
	M_S_GRADIENT,			// Menü Anzeige der Steigung des Futter-Sensormesswertes
} eMenuID;

typedef enum eLEDState {
	LED_DEFAULT,
	LED_SENSOR,
	LED_MENU,
} eLEDState;

// Defines								 // Bei dem Defaultwerten der Menüs die Min. Max. Werte beachten!
#define MAX_MENUE			M_S_GRADIENT // groesste moegliche Menue Nummer (siehe enum eMenuID)
#define MIN_MENUE			M_PAUSE		 // kleinst moegliche Menue Nummer (siehe enum eMenuID)
#define DEFAULT_WATER		0			 // Defaultwert für Menu-Water
#define DEFAULT_FEED		1			 // Defaultwert für Menu-Feed
#define DEFAULT_PAUSE		10			 // Defaultwert für Menu-Pause
#define DEFAULT_POWER		1			 // Defaultwert für Einschaltzustand
#define	DEFAULT_TROUGH_EMPTY_LEVEL 120 	 // 60	 // Trogmesswerte > DEFAULT_TROUGH_EMPTY_LEVEL werden als Trogleer angenommen
#define DEFAULT_TROUGH_LEVEL 0			 // Defaultwert für den Vollmeldelevel, 0 startet nach Reset sofort eine Fütterung
#define DEFAULT_SENSOR_SENSITIVETY 20 	 // 10 // Sensitivity of the trough sensor in digit
#define DEFAULT_BRIGHTNESS	8			 // Brightness of the 7 segment display
#define DEFAULT_WORKING_VERSION     7	 // Rondomat works like version 7
#define DEFAULT_RESET_COUNTS		0	 // set reset counts to 0


#define PROTEC_MENU_TMO		250			// Zeit in MenuHandler()-Aufrufen für Tastenfunktion "Protected Menu"
										// 1-255

										// Es muss gelten DEFAULT_MENU_TMO > DEFAULT_SAVE_TMO > 0
#define MENU_TMO			30			// Rücksprung ins DefaultMenu nach sovielen Sec.
#define SAVE_TMO			3			// Speichern ins Flash in Sec.


//#define CALLS_PRO_SEC		50			// Aufrufe von MenuHandler() je Sekunde
#define FLASH_FAILED		0xAA		// Kennung für Flash bereits min. einmal beschrieben

// Menu structure
struct stMenu {
	uint8_t Seg[DISPLAY_SEGs];		// Segmente for Text
	uint8_t DotPos;						// Dot position
	eLEDState DuoLed;					// Momentaner User der Duo-LED
	eMenuID Menu;						// Aktuelles Menu
	uint8_t MenuTMO;					// Timeout für Rücksprung zum Standard-Menu
	uint8_t SaveTMO;					// Timeout für Speichern ins Flash
};

// Menu data structure
struct stMenuData {
	uint8_t FailedFlag;					// Magic Key: FLASH_FAILED -> Data in Flash valid
	int8_t Water;	 					// -3 bis 60
	uint8_t Feed;						// 1 bis 60
	uint8_t Pause;						// 1 bis 99
	uint8_t PowerOn;					// 0 oder 1
	uint8_t SensorSensitivety;			// Trough sensor sensitivety in digit
	uint8_t Brightness;					// Brightness of the 7 segment display
	uint8_t WorkingVersion;				// Working version of the Rondomat
	uint32_t ResetCounts;				// Reset counter
};

extern const uint8_t ProgVersion[];
extern volatile struct stMenuData stData;

#define MAX_FLASH_DATA_TYPES			2

		// Used pages of the Flash memory
#define FLASH_PAGES						2
		// Flash memory address
#define FLASH_DATA_ADDRESS				FLASH_DATA_END_ADDR - (FLASH_PAGES*FLASH_PAGE_SIZE)

		// Rondomat data 7 Bytes + 4 Bytes write counter <= FLASH_PAGE_SIZE => 1 Page
#define RONDOMAT_FLASH_DATA				FLASH_DATA_ADDRESS
#define RONDOMAT_DATA_PAGES				1

		// Device data = 0 Bytes + 4 Bytes write counter <= FLASH_PAGE_SIZE => 1 Page
#define DEVICE_FLASH_DATA				RONDOMAT_FLASH_DATA + RONDOMAT_DATA_PAGES * FLASH_PAGE_SIZE
#define DEVICE_DATA_PAGES				1

// public Funktionen
	/* Menu APIs */
struct MenuApi{

	/**
	 * @brief	Set up and initialize the menu function.
	 * @return	None
	 */
	void ( *Init )(void);

	/**
	 * @brief	Monitoring the timeouts for the menu function. The main must call the menu timeouts every second
	 * @return	None
	 */
	void ( *Timeouts )(void);

	/**
	 * @brief	Menu handling. The main program must call the menu handling every 10ms
	 * @param	State of the buttons
	 * @return	None
	 */
	void ( *Handling )(Button_StatusTypeDef Lup, Button_StatusTypeDef Ldw, Button_StatusTypeDef Rup, Button_StatusTypeDef Rdw);

	/**
	 * @brief	Sets the sensor led
	 * @param	state of the LED
	 * @return	None
	 */
	void ( *SetSensorLed )(uint8_t state);

};
extern const struct MenuApi Menu;

#endif /* __MENU_H_ */
