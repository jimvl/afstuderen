/**
  * @file    board.h
  * @brief   This file contains definitions for:
  *          - LEDs, push-buttons, ADC for the trough sensor, PWM for the outputs and SPI for the 7-segment
  *            displays available on the Rondomat board
  *          -
  * @note
  * 		Copyright(C) PigTek Europe GmbH, 2018
  * 		All rights reserved.
  *
  *  		Created on: 03.01.2018
  *      	Author: fnordbeck
  ******************************************************************************
  * IWDG watchdog is used
  * 1-- calculate the iwdg timeout:
  * 	Set counter reload value to obtain 2 sec. IWDG TimeOut.
  * 	IWDG counter clock Frequency = LsiFreq (40kHz)
  * 	Set prescaler to 32 (IWDG_PRESCALER_32)
  * 	Timeout Period = (Reload Counter Value * 32) / LsiFreq
  * 	So Set Reload Counter Value = (2 * LsiFreq) / 32 = 2500
  *
  * 2-- enable and configure the iwdg with CubeMX:
  * 	set IWDG counter clock prescaler to 32
  * 	set window value to 0xFFF, window function is disabled
  * 	set IWDG down counter reload value to the desired iwdg timeout (2500)
  *
  * 3-- refresh the iwdg every 1 second:
  *		if(HAL_IWDG_Refresh(hiwdg) != HAL_OK)
  *		{
  *		   // Refresh Error
  *		   Error_Handler();
  *		}
  */ 
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BOARD_H
#define __BOARD_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"


 /**
   *  @general defines
   */
typedef _Bool bool_t;

/**
  *  @defgroup flash Constants
  */
#define RONDOMAT_FLASH_DATA_START_ADDR   ADDR_FLASH_PAGE_63  					/* Start of Rondomat flash data */
#define FLASH_DATA_END_ADDR     		 ADDR_FLASH_PAGE_63 + FLASH_PAGE_SIZE	/* End of Flash data */


typedef enum {
  	LEDs = 2,				// Number of LEDs
  	DISPLAY_SEGs = 4,		// Number of 7 segment displays
  	BUTTONs = 4,			// Number of buttons
  	OUTPUTs = 2				// Number of output ports
  }Board_TypeDef;

  /* LED defines */
typedef enum
 {
   GREEN_LED,
   RED_LED
 } Led_TypeDef;

typedef enum
 {
   GREEN,
   YELLOW,
   RED,
   DARK
 } Led_ColorTypeDef;

 /* Output defines */
typedef enum
{
  FEED_MOTOR = 1,
  WATER_VALVE
} Output_TypeDef;

 /* Button defines */
 typedef enum
 {
   WATER_BUTTON_PLUS,
   WATER_BUTTON_MINUS,
   FEED_BUTTON_PLUS,
   FEED_BUTTON_MINUS
 } Button_TypeDef;

 /* ADC defines */
 typedef enum
 {
   ADC_TS,
   ADC_24V
 } Adc_TypeDef;

  typedef enum {
     BTN_IDLE            = 0x00U,             // Button not pressed
     BTN_PRESSED         = 0x01U,             // Button pressed
     BTN_SHORT           = 0x02U,             // Button was short pressed
     BTN_LONG            = 0x04U,             // Button was long pressed
     BTN_LONG_PRESSED    = 0x05U              // Button is still long pressed
 } Button_StatusTypeDef;

#endif /* __BOARD_H */
