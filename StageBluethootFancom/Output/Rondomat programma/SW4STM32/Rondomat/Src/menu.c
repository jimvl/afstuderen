/*
 * menu.c
 * @brief Rondomat menu functions
 *
 * @note
 * Copyright(C) PigTek Europe GmbH, 2017
 * All rights reserved.
 *
 *  Created on: 06.07.2017
 *      Author: fnordbeck
 */

#include "string.h"
#include <debug.h>
#include <board.h>
#include <iap.h>
#include <display.h>
#include <keyboard.h>
#include <menu.h>
#include <rondomat.h>
#include <debug.h>
volatile struct stMenuData stData;
volatile struct stMenu stMenu;

static uint8_t WaitForKeyRelease;

// Default menu data, the sequence MUST resemble the structure of stMenuData!
uint8_t const stDataDefaults[] = {
	0,   						// FLASH_FALID must be 0
	DEFAULT_WATER,
	DEFAULT_FEED,
	DEFAULT_PAUSE,
	DEFAULT_POWER,
	DEFAULT_SENSOR_SENSITIVETY,
	DEFAULT_BRIGHTNESS,
	DEFAULT_WORKING_VERSION,
	DEFAULT_RESET_COUNTS
};

void SaveDataToFlash(void){

	stData.FailedFlag = FLASH_FAILED;
	// Save the device data into flash memory
	IAP.WriteData2Flash(RONDOMAT_FLASH_DATA, (uint32_t)&stData, 0, sizeof(struct stMenuData), RONDOMAT_DATA_PAGES);

#ifdef DEBUG_FLASH_INFO
	DEBUG_PRINTF("Data flashed\r\n");
#endif

}

void InitMenu(void) {
	WaitForKeyRelease = 0;
	memset((uint8_t *) &stMenu, 0, sizeof(struct stMenu));
	stMenu.Menu = M_DEFAULT;

	/* Restore the rondomat data from the flash memory */
	if (IAP.CopyFlashData2ControlRegister((uint8_t*) &stData, (uint8_t*)(RONDOMAT_FLASH_DATA), sizeof(struct stMenuData), RONDOMAT_DATA_PAGES)){
		// CRC Error => predefine the rondomat data register with default values
		memcpy((struct stMenuData *)&stData, &stDataDefaults, sizeof(struct stMenuData));
	}else{
		if(stData.FailedFlag != FLASH_FAILED) {
			// Flash failed => predefine the rondomat data register with default values
			memcpy((struct stMenuData *)&stData, &stDataDefaults, sizeof(struct stMenuData));
		}
	}
	stData.ResetCounts++;
	// save the stData into flash
	stMenu.SaveTMO = 1;
}

void SetSensorLED(uint8_t state) {
	switch(stMenu.DuoLed) {
		case LED_DEFAULT:
		case LED_SENSOR:
			// Sensor is allowed to operate LED
			if(state) {
				// request LED
				Display.SetDuoLedtoColor(GREEN); //(YELLOW);
				stMenu.DuoLed = LED_SENSOR;
			} else {
				// release LED
				if(stMenu.DuoLed == LED_SENSOR) {
					Display.SetDuoLedtoColor(DARK);
					stMenu.DuoLed = LED_DEFAULT;
				}
			}
		break;
		default:
			// Sensor is not allowed to operate LED
		break;
	}
}

void SetMenuLED(uint8_t state) {
	switch(stMenu.DuoLed) {
		default:
			// Menu is allowed to operate LED
			if(state) {
				// request LED
				Display.SetDuoLedtoColor(RED);
				stMenu.DuoLed = LED_MENU;
			} else {
				// release LED
				if(stMenu.DuoLed == LED_MENU) {
					Display.SetDuoLedtoColor(DARK);
					stMenu.DuoLed = LED_DEFAULT;
				}
			}
		break;
	}
}

uint8_t CheckForMenuSwitch(const Button_StatusTypeDef up, const Button_StatusTypeDef down, eMenuID *Menu) {
	if(WaitForKeyRelease)
		return 0;
	if(up & BTN_SHORT) {
		(*Menu)++;
		if((*Menu) > MAX_MENUE)
			(*Menu) = MAX_MENUE;
		WaitForKeyRelease = 1;
		return 1;
	}
	if(down & BTN_SHORT) {
		(*Menu)--;
		WaitForKeyRelease = 1;
		if((*Menu) < MIN_MENUE)
			(*Menu) = MIN_MENUE;
		return 1;
	}
	return 0;
}

uint8_t CheckForProtectedMenuKey(const Button_StatusTypeDef KeyOne, const Button_StatusTypeDef KeyTwo) {
	static unsigned char KeyTimer = 0;
	if(WaitForKeyRelease)
		return 0;
	if((KeyOne & BTN_PRESSED) && (KeyTwo & BTN_PRESSED)) {
		if(KeyTimer >= PROTEC_MENU_TMO) {
			WaitForKeyRelease = 1;
			return 1;
		}
		KeyTimer++;
	} else {
		KeyTimer = 0;
	}
	return 0;
}

uint8_t CheckForOnOffKey(const Button_StatusTypeDef KeyOne, const Button_StatusTypeDef KeyTwo) {
	static uint8_t KeyTimer = 0;
	if(WaitForKeyRelease)
		return 0;
	if((KeyOne & BTN_PRESSED) && (KeyTwo & BTN_PRESSED)) {
		if(KeyTimer >= PROTEC_MENU_TMO) {
			WaitForKeyRelease = 1;
			return 1;
		}
		KeyTimer++;
	} else {
		KeyTimer = 0;
	}
	return 0;
}

uint8_t UpdateValue(const Button_StatusTypeDef up, const Button_StatusTypeDef down, int8_t *Value, int8_t Min, int8_t Max) {
	if(WaitForKeyRelease)
		return 0;
	// Up
	if(up & BTN_SHORT) {
		if(*Value < Max) {
			(*Value)++;
			return 1;
		}
	}
	// Down
	if(down & BTN_SHORT) {
		if(*Value > Min) {
			(*Value)--;
			return 1;
		}
	}
	// no changes
	return 0;
}

void PrintTwoDigitSigned(uint8_t *Buf, int8_t Number) {
	if((Number > 99) || (Number < -9)) {
		// More can not be displayed with 2 digits
		Buf[0] = SYM_MINUS;
		Buf[1] = SYM_MINUS;
	}
	if(Number < 0) {
		// negative value
		Buf[0] = SYM_MINUS;
		Buf[1] = (~Number) +1;
	} else {
		// positive value
		Buf[0] = Number / 10;
		Buf[1] = Number % 10;
	  if(Buf[0] == 0) {
		// Suppress leading zeros
		Buf[0] = SYM_BLANK;
		}
	}
	stMenu.DotPos = 0;
}

void PrintThreeDigitUnsigned(uint8_t *Buf, uint8_t Number) {

		Buf[0] = Number / 100;
		Buf[1] = (Number / 10) % 10;
		Buf[2] = Number % 10;
		stMenu.DotPos = 0;
}

void PrintFourDigitUnsigned(uint8_t *Buf, uint16_t Number) {

		Buf[0] = Number / 1000;
		Buf[1] = (Number / 100) % 10;
		Buf[2] = (Number / 10) % 10;
		Buf[3] = Number % 10;
		stMenu.DotPos = 0;
}

void PrintThreeDigitSigned(uint8_t *Buf, int8_t Number) {
	if((Number > 99) || (Number < -99)) {
		// More can not be displayed with 3 signed digits
		Buf[0] = SYM_MINUS;
		Buf[1] = SYM_MINUS;
		Buf[2] = SYM_MINUS;
		Buf[3] = SYM_MINUS;
	}
	if(Number < 0) {
		// negative value
		Buf[0] = SYM_MINUS;
		Buf[1] = (~Number) / 10;
		Buf[2] = (~Number) % 10 + 1;
	} else {
		// positive value
		Buf[0] = SYM_BLANK;
		Buf[1] = Number / 10;
		Buf[2] = Number % 10;
	}
	stMenu.DotPos = 0;
}

void PrintFourDigitSigned(uint8_t *Buf, int16_t Number) {
	if((Number > 999) || (Number < -999)) {
		// More can not be displayed with 4 signed digits
		Buf[0] = SYM_MINUS;
		Buf[1] = SYM_MINUS;
		Buf[2] = SYM_MINUS;
		Buf[3] = SYM_MINUS;
	}
	if(Number < 0) {
		// negative value
		Buf[0] = SYM_MINUS;
		Buf[1] = (~Number) / 100;
		Buf[2] = ((~Number) / 10) % 10;
		Buf[3] = (~Number) % 10 + 1;
	} else {
		// positive value
		Buf[0] = SYM_BLANK;
		Buf[1] = Number / 100;
		Buf[2] = (Number / 10) % 10;
		Buf[3] = Number % 10;
	}
	stMenu.DotPos = 0;
}

void MenuTimeouts(void){

	// Menu timeouts, is called every second
	if(stMenu.MenuTMO > 0)
		stMenu.MenuTMO--;
	else
		stMenu.Menu = M_DEFAULT;
	// handle save timeout
	if(stMenu.SaveTMO > 0) {
		if (stMenu.SaveTMO == 1) {
			SaveDataToFlash();
		}
		stMenu.SaveTMO--;
	}
}

void MenuHandler(Button_StatusTypeDef Lup, Button_StatusTypeDef Ldw, Button_StatusTypeDef Rup, Button_StatusTypeDef Rdw){
	// Handling of the menu, is called every 10ms
	static uint8_t CountingSeg = 0;
	static uint8_t Timer = 0;
	uint32_t resetCounts = 0;

	// Should the menus be entered or left?
	if(CheckForProtectedMenuKey(Lup, Ldw)) {
		if(stMenu.Menu == M_DEFAULT) {
			// the protected menus could be entered
			stMenu.Menu = M_PAUSE;
		} else {
			// the protected menus should be left
			stMenu.Menu = M_DEFAULT;
		}
	}
	// Is the Rondomat switched on or off?
	if(CheckForOnOffKey(Rup, Rdw)) {
		if(stData.PowerOn)
			// switch off the Rondomat
			stData.PowerOn = 0;
		else{
			// switch on the Rondomat and start the feeding
			stData.PowerOn = 1;
			TriggerFeeding = 1;
		}
		stMenu.SaveTMO = SAVE_TMO +1;
	}
	// Reload menu timeout
	if(Lup | Ldw | Rup | Rdw)
		stMenu.MenuTMO = MENU_TMO;
	// Check for key release
	if(WaitForKeyRelease) {
		if(!((Lup | Ldw | Rup | Rdw) & BTN_PRESSED)) {
			WaitForKeyRelease = 0;
		}
		// Delete any button states, all button states are invalid
		Keyboard.Init();
	}
	// If power off, then clear the display and do nothing
	if(!stData.PowerOn) {
	 	SetMenuLED(1);
		stMenu.Seg[0] = SYM_BLANK;
		stMenu.Seg[1] = 0;
		stMenu.Seg[2] = SYM_F;
		stMenu.Seg[3] = SYM_F;
		stMenu.DotPos = 0;
		Display.WriteTo7Seg((uint8_t *) &stMenu.Seg[0], stMenu.DotPos);
		stMenu.Menu = M_DEFAULT;
		return;
	}
	// Show the status
	SetMenuLED(stMenu.Menu != M_DEFAULT);
	// Switch to the desired menu
	switch(stMenu.Menu) {
		case M_DEFAULT:
			// Normal view, also 'Water and Feed'
			if(UpdateValue(Lup, Ldw, (int8_t *)&stData.Water, -3, 60))
				stMenu.SaveTMO = SAVE_TMO +1;
			if(UpdateValue(Rup, Rdw, (int8_t *)&stData.Feed, 1, 60))
				stMenu.SaveTMO = SAVE_TMO +1;
			// Manual switching of the outputs
			Rondomat.SetWaterManually(((Lup & BTN_LONG) && !(Ldw & BTN_PRESSED)));
			Rondomat.SetMotorAndWaterManually(((Rup & BTN_LONG) && !(Rdw & BTN_PRESSED)));
			// With this menu there is no timeout
			stMenu.MenuTMO = MENU_TMO;
			// Generate the values for the 7 segment digits
			PrintTwoDigitSigned((uint8_t *) &stMenu.Seg[0], stData.Water);
			PrintTwoDigitSigned((uint8_t *) &stMenu.Seg[2], stData.Feed);
		break;
		case M_PAUSE:
			// Menu-Pause
			CheckForMenuSwitch(Lup, Ldw, (uint8_t *) &stMenu.Menu);
			if(UpdateValue(Rup, Rdw, (int8_t *)&stData.Pause, 1, 99))
				stMenu.SaveTMO = SAVE_TMO +1;
			// Generate the values for the 7 segment digits
			stMenu.Seg[0] = M_PAUSE;
			stMenu.Seg[1] = SYM_BLANK;
			PrintTwoDigitSigned((uint8_t *) &stMenu.Seg[2], stData.Pause);
		break;
		case M_SENSOR_SENSITIVETY:
			// Menu-Sensor sensitivity
			CheckForMenuSwitch(Lup, Ldw, (uint8_t *) &stMenu.Menu);
			if(UpdateValue(Rup, Rdw, (int8_t *)&stData.SensorSensitivety, 1, 30))
				stMenu.SaveTMO = SAVE_TMO +1;
			// Generate the values for the 7 segment digits
			stMenu.Seg[0] = M_SENSOR_SENSITIVETY;
			stMenu.Seg[1] = SYM_BLANK;
			PrintTwoDigitSigned((uint8_t *) &stMenu.Seg[2], stData.SensorSensitivety);
		break;
		case M_RESET_DEF:
			// Menu-Reset the Rondomat data to default values
			CheckForMenuSwitch(Lup, Ldw, (uint8_t *) &stMenu.Menu);
			if (stMenu.Menu != M_RESET_DEF) CountingSeg = 0;
			if(Rup & BTN_LONG) {
				// set all to default values
				resetCounts = stData.ResetCounts;
				memcpy((struct stMenuData *)&stData, &stDataDefaults, sizeof(struct stMenuData));
				stData.ResetCounts = resetCounts;
				stMenu.SaveTMO = SAVE_TMO +1;
				CountingSeg = 1;
				stMenu.Seg[0] = 0;
				stMenu.Seg[1] = 0;
				stMenu.Seg[2] = 0;
				stMenu.Seg[3] = 0;
			}
			// Generate the values for the 7 segment digits
			if(CountingSeg) {
				Timer++;
				if(Timer > 100) {
					Timer = 0;
					// every second (if calling the function every 10ms)
					stMenu.Seg[0]++;
					stMenu.Seg[1]++;
					stMenu.Seg[2]++;
					stMenu.Seg[3]++;
					if(stMenu.Seg[3] > 9) {
						stMenu.Seg[0] = 0;
						stMenu.Seg[1] = 0;
						stMenu.Seg[2] = 0;
						stMenu.Seg[3] = 0;
					}
				}
			} else {
				stMenu.Seg[0] = M_RESET_DEF;
				stMenu.Seg[1] = SYM_BLANK;
				stMenu.Seg[2] = SYM_BLANK;
				stMenu.Seg[3] = SYM_MINUS;
			}
			stMenu.DotPos = 0;
		break;
		case M_VERSION:
			// Menu-Show the program version
			CheckForMenuSwitch(Lup, Ldw, (uint8_t *) &stMenu.Menu);
			// Generate the values for the 7 segment digits
			stMenu.Seg[0] = M_VERSION;
			stMenu.Seg[1] = SYM_BLANK;
			stMenu.Seg[2] = ProgVersion[0];
			stMenu.Seg[3] = ProgVersion[1];
			stMenu.DotPos = 3;
		break;
		case M_WORKING_VERSION:
			// Menu-Working version
			CheckForMenuSwitch(Lup, Ldw, (uint8_t *) &stMenu.Menu);
			if(UpdateValue(Rup, Rdw, (int8_t *)&stData.WorkingVersion, 6, 7))
				stMenu.SaveTMO = SAVE_TMO +1;
			// Generate the values for the 7 segment digits
			stMenu.Seg[0] = M_WORKING_VERSION;
			stMenu.Seg[1] = SYM_BLANK;
			PrintTwoDigitSigned((uint8_t *) &stMenu.Seg[2], stData.WorkingVersion);
		break;


		case M_DISP_BRIGHTNESS:
			// Menu-7 segment brightness
			CheckForMenuSwitch(Lup, Ldw, (uint8_t *) &stMenu.Menu);
			if(UpdateValue(Rup, Rdw, (int8_t *)&stData.Brightness, 1, 10))
				stMenu.SaveTMO = SAVE_TMO +1;
			// Generate the values for the 7 segment digits
			stMenu.Seg[0] = M_DISP_BRIGHTNESS;
			stMenu.Seg[1] = SYM_BLANK;
			PrintTwoDigitSigned((uint8_t *) &stMenu.Seg[2], stData.Brightness);
		break;



		case M_S_VALUE:
			// Menu-Show the sensor Value
			CheckForMenuSwitch(Lup, Ldw, (uint8_t *) &stMenu.Menu);
			// on this menu we haven't an timeout
			stMenu.MenuTMO = MENU_TMO;
			// Generate the values for the 7 segment digits
			stMenu.Seg[0] = SYM_BLANK;
			//PrintThreeDigitUnsigned(&stMenu.Seg[1], FilteredTroughValue);
			PrintFourDigitUnsigned((uint8_t *) &stMenu.Seg[0], FilteredTroughValue);
		break;
		case M_S_GRADIENT:
			// Menu-Show the sensor gradient
			CheckForMenuSwitch(Lup, Ldw, (uint8_t *) &stMenu.Menu);
			// on this menu we haven't an timeout
			stMenu.MenuTMO = MENU_TMO;
			// Generate the values for the 7 segment digits
			stMenu.Seg[0] = SYM_BLANK;
			//PrintThreeDigitSigned(&stMenu.Seg[1], GradientTroughValue);
			PrintFourDigitSigned((uint8_t *) &stMenu.Seg[0], GradientTroughValue);
		break;
		default:
			// unknown menu
			WaitForKeyRelease = 1;
			stMenu.Menu = M_DEFAULT;
		break;
	}
	// show the current value on the display
	Display.WriteTo7Seg((uint8_t *) &stMenu.Seg[0], stMenu.DotPos);
}

const struct MenuApi Menu = {
		InitMenu,
		MenuTimeouts,
		MenuHandler,
		SetSensorLED
};
