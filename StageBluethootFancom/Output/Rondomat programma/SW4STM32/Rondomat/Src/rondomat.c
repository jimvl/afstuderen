/*
 * rondomat.c
 * @brief rondomat functions
 *
 * @note
 * Copyright(C) PigTek Europe GmbH, 2017
 * All rights reserved.
 *
 *  Created on: 07.07.2017
 *      Author: fnordbeck
 */

#include "main.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "board.h"
#include "iof.h"
#include "adc.h"
#include "menu.h"
#include "rondomat.h"
#include "debug.h"

volatile uint8_t FeedSensActivityTimer = 0;
volatile int8_t GradientTroughValue;
volatile uint16_t FilteredTroughValue;
volatile uint16_t TroughValueBefore = 0;
volatile uint8_t TroughMeasureState = STOP;
volatile uint8_t TroughFullLevel = DEFAULT_TROUGH_LEVEL;
volatile bool_t TriggerFeeding = 0;

// Static variables (private)
static bool_t BothManually = 0;
static bool_t WaterManually = 0;
static bool_t MotorAuto = 0;
static bool_t WaterAuto = 0;
static bool_t TroughEmpty = 0;
static uint8_t FeedingState = 'W';
static uint8_t ADCState = 0;

static eRondo State = WAIT_TROUGH_EMPTY;

// Debug variables
static uint8_t Hours = 0;
static uint8_t Minutes = 0;
static uint8_t Seconds = 0;
static uint32_t MotorcountsPerDay = 0;
static bool_t debugPrint = 0;

//Communication variables aanpassingen Martijn
extern UART_HandleTypeDef huart1;
#define RXBUFFERSIZE 1
uint8_t receive[RXBUFFERSIZE + 1];
static uint8_t opvang = 0;


// switch ON the motor and the water valve manually
void SetMotorAndWaterManually(bool_t state) {
	BothManually = state;
}

// switch ON the motor manually
void SetWaterManually(bool_t state) {
	WaterManually = state;
}
void Bluetooth(void){ //RX receive, martijn
	HAL_UART_Receive(&huart1, receive, RXBUFFERSIZE , 0xFFFF); //Zorgt voor de RX ontvangen, martijn
	if (receive[0] == 'X'){
		opvang = 1;
	}
}

// Control the outputs, is called every 100ms
void ControlOutputs(void) {
	if(stData.PowerOn) {
			// Rondomat is switched ON
		if(WaterManually || BothManually || WaterAuto) {
				// turn ON the water valve
			IO.SetOutput(WATER_VALVE, RONDO_IOPORT_TMO);
		} else {
				// turn OFF the water valve
			IO.SetOutput(WATER_VALVE, 0);
		}
		if(BothManually || MotorAuto || opvang == 1) { //opvang, martijn
				// turn ON the feed motor
			IO.SetOutput(FEED_MOTOR, RONDO_IOPORT_TMO);
			receive[0] = 0; //Reset van ontvangen waardes, martijn
			opvang = 0; //Reset van tussen variabele, martijn
		} else {
			// turn OFF the feed motor
			IO.SetOutput(FEED_MOTOR, 0);
		}

#if 1
		if (stData.WorkingVersion == 7){
			if(FeedSensActivityTimer){
				Menu.SetSensorLed(FEED_SENS_ACTIVE);
				FeedSensActivityTimer--;
			}
			else Menu.SetSensorLed(FEED_SENS_NOT_ACTIVE);
		}
#endif

#if 0
		if (stData.WorkingVersion == 7){
			if(FeedingState == 'W') Menu.SetSensorLed(TROUGH_FULL);
			else Menu.SetSensorLed(TROUGH_EMPTY);
		}
#endif

		if (stData.WorkingVersion == 6){
			Menu.SetSensorLed(!TroughEmpty);
		}
#ifdef DEBUG_ENABLE
		if (debugPrint){
			DEBUG_PRINTF("%i,%i,%i,%i,%i,", (int)Hours, (int)Minutes, (int)Seconds, (int)stData.WorkingVersion, (int)MotorcountsPerDay);
			DEBUG_PRINTF("%i,%c,%i,%i,%i,", (int)Adc.Get24VPowerValue(), (char)FeedingState, (int)FilteredTroughValue, (int)GradientTroughValue, (int)TroughFullLevel);
			DEBUG_PRINTF("%i,%i,%i,%i\r\n", (int)stData.Water, (int)stData.Feed, (int)stData.Pause, (int)stData.SensorSensitivety);
			debugPrint = 0;
		}
#endif
	} else {
			// Rondomat is switched OFF
		IO.SetOutput(FEED_MOTOR, 0);
		IO.SetOutput(WATER_VALVE, 0);
		BothManually = 0;
		MotorAuto = 0;
		WaterManually = 0;
		WaterAuto = 0;
		TroughEmpty = 0;
	}
}

void ReadTroghSensor(void) {
	//static uint8_t ADCState = 0;
	static uint16_t SumTrogValue = 0;
	volatile uint16_t adcValue;

	/* Reads the trough sensor value, is called every 10ms, get a sample every 10ms.
	 * Switch on the trough sensor and wait 10ms for a stable state for the ADC, min. 10ms, until all capacitors have charged up.
	 * When the number of samples is reached, ADC_SAMPLES = 4, then calculate the mean of the samples and turn off the sensor.
	 * Wait 2000ms. Then turn on the sensor again, wait 10ms and add up the samples again.
	 * Every 250ms a valid trough value is present.
	 */

	ADCState++;
	if(ADCState == 1) {
			// switch on the trough sensor
		Adc.SwitchOnTroughSensor();
	}

	if(ADCState > 1 && ADCState <= ADC_SAMPLES+1) {
			// add the sample
		adcValue = Adc.GetTroughValue();

		if (adcValue != 0xffff){
			SumTrogValue += adcValue;
		}
	}

	if(ADCState == ADC_SAMPLES+1) {
			// all samples are valid
		FilteredTroughValue = SumTrogValue / ADC_SAMPLES;
		GradientTroughValue = FilteredTroughValue - TroughValueBefore;
		TroughValueBefore = FilteredTroughValue;
		SumTrogValue = 0;

		if (abs(GradientTroughValue) > stData.SensorSensitivety){
				// start feeding
			TriggerFeeding = 1;
		}
			// only used for the LED to show the feed sensor activity
			// is TriggerFeeding == TRUE then switch ON the LED as long as the motor and water valve is ON
			// otherwise let the LED blinking if the gradient trough value is > 1 or < -1
		if(TriggerFeeding) FeedSensActivityTimer = 5;
		else{
			if (abs(GradientTroughValue) > 1) FeedSensActivityTimer = 1;
		}

		switch (TroughMeasureState){
			case START:
					// if the trough-level < DEFAULT_TROUGH_EMPTY_LEVEL, then initiate a new measure cycle
					// if feed is in the trough, then the trough-level is approximately: 48 to 64 @ 1.5V, @ 3V I don't know
				if (FilteredTroughValue < DEFAULT_TROUGH_EMPTY_LEVEL){
					TroughMeasureState = DETECT_ZERO;
				}
			break;
			case DETECT_ZERO:
					// if in the valid range, then set the full-level
				if (GradientTroughValue > -2 && GradientTroughValue < 2 ){ // Zero range -1 to 1
					TroughFullLevel = FilteredTroughValue;
					TroughMeasureState = MONITOR_TROUGH_LEVEL;
				}
			break;
			case MONITOR_TROUGH_LEVEL:
					// if the trough level drops, then re-occupy the full-level
				if (FilteredTroughValue < TroughFullLevel - 1 ){
					TroughFullLevel = FilteredTroughValue;
				}
			break;
			case STOP:
			default:
			break;
		}

			// switch off the trough sensor
		Adc.SwitchOffTroughSensor();

			// set debugPrint for print out the measured values
		debugPrint = 1;
	}

	if(ADCState >= ADC_SAMPLES+20) {
	//if(ADCState >= ADC_SAMPLES+15) {
		// the measurement cycle is finished. Start again the next measurement cycle.
		ADCState = 0;
	}
}

/* Rondomat feeding function
 * must called every second
 */
void RondoFunc(void) {
	static unsigned char Timer = 0;

		// only for debug
	if(++Seconds >= 60) {Minutes++; Seconds = 0;}
	if(Minutes >= 60) {Hours++; Minutes = 0;}
	if(Hours >= 24) {Hours = 0; MotorcountsPerDay = 0;}


	if(stData.PowerOn) {
		// Rondomat is switched ON

		MotorAuto = 0;
		WaterAuto = 0;

		TroughEmpty = FilteredTroughValue > (TroughFullLevel + stData.SensorSensitivety);

		switch(State) {
			case WAIT_TROUGH_EMPTY:
				FeedingState = 'W';

				if (stData.WorkingVersion == 7){
					if(TriggerFeeding) {
						TroughMeasureState = STOP;	// Stop the trough measurement
						Timer = stData.Pause;
						State = WAIT_PAUSE;
					}
				}

				if (stData.WorkingVersion == 6){
					if (TroughEmpty){
						TroughMeasureState = STOP;	// Stop the trough measurement
						if (stData.Pause >= Timer){	// Wait for the pause-time minus the time that has passed during the trough measurement
							Timer = stData.Pause - Timer;
							State = WAIT_PAUSE;
						}else{
							Timer = 0;
							State = FEEDING;
						}
					}
				}
			break;
			case WAIT_PAUSE:
				FeedingState = 'P';
				if(Timer) Timer--;
				else{
					State = FEEDING;
					Timer = 0;
				}
			break;
			case FEEDING:
				FeedingState = 'F';
				Timer++;
				if(Timer <= (stData.Feed + MIN_DOSING_TIME + stData.Water)) WaterAuto = 1;
				if(Timer <= (stData.Feed + MIN_DOSING_TIME)) MotorAuto = 1;
				if(!WaterAuto && !MotorAuto) {
					// Motor and water valve are switched OFF
					TriggerFeeding = 0;
					Timer = 0;
					TroughMeasureState = START;	// Start the trough measure after feeding is finished
					State = MEASURE_TROUGH_VALUE;

					// only for debug
					MotorcountsPerDay++;

				}
			break;
			case MEASURE_TROUGH_VALUE: // Wait of trough measurement is finished
				FeedingState = 'M';
				if(++Timer > TROUGH_MESURE_TIME || TroughMeasureState == MONITOR_TROUGH_LEVEL){
					State = WAIT_TROUGH_EMPTY;
				}
			break;
			default:
				FeedingState = 'D';
				State = WAIT_TROUGH_EMPTY;
			break;
		}
	}else{
		// Rondomat is switched OFF
		State = WAIT_TROUGH_EMPTY;
		Timer = 0;
		FeedingState = 'O';
	}
}

const struct RondomatApi Rondomat = {
		SetMotorAndWaterManually,
		SetWaterManually,
		ControlOutputs,
		ReadTroghSensor,
		RondoFunc,
		Bluetooth //Extra functie voor RX, martijn
};
