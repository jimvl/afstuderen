/*
 * @file    debug.c
 *
 * @brief   debug functions
 *
 * @note
 *  Copyright(C) PigTek Europe GmbH, 2018
 *  All rights reserved.
 *
 *  Created on: 11.06.2018
 *      Author: fnordbeck
 */
#include "board.h"
#include "stm32f0xx_hal.h"
#include "debug.h"

extern UART_HandleTypeDef huart1;

int __io_putchar(int     ch)
{
 uint8_t c[1];
 c[0] = ch & 0x00FF;
 HAL_UART_Transmit(&huart1, (uint8_t *) &c[0], 1, 10);

 return ch;
}   

int _write(int file, char *ptr, int len)
{
 int DataIdx;
 for(DataIdx = 0; DataIdx < len; DataIdx++)
 {
 __io_putchar(*ptr++);
 }
return len;
}
