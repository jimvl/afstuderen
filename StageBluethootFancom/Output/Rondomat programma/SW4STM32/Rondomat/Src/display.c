/*
 * display.c
  * @brief Display functions
 *
 * @note
 * Copyright(C) PigTek Europe GmbH, 2017
 * All rights reserved.
 *
 *  Created on: 06.07.2017
 *      Author: fnordbeck
 */

#include "board.h"
//#include <debug.h>
#include <spi.h>
#include <display.h>


// 7 Segments
#define 	SGA						0x01
#define 	SGB						0x02
#define 	SGC						0x04
#define 	SGD						0x08
#define 	SGE						0x10
#define 	SGF						0x20
#define 	SGG						0x40
#define 	SGP						0x80

// Segment position
//		    a
//         ---
//       f| g |b
//         ---
//       e|   |c
//         ---
//          d   ° p
//

const unsigned char CodeTable[] = {
	(unsigned char) ~(SGA | SGB | SGC | SGD | SGE | SGF),		// 0
	(unsigned char) ~(SGB | SGC),								// 1
	(unsigned char) ~(SGA | SGB | SGG | SGE | SGD),				// 2
	(unsigned char) ~(SGA | SGB | SGC | SGD | SGG),				// 3
	(unsigned char) ~(SGB | SGC | SGG | SGF),					// 4
	(unsigned char) ~(SGA | SGF | SGG | SGC | SGD),				// 5
	(unsigned char) ~(SGA | SGC | SGD | SGE | SGF | SGG),		// 6
	(unsigned char) ~(SGA | SGB | SGC),							// 7
	(unsigned char) ~(SGA | SGB | SGC | SGD | SGE | SGF | SGG),	// 8
	(unsigned char) ~(SGA | SGB | SGC | SGD | SGF | SGG),		// 9
	(unsigned char) ~(0),										// blank
	(unsigned char) ~(SGG),										// -
	(unsigned char) ~(SGP),										// decimal point
	(unsigned char) ~(SGA | SGF | SGG | SGE),					// F
};

volatile struct {
	uint8_t Segment[DISPLAY_SEGs];	// Currently value of the display
	uint8_t OnTime;					// Display ON time, used for brightness
	uint8_t OffTime;				// Display OFF time, used for brightness
} stDisplay;


/* Set duo LED  */
void SetDuoLEDtoColor(uint8_t color)
{
	switch(color) {
		case GREEN:
			HAL_GPIO_WritePin(GPIOB, LED_GREEN_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOB, LED_RED_Pin, GPIO_PIN_SET);
		break;
		case YELLOW:
			HAL_GPIO_WritePin(GPIOB, LED_GREEN_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOB, LED_RED_Pin, GPIO_PIN_RESET);
		break;
		case RED:
			HAL_GPIO_WritePin(GPIOB, LED_GREEN_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(GPIOB, LED_RED_Pin, GPIO_PIN_RESET);
		break;
		case DARK:
		default:
			HAL_GPIO_WritePin(GPIOB, LED_GREEN_Pin|LED_RED_Pin, GPIO_PIN_SET);
		break;
	}
}

/* Toggle the duo LED */
void ToggleDuoLed(uint8_t color)
{
	switch(color) {
		case GREEN:
			HAL_GPIO_TogglePin(GPIOB, LED_GREEN_Pin);
		break;
		case YELLOW:
			HAL_GPIO_TogglePin(GPIOB, LED_GREEN_Pin);
			HAL_GPIO_TogglePin(GPIOB, LED_RED_Pin);
		break;
		case RED:
			HAL_GPIO_TogglePin(GPIOB, LED_RED_Pin);
		break;
		default:
			// turn OFF the LEDs
			HAL_GPIO_WritePin(GPIOB, LED_GREEN_Pin|LED_RED_Pin, GPIO_PIN_SET);
		break;
	}
}

void InitDisplay (void){
	for(uint8_t i=0; i<DISPLAY_SEGs; i++){
		stDisplay.Segment[i] = CodeTable[SYM_BLANK];
	}
	SetDuoLEDtoColor(DARK);
}

/**
 * @brief	set the brightness and send data to the 7 segment displays
 * @param	brightness: brightness of the 7 segment display in steps from 1 to 10
 * @return	None
 */
void Brightness(uint8_t brightness){
	static bool_t OnOffFlag = 0;	// display ON / OFF flag
	static uint8_t timer = 0;		// Timer for brightness
	const uint8_t OffData[4] = {CodeTable[SYM_BLANK], CodeTable[SYM_BLANK], CodeTable[SYM_BLANK], CodeTable[SYM_BLANK]};

	if (brightness <= 10){
		stDisplay.OnTime = brightness;
		stDisplay.OffTime = 10 - brightness;
	}else{
		stDisplay.OnTime = 5;
		stDisplay.OffTime = 5;
	}

#if 1
	if (timer) timer--;
	else{
		if (OnOffFlag){
			timer = stDisplay.OnTime;
			OnOffFlag = 0;
			//Send data to display
			SPI.WriteData((uint8_t *) &stDisplay, 4);
		}else{
			timer = stDisplay.OffTime;
			OnOffFlag = 1;
			//Send Off-data to display
			SPI.WriteData((uint8_t *) &OffData, 4);
		}
	}
#else
	SPI.WriteData((uint8_t *) &stDisplay, 4);
#endif
}

void WriteDisplayData (uint8_t *value, uint8_t dotPos) {
	uint8_t j=3;

	for(uint8_t i=0; i<DISPLAY_SEGs; i++) {
		if(value[i] >= sizeof(CodeTable))
			stDisplay.Segment[i] = CodeTable[SYM_BLANK];
		else{
			if (i == dotPos-1){
				stDisplay.Segment[i] = CodeTable[value[j]] & CodeTable[SYM_DOT];
			}else{
				stDisplay.Segment[i] = CodeTable[value[j]];
			}
		}
		j--;
	}
}

const struct DisplayApi Display = {
		SetDuoLEDtoColor,
		ToggleDuoLed,
		InitDisplay,
		WriteDisplayData,
		Brightness
};
