/*
 * keyboard.c
 *
 * @brief keyboard functions
 *
 * @note
 * Copyright(C) PigTek Europe GmbH, 2017
 * All rights reserved.
 *
 *  Created on: 04.07.2017
 *      Author: fnordbeck
 */

#include "string.h"
#include "board.h"
#include <keyboard.h>

volatile uint8_t BTNTimer[BUTTONs];
volatile Button_StatusTypeDef BTNFlags[BUTTONs];


void InitKeyBoard(void) {
	memset((Button_StatusTypeDef *) &BTNFlags, BTN_IDLE, sizeof(Button_StatusTypeDef) * BUTTONs);
	memset((uint8_t *) &BTNTimer, 0, sizeof(uint8_t) * BUTTONs);
}

Button_StatusTypeDef GetStatus(Button_TypeDef ID) {

	Button_StatusTypeDef ReturnVal = BTNFlags[ID];

	if(BTNTimer[ID] >= 255) {
		// The button is no longer active, can be deleted
		BTNFlags[ID] = BTN_IDLE;
		BTNTimer[ID] = 0;
	}
	return ReturnVal;
}

void Analyze_Key(GPIO_PinState pinState, Button_TypeDef ID) {

		if(BTNTimer[ID] < 255) {
		// Button is not locked, also update the status
		if(pinState == PRESSED) {
			// Button is pressed
			BTNFlags[ID] |= BTN_PRESSED;
			if(BTNTimer[ID] > TIME_LONG_PRESS) {
				BTNFlags[ID] |= BTN_LONG;
			} else {
				BTNTimer[ID]++;
			}
		} else {
			// Button is not pressed
			BTNFlags[ID] &= ~BTN_PRESSED;
			if(BTNTimer[ID] > TIME_LONG_PRESS) {
				BTNFlags[ID] |= BTN_LONG;
				BTNTimer[ID] = 255;		// Button is locked, until status has been read
			} else if(BTNTimer[ID] > TIME_SHORT_PRESS) {
				BTNFlags[ID] |= BTN_SHORT;
				BTNTimer[ID] = 255;		//  Button is locked, until status has been read
			}
		}
	}
}

const struct KeyboardApi Keyboard = {
		InitKeyBoard,
		GetStatus,
		Analyze_Key
};
