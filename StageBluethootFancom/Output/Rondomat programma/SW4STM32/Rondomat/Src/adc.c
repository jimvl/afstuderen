/*
 * @file    adc.c
 *
 * @brief   adc functions
 *
 * @note
 *  Copyright(C) PigTek Europe GmbH, 2018
 *  All rights reserved.
 *
 *  Created on: 31.05.2018
 *      Author: fnordbeck
 */
#include "board.h"
#include "stm32f0xx_hal.h"
#include "adc.h"


/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/
#define ADC_CHANNELS	2   // Number of ADC channels
#define MAX_SAMPLES 	40	// 2 50Hz periods, used for 24V power measure

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
	/* Variable containing ADC conversions data */
static uint16_t aADC_Buffer[ADC_CHANNELS];
bool_t adcConvCplt = 0;

/* public variables ---------------------------------------------------------*/
extern ADC_HandleTypeDef hadc;
//volatile uint16_t aPwrSamples[MAX_SAMPLES]; // array for the 24V samples
volatile uint16_t powerValue = 0;

/* private functions ---------------------------------------------------------*/

// ADC conversion complete callback function
void HAL_ADC_ConvHalfCpltCallback(ADC_HandleTypeDef *hadc)
{
	adcConvCplt = 1;
}

/**
  * @brief  sample the raw adc values, must called cyclic
  * @param  none
  * @retval none
  */
#define START_ADC 0
#define WAIT 1
#define CONTINUOUSLY 1
void adcSample (void){
	static uint8_t i = 0;
	static uint8_t state = 0;
	static uint16_t valueBefore = 0;
	static int16_t diffValue = 0;
	static uint32_t sum = 0;

	switch (state){
		case START_ADC:
			HAL_ADC_Start_DMA(&hadc, (uint32_t *) aADC_Buffer, ADC_CHANNELS);
			state = CONTINUOUSLY;
		break;

		case CONTINUOUSLY:
			if (adcConvCplt) {
					// if conversion complete then sample the 24V power value
				HAL_ADC_Stop_DMA((ADC_HandleTypeDef *) &hadc);
				state = CONTINUOUSLY;
				adcConvCplt = 0;
					// eliminate the offset voltage
				diffValue = aADC_Buffer[ADC_24V] - valueBefore;
				if(diffValue < 0) diffValue *= -1;
				valueBefore = aADC_Buffer[ADC_24V];
					// build the average value over two 50Hz periods
				if (i >= MAX_SAMPLES){
					powerValue = sum / MAX_SAMPLES;
					sum = 0;
					i = 0;
				}
					// add the sample
				sum += diffValue;
				i++;
					// start the adc again
				HAL_ADC_Start_DMA(&hadc, (uint32_t *) aADC_Buffer, ADC_CHANNELS);
			}
		break;

		default:
			state = START_ADC;
		break;
	}


#if 0
	switch (state){
		case START_ADC:
			HAL_ADC_Start_DMA(&hadc, (uint32_t *) aADC_Buffer, ADC_CHANNELS);
			state = WAIT;
		break;

		case WAIT:
			if (adcConvCplt) {
					// if conversion complete then sample the 24V power values
				HAL_ADC_Stop_DMA((ADC_HandleTypeDef *) &hadc);
				state = START_ADC;
				adcConvCplt = 0;
				if (i >= MAX_SAMPLES) i = 0;
				a24V_Samples[i] = aADC_Buffer[ADC_24V];
				i++;
			}
		break;

		default:
			state = START_ADC;
		break;
	}
#endif
}

/* public functions ---------------------------------------------------------*/

/**
  * @brief  switch on the trough sensor
  * @param  none
  * @retval none
  */
void adcSwitchOnTroughSensor(void){

	 HAL_GPIO_WritePin(ADC_TS_OUT_GPIO_Port, ADC_TS_OUT_Pin, GPIO_PIN_SET);
}

/**
  * @brief  switch of the trough sensor
  * @param  none
  * @retval none
  */
void adcSwitchOffTroughSensor(void){

	 HAL_GPIO_WritePin(ADC_TS_OUT_GPIO_Port, ADC_TS_OUT_Pin, GPIO_PIN_RESET);
}

/**
  * @brief  get the raw adc trough value
  * @param  channel
  * @retval raw adc value or on error with 0xffff
  */
uint16_t adcGetTroughValue (){

	// reduce the ADC value to desired ADC bits, defined in adc.h
	return (aADC_Buffer[ADC_TS] >> ADC_RESOLUTION);
}

/**
  * @brief  get the ptr of the raw adc 24V array
  * @param  none
  * @retval ptr to raw adc value array
  */
uint16_t adcGetPwrValue (void){

	return powerValue;
}

/**
  * @brief  Calibrate the adc
  * @param  none
  * @retval HAL status
  */
HAL_StatusTypeDef adcCalibrate(void){

		/* start adc calibration */
   if (HAL_ADCEx_Calibration_Start(&hadc) != HAL_OK)
   {
	   return HAL_ERROR;
   }
   return HAL_OK;
}

/**
  * @brief  Start the adc in DMA mode
  * @param  none
  * @retval HAL status
  */
HAL_StatusTypeDef adcStart(void){
	   /* start conversion in DMA mode */
   if (HAL_ADC_Start_DMA(&hadc, (uint32_t *) aADC_Buffer, ADC_CHANNELS) != HAL_OK)
   {
	   return HAL_ERROR;
   }
   return HAL_OK;
}

/**
  * @brief  Stop the adc conversion
  * @param  none
  * @retval HAL status
  */
HAL_StatusTypeDef adcStop(void){
	/* start conversion */
	if(HAL_ADC_Stop_DMA(&hadc) != HAL_OK)
	{
		return HAL_ERROR;
	}
	return HAL_OK;
}


/* ADC API ------------------------------------------------------------------*/
const struct adcApi Adc = {
	adcCalibrate,
	adcStart,
	adcStop,
	adcSwitchOnTroughSensor,
	adcSwitchOffTroughSensor,
	adcGetTroughValue,
	adcGetPwrValue,
	adcSample
};
