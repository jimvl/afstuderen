/*
 * @file    spi.c
 *
 * @brief   spi function for the 7 segment displays based on
 *          polling transfer.
 *
 * @note
 *  Copyright(C) PigTek Europe GmbH, 2018
 *  All rights reserved.
 *
 *  Created on: 24.04.2018
 *      Author: fnordbeck
 */

#include "string.h"
#include "stm32f0xx_hal.h"
#include "spi.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* public functions ---------------------------------------------------------*/


/**
  * @brief  SPI write an amount of data to device
  * @param  ptr to data to be written
  * @param  dataLength number of bytes to write
  * @retval HAL status
  */
HAL_StatusTypeDef spi1WriteData(uint8_t *dataIn, uint16_t dataLength){
  extern SPI_HandleTypeDef hspi1;
  HAL_StatusTypeDef halStatus;

  // transmit the message for the 7 segment displays
  halStatus = HAL_SPI_Transmit((SPI_HandleTypeDef *) &hspi1, dataIn, dataLength, SPI1_TIMEOUT);
  // clock the latch to take over the shifted data
  HAL_GPIO_WritePin(SPI1_NSS_GPIO_Port, SPI1_NSS_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(SPI1_NSS_GPIO_Port, SPI1_NSS_Pin, GPIO_PIN_SET);
  return halStatus;
}


/* SPI API ------------------------------------------------------------------*/
const struct spiApi SPI = {
	spi1WriteData
};
