/*
 * iof.c
 *
 ** @brief input output functions
 *
 * @note
 * Copyright(C) PigTek Europe GmbH, 2017
 * All rights reserved.
 *
 *  Created on: 06.07.2017
 *      Author: fnordbeck
 */

#include "string.h"
#include "board.h"
#include "iof.h"

// private
static Output_CtrDataTypeDef OutputCtrReg[OUTPUTs];


// public
void InitOutput(void){

	// delete the output control register
	memset((Output_CtrDataTypeDef *)&OutputCtrReg, 0, sizeof(Output_CtrDataTypeDef) * OUTPUTs);
}

void SetOutput(uint8_t ID, uint32_t time){

	if(ID && ID <= OUTPUTs){
		// valid output ID
		OutputCtrReg[ID-1].Time = time;
	}
}

Output_StatusTypeDef AnalyzeOutput(Output_TypeDef ID){

	if(ID && ID <= (Output_TypeDef) OUTPUTs){
		if(OutputCtrReg[ID-1].Time){
			// Output is active
			OutputCtrReg[ID-1].Time--;
			return ON;
		}else{
			// Output is inactive
			return OFF;
		}
	}
	return OFF;
}

const struct IOApi IO = {
		InitOutput,
		SetOutput,
		AnalyzeOutput
};
