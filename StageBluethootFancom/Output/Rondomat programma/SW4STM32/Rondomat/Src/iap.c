/**
  ******************************************************************************
  * @file    iap.c
  * @author  MCD Application Team
  * @brief   Erase and program functions for the STM32F0xx FLASH.
  * @note
  * 		Copyright(C) PigTek Europe GmbH, 2018
  * 		All rights reserved.
  *
  *  		Created on: 06.02.2018
  *      	Author: fnordbeck
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "iap.h"
#include "crc16.h"
#include "string.h"
//#include "board.h"

extern uint16_t CRC16LoopTable(volatile uint8_t* ptr, uint8_t len);

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

__attribute ((aligned(32))) union PageData Flash;

/*Variable used for Erase procedure*/
static FLASH_EraseInitTypeDef EraseInitStruct;

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Prog_Flash: program one or more complete flash pages.
  *
  * @param  pageAddr: address of the first flash page
  *      	sourceAddr: address of the memory data which should be programmed into the flash
  *      	pages: number of pages
  *
  * @retval IAP_STATUS
  */
IAP_StatusTypeDef Prog_Flash(uint32_t pageAddr, uint32_t sourceAddr, uint32_t pages)
{
  uint32_t memData32 = 0, flashData32 = 0 ,programStatus = 0;
  uint32_t pageError = 0, flashAddress, sourceAddress, flashSize;

  /* Unlock the flash to enable the flash control register access */
  HAL_FLASH_Unlock();

  /* Erase the flash page(s)
    (area defined by pageAddr and pages) */

  /* Fill EraseInit structure*/
  EraseInitStruct.TypeErase = FLASH_TYPEERASE_PAGES;
  EraseInitStruct.PageAddress = pageAddr;
  EraseInitStruct.NbPages = pages;

  if (HAL_FLASHEx_Erase(&EraseInitStruct, &pageError) != HAL_OK)
  {
    /*Error occurred while page erase */
	return IAP_ERASE_ERROR;
  }

  /* Program the flash page(s) word by word (4 bytes by 4 bytes)
    (area defined by pageAddr and pages) */

  flashAddress = pageAddr;
  sourceAddress = sourceAddr;
  flashSize = flashAddress + pages * FLASH_PAGE_SIZE;

  while (flashAddress < flashSize)
  {
	memData32 =  *(__IO uint32_t *)sourceAddress;

    if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, flashAddress, memData32) == HAL_OK)
    {
      flashAddress += 4;
      sourceAddress+= 4;
    }
    else
    {
      /* Error occurred while writing data into flash memory */
      return IAP_WRITE_ERROR;
    }
  }

  /* Lock the flash to disable the flash control register access (recommended
     to protect the flash memory against possible unwanted operation) */
  HAL_FLASH_Lock();

  /* Check if the programmed data is OK
      programStatus = 0: data programmed correctly
      programStatus != 0: number of words not programmed correctly */
  flashAddress = pageAddr;
  sourceAddress = sourceAddr;
  programStatus = 0x0;

  while (flashAddress < flashSize)
  {
    memData32 = *(__IO uint32_t *)sourceAddress;
    flashData32 = *(__IO uint32_t *)flashAddress;

    if (memData32 != flashData32)
    {
      programStatus++;
    }
    flashAddress += 4;
    sourceAddress+= 4;
  }

  /*Check if there is an issue to program data*/
  if (programStatus != 0)
  {
	/* Error detected */
	return IAP_CMP_ERROR;
  }
  return IAP_OK;
}

/**
 *@brief WriteData2Flash: program one or more complete flash pages.
 * 		 The procedure compares the source RAM data with the destination
 *		 FLASH data. Are the data not the same then the data with the CRC will be
 *		 written into the flash memory otherwise leaves the procedure without flashing
 *
 *@Caution: If dataLength is 0 then the complete flash page(s) without CRC- and write counter block will be set to 0
 *
 *		  <--------------------------- Page size -------------------------->
 *		 | CRC16 |   uint32_t  | n Bytes  | n Bytes  |...        | n Bytes  |
 * 		 | value |Write counter|Data set 1|Data set 2|           |Data set n|
 *
 *@param pageAddr: 	 address of the flash page, should be a 1024 byte boundary
 *       sourceAddr: address of the memory which should be programmed into the flash
 *       offset:	 Offset of the data set
 *       dataLength: length of the data
 *       pages:		 number of pages
 *
 *@retval IAP_STATUS
 */
IAP_StatusTypeDef WriteData2Flash(uint32_t pageAddr, uint32_t srcAddr, uint32_t offset, uint32_t dataLength, uint32_t pages){
	uint32_t status;
	uint32_t writeCounter;

	if (!pages || pages>1) return  IAP_PAGE_ERROR; // @ Rondomat only one page (1024 bytes) could be used, because freeRTOS uses a lot of RAM
												   // Rondomat only use few bytes :(2 bytes (crc16) + 4 bytes (write counter) + 8 bytes rondomat data
	/* Check if the memory data is differently the flash data */
	memcpy(&Flash.Buffer, &pageAddr, pages * FLASH_PAGE_SIZE);
	if (dataLength){
		memcpy((uint8_t *)(Flash.Buffer + sizeof(struct FlashPageData) + offset), (uint8_t *) srcAddr, dataLength);
		status = memcmp(&pageAddr, &Flash.Buffer, pages * FLASH_PAGE_SIZE);
		if (!status) return IAP_OK;	// Data are not different
	}else{
		/* Data length is 0 => erase the flash page(s), set to 0 */
		memset(&Flash.Buffer, 0, pages * FLASH_PAGE_SIZE);
	}

	/* Increment the write counter */
	writeCounter = Flash.page.writeCounter[0] << 24;
	writeCounter += Flash.page.writeCounter[1] << 16;
	writeCounter += Flash.page.writeCounter[2] << 8;
	writeCounter += Flash.page.writeCounter[3];
	writeCounter++;
	Flash.page.writeCounter[0] = writeCounter >> 24;
	Flash.page.writeCounter[1] = writeCounter >> 16;
	Flash.page.writeCounter[2] = writeCounter >> 8;
	Flash.page.writeCounter[3] = writeCounter;

	/* Build CRC16 */
	Flash.page.CRC16 = CRC16LoopTable((uint8_t *)(Flash.Buffer+sizeof Flash.page.CRC16), pages * FLASH_PAGE_SIZE - sizeof Flash.page.CRC16);

	/* Write the memory data into flash */
	status = Prog_Flash(pageAddr, (uint32_t)&Flash.Buffer, pages);

	/* Check if there is an issue to programmed data*/
	if (status != 0)
	{
		/* Error detected */
		return status;
	}
	 return IAP_OK;
}

/**
 *@brief CopyFlashData2ControlRegister: copy the FLASH data to the control register
 *
 *@param controlRegAddr:	address of the control register
 *       pageAddr: 		address of the flash page which should be copied into the control register
 *       dataLength: 		data length of the control register
 *       pages: 			number of pages
 *
 * @retval IAP_STATUS
 */
IAP_StatusTypeDef CopyFlashData2ControlRegister(uint8_t *controlRegAddress, uint8_t *pageAddress, uint16_t dataLength, uint8_t pages){

	memcpy((uint8_t*)&Flash, pageAddress, sizeof(struct FlashPageData));
	if (CRC16LoopTable((uint8_t*)(pageAddress + sizeof Flash.page.CRC16), pages * FLASH_PAGE_SIZE - sizeof Flash.page.CRC16) == Flash.page.CRC16){
		memcpy(controlRegAddress, (uint8_t*)(pageAddress + sizeof(struct FlashPageData)), dataLength);
	}else{
		//CRC Error, erase the control registers
		memset((uint8_t *)(controlRegAddress), 0, dataLength);
		return IAP_CRC_ERROR;
	}
	return IAP_OK;
}

const struct IAPApi IAP = {
		WriteData2Flash,
		CopyFlashData2ControlRegister
};

